export enum EInitialTimezone {
  WIB = 'WIB',
  WITA = 'WITA',
  WIT = 'WIT',
}
