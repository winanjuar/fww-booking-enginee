export enum ETaskName {
  COMMIT_CHARGE_PAYMENT = 'CommitChargePayment',
  SUBMIT_CHARGE_TRANSACTION = 'SubmitChargeTransaction',
  PAYMENT_CONFIRMATION = 'ConfirmPayment',
  REDEEM_TICKET = 'RedeemTicket',
  ONBOARD = 'Onboard',
}
