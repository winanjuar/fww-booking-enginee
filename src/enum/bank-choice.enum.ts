export enum EBankChoice {
  MANDIRI = 'MANDIRI',
  BCA = 'BCA',
  BNI = 'BNI',
  BRI = 'BRI',
  PERMATA = 'PERMATA',
}
