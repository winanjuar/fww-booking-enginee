import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Workflow } from './workflow.entity';

@Entity()
export class WorkflowDetail {
  @ManyToOne(() => Workflow, (workflow) => workflow.processInstanceId, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'workflow' })
  @Column({ name: 'workflow', type: 'uuid' })
  workflow: Workflow;

  @PrimaryColumn({ name: 'task_id', type: 'uuid' })
  taskId: string;

  @Column({ name: 'activity_name', length: 100 })
  activityName: string;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
