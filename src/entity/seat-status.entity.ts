import { ESeatClass } from 'src/enum/seat-class.enum';
import { ESeatStatus } from 'src/enum/seat-status.enum';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';

@Unique(['flightDate', 'flight', 'seat'])
@Entity('seat_status')
export class SeatStatus {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 16 })
  passenger: string;

  @Column({ name: 'flight_date', type: 'date' })
  flightDate: string;

  @Column({ type: 'int' })
  flight: number;

  @Column({ type: 'int' })
  departure: number;

  @Column({ type: 'int' })
  destination: number;

  @Column({ type: 'int' })
  airplane: number;

  @Column({ type: 'int' })
  seat: number;

  @Column({ name: 'seat_class', length: 10 })
  seatClass: ESeatClass;

  @Column({ name: 'seat_number', length: 10 })
  seatNumber: string;

  @Column({ type: 'int' })
  price: number;

  @Column({ length: 10 })
  status: ESeatStatus;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
