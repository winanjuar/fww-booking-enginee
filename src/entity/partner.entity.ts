import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Partner {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'uuid', name: 'cognito_id', unique: true })
  cognitoId: string;

  @Column({ length: 20 })
  name: string;

  @Column({ unique: true })
  username: string;

  @Column({ name: 'pic_email', length: 50 })
  picEmail: string;

  @Column({ name: 'pic_phone', length: 20 })
  picPhone: string;

  @CreateDateColumn({ name: 'created_at', select: false })
  createdAt: string;

  @UpdateDateColumn({ name: 'updated_at', select: false })
  updatedAt: string;

  @DeleteDateColumn({ name: 'deleted_at', select: false })
  deletedAt: string;
}
