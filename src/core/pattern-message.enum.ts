export enum EPatternMessage {
  REPORT_TASK_WORKFLOW = 'EP_ReportTaskWorkflow',
  SEND_NORMAL_MAIL = 'EP_SendNormalMail',
  SEND_DECLINE_MAIL = 'EP_SendDeclineMail',
  UPDATE_SEAT_STATUS = 'EP_UpdateSeatStatus',
  UPDATE_RESERVATION = 'EP_UpdateReservation',
  CHARGE_PAYMENT = 'EP_ChargePayment',
  UPDATE_PAYMENT = 'EP_UpdatePayment',
}
