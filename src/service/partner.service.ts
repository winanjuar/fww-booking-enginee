import { Injectable, Logger } from '@nestjs/common';
import {
  AuthenticationDetails,
  CognitoUser,
  CognitoUserAttribute,
  CognitoUserPool,
} from 'amazon-cognito-identity-js';
import { PartnerRepository } from 'src/repository/partner.repository';
import { AuthConfig } from 'src/auth/auth.config';
import { RegisterRequestDto } from 'src/dto/register-partner.dto';
import { IAWSError } from 'src/interface/aws-error.interface';
import { IRegisterPartner } from 'src/interface/register-partner.interface';
import { LoginRequestDto } from 'src/dto/login.request.dto';
import { ITokenLogin } from 'src/interface/token-login.interface';

@Injectable()
export class PartnerService {
  private readonly logger = new Logger(PartnerService.name);
  private readonly userPool: CognitoUserPool;

  constructor(
    private authConfig: AuthConfig,
    private readonly partnerRepo: PartnerRepository,
  ) {
    this.userPool = new CognitoUserPool({
      UserPoolId: this.authConfig.userPoolId,
      ClientId: this.authConfig.clientId,
    });
  }

  async register(registerDto: RegisterRequestDto): Promise<IRegisterPartner> {
    const { username, name, picEmail, picPhone, password } = registerDto;

    const newUser = new Promise<IRegisterPartner>((resolve, reject) => {
      return this.userPool.signUp(
        username,
        password,
        [
          new CognitoUserAttribute({ Name: 'email', Value: picEmail }),
          new CognitoUserAttribute({
            Name: 'phone_number',
            Value: picPhone,
          }),
        ],
        null,
        (error, result) => {
          if (!result) {
            const listExceptions = [
              'UsernameExistsException',
              'InvalidPasswordException',
            ];
            if (listExceptions.includes(error.name)) {
              reject({
                name: error.name,
                response: {
                  statusCode: 422,
                  message: error.message,
                  error: 'Unprocessable Entity',
                },
              } as IAWSError);
            } else {
              reject({
                name: error.name,
                response: {
                  statusCode: 500,
                  message: error.message,
                  error: 'Internal Serval Error',
                },
              } as IAWSError);
            }
          } else {
            const partner: IRegisterPartner = {
              cognitoId: result.userSub,
              username,
              name,
              picEmail,
              picPhone,
            };
            this.logger.log('Register new partner to cognito');
            resolve(partner);
          }
        },
      );
    });

    const dataPartner = await newUser;
    const newPartner = await this.partnerRepo.savePartner(dataPartner);
    this.logger.log('Process data new partner');
    return newPartner;
  }

  async login(user: LoginRequestDto): Promise<ITokenLogin> {
    const { username, password } = user;
    const authenticationDetails = new AuthenticationDetails({
      Username: username,
      Password: password,
    });

    const userData = {
      Username: username,
      Pool: this.userPool,
    };
    const cognitoUser = new CognitoUser(userData);

    const token = new Promise<ITokenLogin>((resolve, reject) => {
      return cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: (result) => {
          const accessToken = result.getIdToken().getJwtToken();
          const refreshToken = result.getRefreshToken().getToken();
          this.logger.log('Login cognito successfully');
          resolve({ accessToken, refreshToken } as ITokenLogin);
        },
        onFailure: (error) => {
          const listExceptions = [
            'NotAuthorizedException',
            'UserNotConfirmedException',
          ];
          if (listExceptions.includes(error.code)) {
            reject({
              name: error.code,
              response: {
                statusCode: 401,
                message: error.message,
                error: 'Unauthorize',
              },
            } as IAWSError);
          } else {
            reject({
              name: error.code,
              response: {
                statusCode: 500,
                message: error.message,
                error: 'Internal Server Error',
              },
            } as IAWSError);
          }
        },
      });
    });

    this.logger.log('Login successfully');
    return await token;
  }
}
