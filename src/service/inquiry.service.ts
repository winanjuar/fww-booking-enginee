import {
  BadRequestException,
  Injectable,
  Logger,
  UnprocessableEntityException,
} from '@nestjs/common';
import { ChoseSeatDto } from 'src/dto/chose-seat.dto';
import { SeatStatusRepository } from 'src/repository/seat-status.repository';
import { CoreService } from './core.service';
import { FlightCandidateDto } from 'src/dto/flight-candidate.dto';
import { SeatDto } from 'src/dto/seat.dto';
import { IFlightCandidateRequest } from 'src/interface/core-service/request/flight-candidate-request.interface';
import { TranslateService } from './translate.service';
import { IFlight } from 'src/interface/core-service/flight.interface';
import { ISeatValidationResponse } from 'src/interface/core-service/response/seat-validation-response.interface';
import { IChoseSeatResult } from 'src/interface/chose-seat-result.interface';
import { IChoseSeatRequest } from 'src/interface/chose-seat-request.interface';

@Injectable()
export class InquiryService {
  private readonly logger = new Logger(InquiryService.name);

  constructor(
    private readonly coreService: CoreService,
    private readonly translateService: TranslateService,
    private readonly seatStatusRepo: SeatStatusRepository,
  ) {}

  async getFlightCandidate(flightDto: FlightCandidateDto): Promise<IFlight[]> {
    const todayString =
      this.translateService.reformattingCurrentTimeFollowTimezone(
        flightDto.timezone,
      );
    if (flightDto.flightDate < todayString.slice(0, 10)) {
      throw new BadRequestException(
        `flightDate should be greather than or equal ${todayString.slice(
          0,
          10,
        )}`,
      );
    }

    const flightCandidateReq: IFlightCandidateRequest = {
      departureId: flightDto.departure,
      destinationId: flightDto.destination,
    };

    const result = await Promise.all([
      this.coreService.fetchFlightCandidate(flightCandidateReq),
      this.seatStatusRepo.getOccupationSummary(flightDto),
    ]);

    const flightCandidates = result[0];
    const occupations = result[1];

    const reformattedFlightCandidates =
      this.translateService.reformattingFlightCandate(
        flightCandidates,
        occupations,
        flightDto.flightDate,
      );

    this.logger.log('Process data flight candidates');
    return reformattedFlightCandidates;
  }

  async getInfoBaggage(id: number): Promise<IFlight> {
    const flightWithBaggages = await this.coreService.fetchFlightBaggage(id);
    const reformattedFlightWithBaggage =
      this.translateService.reformattingInfoBaggage(flightWithBaggages);

    this.logger.log('Process data flight with info baggage');
    return reformattedFlightWithBaggage;
  }

  async getAvailableSeat(seatDto: SeatDto) {
    const todayString =
      this.translateService.reformattingCurrentTimeFollowTimezone(
        seatDto.timezone,
      );
    if (seatDto.flightDate < todayString.slice(0, 10)) {
      throw new BadRequestException(
        `flightDate should be greather than or equal ${todayString.slice(
          0,
          10,
        )}`,
      );
    }

    const result = await Promise.all([
      this.coreService.fetchFlightSeat(seatDto.flight, seatDto.seatClass),
      this.seatStatusRepo.getOccupied(seatDto),
    ]);
    const flightSeats = result[0];
    const occupiedSeats = result[1];
    const occupiedSeatIds = occupiedSeats.map((occ) => occ.seat);
    const availableSeats = this.translateService.reformattingAvailableSeat(
      flightSeats,
      occupiedSeatIds,
    );

    this.logger.log('Reformat data available seat');
    return availableSeats;
  }

  async chooseSeat(seatDto: ChoseSeatDto): Promise<IChoseSeatResult> {
    const { passenger, flightDate, timezone, flight, seat } = seatDto;
    const todayString =
      this.translateService.reformattingCurrentTimeFollowTimezone(timezone);
    if (flightDate < todayString.slice(0, 10)) {
      throw new BadRequestException(
        `flightDate should be greather than or equal ${todayString.slice(
          0,
          10,
        )}`,
      );
    }
    const anotherSeatStillLocked = await this.seatStatusRepo.checkAnotherLock(
      passenger,
      flightDate,
      seat,
    );

    if (anotherSeatStillLocked) {
      throw new UnprocessableEntityException(
        `You still have another selected seat, need release first: ${anotherSeatStillLocked.flightDate} - ${anotherSeatStillLocked.flight} - ${anotherSeatStillLocked.seat}`,
      );
    }

    const validationResponse: ISeatValidationResponse =
      await this.coreService.validateSeat(flight, seat);

    const { isValid, ...seatValid } = validationResponse;

    if (!isValid) {
      throw new UnprocessableEntityException('Invalid input seat info');
    }

    const choseSeatRequest: IChoseSeatRequest = {
      passenger,
      flightDate,
      ...seatValid,
      // seat status akan di lengkapi di repository
    };

    const seatChosen = await this.seatStatusRepo.requestChooseSeat(
      choseSeatRequest,
    );
    this.logger.log('Process choose seat');

    return {
      passenger: seatChosen.passenger,
      flightDate: seatChosen.flightDate,
      flight: seatChosen.flight,
      seat: seatChosen.seat,
      status: seatChosen.status,
    } as IChoseSeatResult;
  }

  async unchooseSeat(seatDto: ChoseSeatDto): Promise<IChoseSeatResult> {
    const { passenger, timezone, flightDate, flight, seat } = seatDto;
    const todayString =
      this.translateService.reformattingCurrentTimeFollowTimezone(timezone);
    if (flightDate < todayString.slice(0, 10)) {
      throw new BadRequestException(
        `flightDate should be greather than or equal ${todayString.slice(
          0,
          10,
        )}`,
      );
    }

    const validationResponse = await this.coreService.validateSeat(
      flight,
      seat,
    );

    const { isValid, ...seatValid } = validationResponse;

    const choseSeatRequest: IChoseSeatRequest = {
      passenger,
      flightDate,
      ...seatValid,
    };

    if (!isValid) {
      throw new UnprocessableEntityException('Invalid input seat info');
    }

    const seatUnchoose = await this.seatStatusRepo.requestUnchooseSeat(
      choseSeatRequest,
    );
    this.logger.log('Process unchoose seat');
    return {
      passenger: seatUnchoose.passenger,
      flightDate: seatUnchoose.flightDate,
      flight: seatUnchoose.flight,
      seat: seatUnchoose.seat,
      status: seatUnchoose.status,
    } as IChoseSeatResult;
  }
}
