import {
  BadRequestException,
  Injectable,
  Logger,
  UnprocessableEntityException,
} from '@nestjs/common';
import { SeatStatusRepository } from 'src/repository/seat-status.repository';
import { CoreService } from './core.service';
import { ReservationDto } from 'src/dto/reservation.dto';
import { IReservationCheck } from 'src/interface/reservation-check.interface';
import { ESeatStatus } from 'src/enum/seat-status.enum';
import { IReservationNew } from 'src/interface/reservation-new.interface';
import { BPMService } from './bpm.service';
import { IStartProcess } from 'src/interface/bpm-service/start-process.interface';
import { WorkflowRepository } from 'src/repository/workflow.repository';
import { TranslateService } from './translate.service';
import { IWorkflowMaster } from 'src/interface/workflow.interface';
import { CognitoUserDto } from 'src/dto/cognito-user.dto';
import { Cron, CronExpression } from '@nestjs/schedule';

@Injectable()
export class ReservationService {
  private readonly logger = new Logger(ReservationService.name);

  constructor(
    private readonly coreService: CoreService,
    private readonly bpmService: BPMService,
    private readonly translateService: TranslateService,
    private readonly seatStatusRepo: SeatStatusRepository,
    private readonly workflowRepo: WorkflowRepository,
  ) {}

  async makeReservation(user: CognitoUserDto, reservationDto: ReservationDto) {
    const { timezone, ...dataReservation } = reservationDto;
    const today =
      this.translateService.reformattingCurrentTimeFollowTimezone(timezone);
    if (reservationDto.flightDate < today.slice(0, 10)) {
      throw new BadRequestException(
        `flightDate should be greather than or equal ${today.slice(0, 10)}`,
      );
    }

    const reservationCheck: IReservationCheck = {
      passenger: reservationDto.identityNumber,
      flightDate: reservationDto.flightDate,
    };

    const resultCheckSeatAndFlight = await Promise.all([
      this.seatStatusRepo.findOneByPassengerAndFlightDate(reservationCheck),
      this.coreService.fetchFlightAirport(reservationDto.flight),
    ]);

    const seatStatus = resultCheckSeatAndFlight[0];
    const flightAirport = resultCheckSeatAndFlight[1];
    const maskingResultFlightAirport =
      this.translateService.reformattingFlightAirport(flightAirport);

    if (seatStatus.flight !== reservationDto.flight) {
      throw new UnprocessableEntityException(
        `Failed to make reservation due to have another flight : ${seatStatus.flight}`,
      );
    }

    if (seatStatus.seat !== reservationDto.seat) {
      throw new UnprocessableEntityException(
        `Failed to make reservation due to seat misplaced : ${seatStatus.seat}`,
      );
    }

    if (seatStatus.status !== ESeatStatus.SELECTED) {
      throw new UnprocessableEntityException(
        `Failed to make reservation due to seat status ${seatStatus.seat} is not selected`,
      );
    }

    const reservationTime = new Date().toISOString();

    const dataReservationNew: IReservationNew = {
      partner: user.cognitoId,
      ...dataReservation,
      reservationTime,
      priceActual: seatStatus.price,
    };

    const reservation = await this.coreService.submitReservation(
      dataReservationNew,
    );
    if (!reservation) {
      throw new UnprocessableEntityException(`Failed to make reservation`);
    }

    const dataWorkflow: IStartProcess = {
      identityNumber: reservationDto.identityNumber,
      name: reservationDto.name,
      birthDate: reservationDto.birthDate,
      isMember: reservationDto.member ? 'Yes' : 'No',
      memberId: reservationDto.member ? reservationDto.member : null,
      email: reservationDto.email,
      flight: maskingResultFlightAirport.id,
      flightCode: maskingResultFlightAirport.code,
      flightDate: reservationDto.flightDate,
      departureAirport: maskingResultFlightAirport.departure.name,
      departureCode: maskingResultFlightAirport.departure.code,
      departureTime: maskingResultFlightAirport.departureTime,
      departureTimezone: maskingResultFlightAirport.departure.timezone,
      destination: maskingResultFlightAirport.destination.city,
      destinationAirport: maskingResultFlightAirport.destination.name,
      destinationCode: maskingResultFlightAirport.destination.code,
      arrivalTime: maskingResultFlightAirport.arrivalTime,
      arrivalTimezone: maskingResultFlightAirport.destination.timezone,
      seat: seatStatus.seat,
      seatNumber: seatStatus.seatNumber,
      reservationTime,
      timezone,
      bussinessKey: String(reservation.id),
    };

    seatStatus.status = ESeatStatus.BOOKED;

    const result = await Promise.all([
      this.bpmService.startProcess(dataWorkflow),
      this.seatStatusRepo.save(seatStatus),
    ]);

    const newProcess = result[0];
    const workflowNew: IWorkflowMaster = {
      processInstanceId: newProcess.id,
      reservationId: reservation.id,
    };
    await this.workflowRepo.saveWorkflow(workflowNew);

    this.logger.log('Process reservation to workflow engine, core service');
    return {
      messageResponse:
        'Success make a reservation, we will deliver booking code via email as soon as possible',
      data: {
        id: reservation.id,
        reservationTime:
          this.translateService.reformattingUTCTimeFollowTimezone(
            reservationTime,
            timezone,
          ),
        status: reservation.status,
      },
    };
  }

  @Cron(CronExpression.EVERY_30_MINUTES)
  async handleSeatReset() {
    const targetUpdatedAt =
      this.translateService.reformattingLast30MinutesInWIB();
    this.logger.log('Seat status reset by cron');
    await this.seatStatusRepo.resetSeatStatus(targetUpdatedAt);
  }
}
