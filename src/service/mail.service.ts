import { Injectable, Logger } from '@nestjs/common';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { NormalMailRequestDto } from 'src/dto/normal-mail-request.dto';
import { DeclineMailRequestDto } from 'src/dto/decline-mail-request.dto';
import { IMailNormal } from 'src/interface/mail-normal.interface';
import { IMailDecline } from 'src/interface/mail-decline.interface';

@Injectable()
export class MailService {
  private readonly logger = new Logger(MailService.name);

  constructor(
    @InjectQueue('NormalMailQueue') private normalQueue: Queue,
    @InjectQueue('DeclineMailQueue') private declineQueue: Queue,
  ) {}

  async sendNormalMail(
    data: NormalMailRequestDto | IMailNormal,
  ): Promise<void> {
    await this.normalQueue.add(data, {
      backoff: 3,
      removeOnComplete: true,
      removeOnFail: { age: 60 * 60 },
    });
    this.logger.log('Add mail to normal queue');
  }

  async sendDeclineMail(
    data: DeclineMailRequestDto | IMailDecline,
  ): Promise<void> {
    await this.declineQueue.add(data, {
      backoff: 3,
      removeOnComplete: true,
      removeOnFail: { age: 60 * 60 },
    });
    this.logger.log('Add mail to decline queue');
  }
}
