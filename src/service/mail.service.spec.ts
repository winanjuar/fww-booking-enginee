import { Test, TestingModule } from '@nestjs/testing';
import { MailService } from './mail.service';
import { IMailNormal } from 'src/interface/mail-normal.interface';
import { faker } from '@faker-js/faker';
import { ETimezoneCode } from 'src/enum/timezone-code.enum';
import { getQueueToken } from '@nestjs/bull';
import { IMailDecline } from 'src/interface/mail-decline.interface';

describe('MailService', () => {
  let service: MailService;

  const normalQueue = {
    add: jest.fn(),
  };

  const declineQueue = {
    add: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MailService,
        { provide: getQueueToken('NormalMailQueue'), useValue: normalQueue },
        { provide: getQueueToken('DeclineMailQueue'), useValue: declineQueue },
      ],
    }).compile();
    module.useLogger(false);
    service = module.get<MailService>(MailService);
  });

  afterEach(() => jest.clearAllMocks());

  describe('sendNormalMail', () => {
    const mailNormalData: IMailNormal = {
      mailTo: faker.internet.email(),
      flightCode: faker.airline.flightNumber(),
      flightDate: faker.date.future().toISOString().split('T')[0],
      departureAirport: faker.airline.airport().name,
      departureCode: faker.airline.airport().iataCode,
      departureTime: faker.date.anytime().toTimeString(),
      departureTimezone: faker.helpers.enumValue(ETimezoneCode),
      destinationAirport: faker.airline.airport().name,
      destinationCode: faker.airline.airport().iataCode,
      arrivalTime: faker.date.anytime().toTimeString(),
      arrivalTimezone: faker.helpers.enumValue(ETimezoneCode),
      seatNumber: faker.airline.seat(),
      typeCode: faker.helpers.arrayElement([
        'Booking Code',
        'Reservation Code',
      ]),
      realCode: faker.string.alphanumeric({ casing: 'upper', length: 8 }),
      nextProcess: faker.helpers.arrayElement(['pembayaran', 'redeem']),
      deadline: faker.date.future().toISOString(),
      consequence: faker.string.sample(),
      specialNote: faker.string.sample(),
    };

    it('should add data to queue normal mail', async () => {
      // arrange
      const addSpy = jest.spyOn(normalQueue, 'add');

      // act
      const sendNormalMail = service.sendNormalMail(mailNormalData);

      // assert
      await expect(sendNormalMail).resolves.not.toThrow();
      expect(addSpy).toHaveBeenCalledTimes(1);
      expect(addSpy).toHaveBeenCalledWith(mailNormalData, {
        backoff: 3,
        removeOnComplete: true,
        removeOnFail: { age: 60 * 60 },
      });
    });
  });

  describe('sendDeclineMail', () => {
    const mailDeclineData: IMailDecline = {
      mailTo: faker.internet.email(),
      flightCode: faker.airline.flightNumber(),
      flightDate: faker.date.future().toISOString().split('T')[0],
      departureAirport: faker.airline.airport().name,
      departureCode: faker.airline.airport().iataCode,
      departureTime: faker.date.anytime().toTimeString(),
      departureTimezone: faker.helpers.enumValue(ETimezoneCode),
      destinationAirport: faker.airline.airport().name,
      destinationCode: faker.airline.airport().iataCode,
      arrivalTime: faker.date.anytime().toTimeString(),
      arrivalTimezone: faker.helpers.enumValue(ETimezoneCode),
      seatNumber: faker.airline.seat(),
      reservationStatus: 'DIBATALKAN',
      reason: faker.string.sample(),
    };

    it('should add data to queue normal mail', async () => {
      // arrange
      const addSpy = jest.spyOn(declineQueue, 'add');

      // act
      const sendDeclineMailSpy = service.sendDeclineMail(mailDeclineData);

      // assert
      await expect(sendDeclineMailSpy).resolves.not.toThrow();
      expect(addSpy).toHaveBeenCalledTimes(1);
      expect(addSpy).toHaveBeenCalledWith(mailDeclineData, {
        backoff: 3,
        removeOnComplete: true,
        removeOnFail: { age: 60 * 60 },
      });
    });
  });
});
