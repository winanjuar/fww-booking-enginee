import {
  Inject,
  Injectable,
  Logger,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { SeatStatusRepository } from 'src/repository/seat-status.repository';
import { IWorkflowDetail } from 'src/interface/workflow.interface';
import { WorkflowDetailRepository } from 'src/repository/workflow-detail.repository';
import { WorkflowRepository } from 'src/repository/workflow.repository';
import { BPMService } from './bpm.service';
import {
  ITaskResult,
  ITaskResultWithInstanceID,
  ITaskVariableResult,
} from 'src/interface/bpm-service/task-result.interface';
import { ChargePaymentDto } from 'src/dto/charge-payment.dto';
import { CoreService } from './core.service';
import {
  IChargeRequest,
  ICustomerDetail,
  IItemDetail,
  ITransactionDetail,
} from 'src/interface/payment-service/charge-request.interface';
import * as crypto from 'crypto';
import { PaymentService } from './payment.service';
import { ClientProxy } from '@nestjs/microservices';
import { EPatternMessage } from 'src/core/pattern-message.enum';
import { EReservationStatus } from 'src/enum/reservation-status.enum';
import { ETaskName } from 'src/enum/task-name.enum';
import { ReservationCodeDto } from 'src/dto/reservation-code.dto';
import { ISeatStatusUpdate } from 'src/interface/seat-status-update.interface';
import { TranslateService } from './translate.service';
import { IReservationUpdateRequest } from 'src/interface/reservation-update.interface';
import {
  IPaymentDetail,
  IPaymentMaster,
  IPaymentUpdate,
} from 'src/interface/payment.interface';
import { ICommitPayment } from 'src/interface/commit-payment.interface';

@Injectable()
export class WorkflowService {
  private readonly logger = new Logger(WorkflowService.name);

  constructor(
    @Inject('CoreEngine') private readonly coreClient: ClientProxy,
    private readonly bpmService: BPMService,
    private readonly coreService: CoreService,
    private readonly paymentService: PaymentService,
    private readonly translateService: TranslateService,
    private readonly seatStatusRepo: SeatStatusRepository,
    private readonly workflowRepo: WorkflowRepository,
    private readonly workflowDetailRepo: WorkflowDetailRepository,
  ) {}

  private async __getCurrentTask(
    reservationId: number,
    taskName: string,
  ): Promise<ITaskResultWithInstanceID> {
    const workflow = await this.workflowRepo.getWorkflowByReservationId(
      reservationId,
    );
    if (!workflow) {
      throw new NotFoundException('Workflow not found');
    }
    const tasks = await this.bpmService.viewTask(workflow.processInstanceId);

    if (tasks.length === 0) {
      throw new NotFoundException('Tasks does not exist in camunda');
    }
    const task = tasks.find(
      (task: ITaskResult) => task.taskDefinitionKey === taskName,
    );
    if (!task) {
      throw new NotFoundException('Tasks does not exist in camunda');
    }
    return {
      workflowInstanceId: workflow.processInstanceId,
      currentTask: task,
    } as ITaskResultWithInstanceID;
  }

  private async __getTaskVariables(
    reservationId: number,
    taskName: string,
  ): Promise<ITaskVariableResult> {
    const task = await this.__getCurrentTask(reservationId, taskName);
    const taskVariables = await this.bpmService.getVariables(
      task.currentTask.id,
    );

    return {
      workflowInstanceId: task.workflowInstanceId,
      currentTaskId: task.currentTask.id,
      currentTaskName: task.currentTask.taskDefinitionKey,
      variables: taskVariables,
    } as ITaskVariableResult;
  }

  async insertWorkflowDetail(workflowData: IWorkflowDetail) {
    const workflowDetail = await this.workflowDetailRepo.saveWorkflowDetail(
      workflowData,
    );
    this.logger.log('Process data workflow detail');
    return workflowDetail;
  }

  async updateSeatStatus(seatStatusData: ISeatStatusUpdate) {
    const seatStatus = await this.seatStatusRepo.updateStatus(seatStatusData);
    this.logger.log('Process data seat status');
    return seatStatus;
  }

  async commitPayment(reservationId: number) {
    const task = await this.__getCurrentTask(
      reservationId,
      ETaskName.COMMIT_CHARGE_PAYMENT,
    );

    const commitPaymentTime = new Date().toISOString();
    const dataCompleteTask = {
      variables: {
        commitPaymentTime: {
          value: commitPaymentTime,
          type: 'String',
        },
      },
    };

    const dataWorkflow: IWorkflowDetail = {
      workflow: { processInstanceId: task.workflowInstanceId },
      taskId: task.currentTask.id,
      activityName: task.currentTask.taskDefinitionKey,
    };

    await Promise.all([
      this.bpmService.completeTask(task.currentTask.id, dataCompleteTask),
      this.insertWorkflowDetail(dataWorkflow),
    ]).catch(() => {
      throw new UnprocessableEntityException('Error update workflow');
    });

    return { reservationId, commitPaymentTime } as ICommitPayment;
  }

  async chargeTransaction(chargeDto: ChargePaymentDto) {
    const result = await Promise.all([
      this.coreService.fetchReservation(chargeDto.reservationId),
      this.__getTaskVariables(
        chargeDto.reservationId,
        ETaskName.SUBMIT_CHARGE_TRANSACTION,
      ),
    ]).catch(() => {
      throw new NotFoundException('Prerequisite task not found');
    });

    const reservation = result[0];
    const workflowInstanceId = result[1].workflowInstanceId;
    const currentTaskId = result[1].currentTaskId;
    const currentTaskName = result[1].currentTaskName;
    const taskVariables = result[1].variables;

    if (reservation.bookingCode !== chargeDto.bookingCode) {
      throw new UnprocessableEntityException('Booking Code does not match');
    }

    const itemDetails: IItemDetail[] = [];

    const ticketItem: IItemDetail = {
      id: crypto.randomUUID(),
      name: `TIKET ${reservation.flight.code} ${reservation.flightDate}`,
      quantity: 1,
      price: reservation.priceActual,
    };
    itemDetails.push(ticketItem);

    let discountDestinationAmount = 0;
    if (taskVariables.discountDestination.value !== 0) {
      discountDestinationAmount = Math.round(
        (taskVariables.discountDestination.value / 100) *
          reservation.priceActual,
      );
      const discountDestinationItem: IItemDetail = {
        id: crypto.randomUUID(),
        name: `DISKON DESTINATION`,
        quantity: 1,
        price: -discountDestinationAmount,
      };
      itemDetails.push(discountDestinationItem);
    }

    let discountFastPaymentAmount = 0;
    if (taskVariables.discountFastPayment.value !== 0) {
      discountFastPaymentAmount = Math.round(
        (taskVariables.discountFastPayment.value / 100) *
          reservation.priceActual,
      );
      const discountFastPaymentItem: IItemDetail = {
        id: crypto.randomUUID(),
        name: `DISKON FAST PAYMENT`,
        quantity: 1,
        price: -discountFastPaymentAmount,
      };
      itemDetails.push(discountFastPaymentItem);
    }

    const paymentCode = Math.round(Math.random() * 500 + 100);
    const uniquePaymentItem: IItemDetail = {
      id: crypto.randomUUID(),
      name: 'KODE UNIK PEMBAYARAN',
      quantity: 1,
      price: paymentCode,
    };
    itemDetails.push(uniquePaymentItem);

    const grossAmount = itemDetails.reduce(
      (total, { price }) => total + price,
      0,
    );

    const paymentType = this.translateService.generatePaymentTypeForBank(
      chargeDto.bank,
    );

    const suffixOrderId = new Date().getTime().toString();

    const transactionDetails: ITransactionDetail = {
      order_id: `${reservation.id}-${reservation.bookingCode}-${suffixOrderId}`,
      gross_amount: grossAmount,
    };

    const names = reservation.passenger.name.split(' ');
    const firtsName = names[0];
    const lastName = names[names.length - 1];
    const customerDetails: ICustomerDetail = {
      first_name: firtsName,
      last_name: lastName,
      email: reservation.email,
      phone: reservation.phone,
    };

    const dataPaymentMidtrans: IChargeRequest = {
      ...paymentType,
      transaction_details: transactionDetails,
      item_details: itemDetails,
      customer_details: customerDetails,
    };

    const chargeResult = await this.paymentService.createCharge(
      dataPaymentMidtrans,
    );
    if (chargeResult.status_code !== '201') {
      throw new UnprocessableEntityException(chargeResult.status_message);
    }

    const dataUpdateReservation: IReservationUpdateRequest = {
      id: reservation.id,
      status: EReservationStatus.PAYMENT_CHARGED,
      journeyTime: new Date().toISOString(),
    };

    this.coreClient.emit(
      EPatternMessage.UPDATE_RESERVATION,
      dataUpdateReservation,
    );
    this.logger.log(`Emit data update reservation done`);

    const paymentDetails: IPaymentDetail[] = itemDetails;
    const paymentMaster: IPaymentMaster = {
      id: chargeResult.transaction_id,
      reservation: { id: reservation.id },
      paymentMethod: chargeDto.bank,
      paymentFinal: grossAmount,
      paymentStatus: chargeResult.transaction_status,
      chargeTime: new Date().toISOString(),
      details: paymentDetails,
    };

    this.coreClient.emit(EPatternMessage.CHARGE_PAYMENT, paymentMaster);
    this.logger.log(`Emit data charge payment done`);

    const dataCompleteTask = {
      variables: {
        paymentId: {
          value: chargeResult.transaction_id,
          type: 'String',
        },
      },
    };

    const dataWorkflow: IWorkflowDetail = {
      workflow: { processInstanceId: workflowInstanceId },
      taskId: currentTaskId,
      activityName: currentTaskName,
    };

    await Promise.all([
      this.bpmService.completeTask(currentTaskId, dataCompleteTask),
      this.insertWorkflowDetail(dataWorkflow),
    ]).catch(() => {
      throw new UnprocessableEntityException('Error update workflow');
    });

    return this.translateService.generateResponseCharge(
      chargeDto.bank,
      chargeResult,
    );
  }

  async confirmPayment(reservationId: number) {
    const task = await this.__getCurrentTask(
      reservationId,
      ETaskName.PAYMENT_CONFIRMATION,
    );

    const dataWorkflow: IWorkflowDetail = {
      workflow: { processInstanceId: task.workflowInstanceId },
      taskId: task.currentTask.id,
      activityName: task.currentTask.taskDefinitionKey,
    };

    await Promise.all([
      this.bpmService.completeTask(task.currentTask.id),
      this.insertWorkflowDetail(dataWorkflow),
    ]).catch(() => {
      throw new UnprocessableEntityException('Error update workflow');
    });

    return {
      messageResponse:
        'Terima kasih atas konfirmasi pembayaran yang sudah dilakukan, mohon tunggu beberapa saat transaksi anda sedang dalam proses pengecekan',
      reservationId,
    };
  }

  async checkPayment(paymentId: string) {
    const result = await this.paymentService.viewStatus(paymentId);
    const statusAllowed = ['200', '201'];
    if (!statusAllowed.includes(result.status_code)) {
      throw new UnprocessableEntityException(result.status_message);
    }

    const checkTime = new Date().toISOString();
    const paymentData: IPaymentUpdate = {
      id: paymentId,
      paymentStatus: result.transaction_status,
      checkTime,
      paymentTime: result.settlement_time,
    };

    this.coreClient.emit(EPatternMessage.UPDATE_PAYMENT, paymentData);
    this.logger.log(`Emit data update reservation done`);

    return paymentData;
  }

  async redeemTicket(reservationDto: ReservationCodeDto) {
    const reservation = await this.coreService.fetchReservationByCode(
      reservationDto,
    );

    if (!reservation) {
      throw new NotFoundException('Reservation not found');
    }

    const task = await this.__getTaskVariables(
      reservation.id,
      ETaskName.REDEEM_TICKET,
    );

    const redeemTime = new Date();
    const flightTimeStr = `${task.variables.flightDate.value} ${task.variables.departureTime.value}`;
    const timezoneCode = task.variables.departureTimezone.value;

    const durationFromDeparture = this.translateService.calculateDuration(
      redeemTime,
      flightTimeStr,
      timezoneCode,
    );

    const dataUpdateReservation: IReservationUpdateRequest = {
      id: reservation.id,
      status: EReservationStatus.REDEEM,
      journeyTime: redeemTime.toISOString(),
    };

    this.coreClient.emit(
      EPatternMessage.UPDATE_RESERVATION,
      dataUpdateReservation,
    );
    this.logger.log(`Emit data update reservation done`);

    let dataCompleteTask = {
      variables: {
        redeemStatus: {
          value: 'Not OK',
          type: 'String',
        },
      },
    };

    if (durationFromDeparture >= 60) {
      dataCompleteTask = {
        variables: {
          redeemStatus: {
            value: 'OK',
            type: 'String',
          },
        },
      };
    }

    const dataWorkflow: IWorkflowDetail = {
      workflow: { processInstanceId: task.workflowInstanceId },
      taskId: task.currentTaskId,
      activityName: task.currentTaskName,
    };

    await Promise.all([
      this.bpmService.completeTask(task.currentTaskId, dataCompleteTask),
      this.insertWorkflowDetail(dataWorkflow),
    ]).catch(() => {
      throw new UnprocessableEntityException('Error update workflow');
    });

    return {
      messageResponse: 'Redeem ticket success',
      redeemTime: redeemTime.toISOString(),
    };
  }

  async onboard(ticketNumber: string) {
    const reservation = await this.coreService.fetchReservationByTicket(
      ticketNumber,
    );

    if (!reservation) {
      throw new NotFoundException('Reservation not found');
    }

    const task = await this.__getCurrentTask(reservation.id, ETaskName.ONBOARD);

    const boardingTime = new Date().toISOString();
    const dataUpdateReservation: IReservationUpdateRequest = {
      id: reservation.id,
      status: EReservationStatus.COMPLETED,
      journeyTime: boardingTime,
    };

    this.coreClient.emit(
      EPatternMessage.UPDATE_RESERVATION,
      dataUpdateReservation,
    );
    this.logger.log(`Emit data update reservation done`);

    const dataWorkflow: IWorkflowDetail = {
      workflow: { processInstanceId: task.workflowInstanceId },
      taskId: task.currentTask.id,
      activityName: task.currentTask.taskDefinitionKey,
    };

    await Promise.all([
      this.bpmService.completeTask(task.currentTask.id),
      this.insertWorkflowDetail(dataWorkflow),
    ]).catch(() => {
      throw new UnprocessableEntityException('Error update workflow');
    });

    return {
      messageResponse: 'Onboard success',
      boardingTime,
    };
  }
}
