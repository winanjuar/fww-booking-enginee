import { Test, TestingModule } from '@nestjs/testing';
import { TranslateService } from './translate.service';
import { faker, fakerID_ID } from '@faker-js/faker';
import { IAirport } from 'src/interface/core-service/airport.interface';
import { ETimezone } from 'src/enum/timezone.enum';
import { IAirplane } from 'src/interface/core-service/airplane.interface';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { IPrice } from 'src/interface/core-service/price.interface';
import { IFlight } from 'src/interface/core-service/flight.interface';
import { IFlightCandidateResponse } from 'src/interface/core-service/response/flight-candidate-response.interface';
import { IOccupiedFlightCandidate } from 'src/interface/occupied-flight-candidate.interface';
import { IFlightBaggageResponse } from 'src/interface/core-service/response/flight-baggage-response.interface';
import { IBaggage } from 'src/interface/core-service/baggage.interface';
import { EBankChoice } from 'src/enum/bank-choice.enum';
import { EInitialTimezone } from 'src/enum/initial-timezone.enum';
import { IFlightSeatResponse } from 'src/interface/core-service/response/flight-seat-response.interface';
import { ESeatSide } from 'src/enum/seat-side.enum';
import { ESeatPosition } from 'src/enum/seat-position.enum';
import { ISeat } from 'src/interface/core-service/seat.interface';
import { IChargeResponse } from 'src/interface/payment-service/charge-response.interface';
import { ETimezoneCode } from 'src/enum/timezone-code.enum';

describe('TranslateService', () => {
  let service: TranslateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TranslateService],
    }).compile();
    module.useLogger(false);
    service = module.get<TranslateService>(TranslateService);
  });

  afterEach(() => jest.clearAllMocks());

  describe('reformattingFlightCandate', () => {
    let mockDeparture: IAirport;
    let mockDestination: IAirport;
    let mockAirplane: IAirplane;
    let mockPrice: IPrice;
    let mockFlight: IFlight;
    let mockFlightCandidateOrigin: IFlightCandidateResponse;
    let mockOccupiedFlightCandidate: IOccupiedFlightCandidate;

    beforeEach(() => {
      mockDeparture = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockDestination = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockDeparture,
        destination: mockDestination,
        airplane: mockAirplane,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
      };

      mockPrice = {
        id: faker.number.int({ min: 1 }),
        flight: mockFlight.id,
        seatClass: faker.helpers.enumValue(ESeatClass),
        priceConfig: faker.number.int({ min: 500000 }),
      };

      mockFlight.prices = [mockPrice];

      mockFlightCandidateOrigin = {
        id: mockFlight.id,
        code: mockFlight.code,
        departure: mockFlight.departure as IAirport,
        destination: mockFlight.destination as IAirport,
        airplane: mockFlight.airplane as IAirplane,
        departureTimeInWIB: mockFlight.departureTime,
        arrivalTimeInWIB: mockFlight.arrivalTime,
        durationInMinutes: mockFlight.durationInMinutes,
        prices: [mockPrice],
      };

      mockOccupiedFlightCandidate = {
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: mockFlight.id,
        seatClass: mockPrice.seatClass,
        occupied: faker.number.int({ min: 1, max: 80 }),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return reformatted response flight candidate in timezone WIB', async () => {
      // arrange
      mockDeparture.timezone = ETimezone.WIB;
      mockDestination.timezone = ETimezone.WIB;

      const flightDate = mockOccupiedFlightCandidate.flightDate;
      const flightOccupation = mockOccupiedFlightCandidate;
      const newDepartureTime = mockFlight.departureTime;
      const newArrivalTime = mockFlight.arrivalTime;

      const remains =
        mockPrice.seatClass === ESeatClass.BUSINESS
          ? mockAirplane.maxBusiness
          : mockAirplane.maxEconomy;

      mockPrice.seatLeft = flightOccupation
        ? remains - Number(flightOccupation.occupied)
        : remains;
      mockFlight.flightDate = mockOccupiedFlightCandidate.flightDate;
      delete mockAirplane.id;
      delete mockPrice.id;
      delete mockPrice.flight;
      mockFlight.departureTime = newDepartureTime;
      mockFlight.arrivalTime = newArrivalTime;

      // act
      const reformattingFlightCandate = service.reformattingFlightCandate(
        [mockFlightCandidateOrigin],
        [mockOccupiedFlightCandidate],
        flightDate,
      );

      // assert
      expect(reformattingFlightCandate).toEqual([mockFlight]);
      [mockFlight].forEach((f) => expect(f).toHaveProperty('departureTime'));
      [mockFlight].forEach((f) =>
        expect(f).not.toHaveProperty('departureTimeInWIB'),
      );
      [mockFlight].forEach((f) => expect(f).toHaveProperty('arrivalTime'));
      [mockFlight].forEach((f) =>
        expect(f).not.toHaveProperty('arrivalTimeInWIB'),
      );
      [mockFlight].forEach((f) =>
        f.prices.forEach((p) => expect(p).toHaveProperty('seatLeft')),
      );
    });

    it('should return reformatted response flight candidate in timezone WITA', async () => {
      // arrange
      mockDeparture.timezone = ETimezone.WITA;
      mockDestination.timezone = ETimezone.WITA;
      mockFlightCandidateOrigin.departureTimeInWIB = '10:10:00';
      mockFlightCandidateOrigin.arrivalTimeInWIB = '12:15:00';

      const flightDate = mockOccupiedFlightCandidate.flightDate;
      const flightOccupation = mockOccupiedFlightCandidate;
      const newDepartureTime = '11:10:00';
      const newArrivalTime = '13:15:00';

      const remains =
        mockPrice.seatClass === ESeatClass.BUSINESS
          ? mockAirplane.maxBusiness
          : mockAirplane.maxEconomy;

      mockPrice.seatLeft = flightOccupation
        ? remains - Number(flightOccupation.occupied)
        : remains;
      mockFlight.flightDate = mockOccupiedFlightCandidate.flightDate;
      delete mockAirplane.id;
      delete mockPrice.id;
      delete mockPrice.flight;
      mockFlight.departureTime = newDepartureTime;
      mockFlight.arrivalTime = newArrivalTime;

      // act
      const reformattingFlightCandate = service.reformattingFlightCandate(
        [mockFlightCandidateOrigin],
        [mockOccupiedFlightCandidate],
        flightDate,
      );

      // assert
      expect(reformattingFlightCandate).toEqual([mockFlight]);
      [mockFlight].forEach((f) => expect(f).toHaveProperty('departureTime'));
      [mockFlight].forEach((f) =>
        expect(f).not.toHaveProperty('departureTimeInWIB'),
      );
      [mockFlight].forEach((f) => expect(f).toHaveProperty('arrivalTime'));
      [mockFlight].forEach((f) =>
        expect(f).not.toHaveProperty('arrivalTimeInWIB'),
      );
      [mockFlight].forEach((f) =>
        f.prices.forEach((p) => expect(p).toHaveProperty('seatLeft')),
      );
    });

    it('should return reformatted response flight candidate in timezone WIT', async () => {
      // arrange
      mockDeparture.timezone = ETimezone.WIT;
      mockDestination.timezone = ETimezone.WIT;
      mockFlightCandidateOrigin.departureTimeInWIB = '21:50:00';
      mockFlightCandidateOrigin.arrivalTimeInWIB = '23:20:00';

      const flightDate = mockOccupiedFlightCandidate.flightDate;
      const flightOccupation = mockOccupiedFlightCandidate;
      const newDepartureTime = '23:50:00';
      const newArrivalTime = '01:20:00';

      const remains =
        mockPrice.seatClass === ESeatClass.BUSINESS
          ? mockAirplane.maxBusiness
          : mockAirplane.maxEconomy;

      mockPrice.seatLeft = flightOccupation
        ? remains - Number(flightOccupation.occupied)
        : remains;
      mockFlight.flightDate = mockOccupiedFlightCandidate.flightDate;
      delete mockAirplane.id;
      delete mockPrice.id;
      delete mockPrice.flight;
      mockFlight.departureTime = newDepartureTime;
      mockFlight.arrivalTime = newArrivalTime;

      // act
      const reformattingFlightCandate = service.reformattingFlightCandate(
        [mockFlightCandidateOrigin],
        [mockOccupiedFlightCandidate],
        flightDate,
      );

      // assert
      expect(reformattingFlightCandate).toEqual([mockFlight]);
      [mockFlight].forEach((f) => expect(f).toHaveProperty('departureTime'));
      [mockFlight].forEach((f) =>
        expect(f).not.toHaveProperty('departureTimeInWIB'),
      );
      [mockFlight].forEach((f) => expect(f).toHaveProperty('arrivalTime'));
      [mockFlight].forEach((f) =>
        expect(f).not.toHaveProperty('arrivalTimeInWIB'),
      );
      [mockFlight].forEach((f) =>
        f.prices.forEach((p) => expect(p).toHaveProperty('seatLeft')),
      );
    });

    it('should return reformatted response flight candidate in seat class economy', async () => {
      // arrange
      mockDeparture.timezone = ETimezone.WITA;
      mockDestination.timezone = ETimezone.WITA;
      mockFlightCandidateOrigin.departureTimeInWIB = '10:10:00';
      mockFlightCandidateOrigin.arrivalTimeInWIB = '12:15:00';
      mockPrice.seatClass = ESeatClass.ECONOMY;
      mockOccupiedFlightCandidate.seatClass = ESeatClass.ECONOMY;

      const flightDate = mockOccupiedFlightCandidate.flightDate;
      const flightOccupation = mockOccupiedFlightCandidate;
      const newDepartureTime = '11:10:00';
      const newArrivalTime = '13:15:00';

      const remains = mockAirplane.maxEconomy;

      mockPrice.seatLeft = flightOccupation
        ? remains - Number(flightOccupation.occupied)
        : remains;
      mockFlight.flightDate = mockOccupiedFlightCandidate.flightDate;
      delete mockAirplane.id;
      delete mockPrice.id;
      delete mockPrice.flight;
      mockFlight.departureTime = newDepartureTime;
      mockFlight.arrivalTime = newArrivalTime;

      // act
      const reformattingFlightCandate = service.reformattingFlightCandate(
        [mockFlightCandidateOrigin],
        [mockOccupiedFlightCandidate],
        flightDate,
      );

      // assert
      expect(reformattingFlightCandate).toEqual([mockFlight]);
      [mockFlight].forEach((f) => expect(f).toHaveProperty('departureTime'));
      [mockFlight].forEach((f) =>
        expect(f).not.toHaveProperty('departureTimeInWIB'),
      );
      [mockFlight].forEach((f) => expect(f).toHaveProperty('arrivalTime'));
      [mockFlight].forEach((f) =>
        expect(f).not.toHaveProperty('arrivalTimeInWIB'),
      );
      [mockFlight].forEach((f) =>
        f.prices.forEach((p) => expect(p).toHaveProperty('seatLeft')),
      );
    });

    it('should return reformatted response flight candidate in seat class business', async () => {
      // arrange
      mockDeparture.timezone = ETimezone.WITA;
      mockDestination.timezone = ETimezone.WITA;
      mockFlightCandidateOrigin.departureTimeInWIB = '10:10:00';
      mockFlightCandidateOrigin.arrivalTimeInWIB = '12:15:00';
      mockPrice.seatClass = ESeatClass.BUSINESS;
      mockOccupiedFlightCandidate.seatClass = ESeatClass.BUSINESS;

      const flightDate = mockOccupiedFlightCandidate.flightDate;
      const flightOccupation = mockOccupiedFlightCandidate;
      const newDepartureTime = '11:10:00';
      const newArrivalTime = '13:15:00';

      const remains = mockAirplane.maxBusiness;

      mockPrice.seatLeft = flightOccupation
        ? remains - Number(flightOccupation.occupied)
        : remains;
      mockFlight.flightDate = mockOccupiedFlightCandidate.flightDate;
      delete mockAirplane.id;
      delete mockPrice.id;
      delete mockPrice.flight;
      mockFlight.departureTime = newDepartureTime;
      mockFlight.arrivalTime = newArrivalTime;

      // act
      const reformattingFlightCandate = service.reformattingFlightCandate(
        [mockFlightCandidateOrigin],
        [mockOccupiedFlightCandidate],
        flightDate,
      );

      // assert
      expect(reformattingFlightCandate).toEqual([mockFlight]);
      [mockFlight].forEach((f) => expect(f).toHaveProperty('departureTime'));
      [mockFlight].forEach((f) =>
        expect(f).not.toHaveProperty('departureTimeInWIB'),
      );
      [mockFlight].forEach((f) => expect(f).toHaveProperty('arrivalTime'));
      [mockFlight].forEach((f) =>
        expect(f).not.toHaveProperty('arrivalTimeInWIB'),
      );
      [mockFlight].forEach((f) =>
        f.prices.forEach((p) => expect(p).toHaveProperty('seatLeft')),
      );
    });
  });

  describe('reformattingFlightAirport', () => {
    let mockDeparture: IAirport;
    let mockDestination: IAirport;
    let mockAirplane: IAirplane;
    let mockPrice: IPrice;
    let mockFlight: IFlight;
    let mockFlightCandidateOrigin: IFlightCandidateResponse;

    beforeEach(() => {
      mockDeparture = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockDestination = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockDeparture,
        destination: mockDestination,
        airplane: mockAirplane,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
      };

      mockPrice = {
        id: faker.number.int({ min: 1 }),
        flight: mockFlight.id,
        seatClass: faker.helpers.enumValue(ESeatClass),
        priceConfig: faker.number.int({ min: 500000 }),
      };

      mockFlight.prices = [mockPrice];

      mockFlightCandidateOrigin = {
        id: mockFlight.id,
        code: mockFlight.code,
        departure: mockFlight.departure as IAirport,
        destination: mockFlight.destination as IAirport,
        airplane: mockFlight.airplane as IAirplane,
        departureTimeInWIB: mockFlight.departureTime,
        arrivalTimeInWIB: mockFlight.arrivalTime,
        durationInMinutes: mockFlight.durationInMinutes,
        prices: [mockPrice],
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return reformatted response flight airport in timezone WIB', async () => {
      // arrange
      mockDeparture.timezone = ETimezone.WIB;
      mockDestination.timezone = ETimezone.WIB;

      const newDepartureTime = mockFlight.departureTime;
      const newDepartureTimezone = EInitialTimezone.WIB;
      const newArrivalTime = mockFlight.arrivalTime;
      const newArrivalTimezone = EInitialTimezone.WIB;

      const newDeparture = { ...mockDeparture, timezone: newDepartureTimezone };
      const newDestination = {
        ...mockDestination,
        timezone: newArrivalTimezone,
      };

      delete mockPrice.id;
      delete mockPrice.flight;
      delete mockFlight.airplane;

      const expectedResult = {
        ...mockFlight,
        departure: newDeparture,
        destination: newDestination,
        departureTime: newDepartureTime,
        arrivalTime: newArrivalTime,
      };

      // act
      const reformattingFlightAirport = service.reformattingFlightAirport(
        mockFlightCandidateOrigin,
      );

      // assert
      expect(reformattingFlightAirport).toEqual(expectedResult);
    });

    it('should return reformatted response flight airport in timezone WITA', async () => {
      // arrange
      mockDeparture.timezone = ETimezone.WITA;
      mockDestination.timezone = ETimezone.WITA;
      mockFlightCandidateOrigin.departureTimeInWIB = '10:10:00';
      mockFlightCandidateOrigin.arrivalTimeInWIB = '12:15:00';

      const newDepartureTime = '11:10:00';
      const newDepartureTimezone = EInitialTimezone.WITA;
      const newArrivalTime = '13:15:00';
      const newArrivalTimezone = EInitialTimezone.WITA;

      const newDeparture = { ...mockDeparture, timezone: newDepartureTimezone };
      const newDestination = {
        ...mockDestination,
        timezone: newArrivalTimezone,
      };

      delete mockPrice.id;
      delete mockPrice.flight;
      delete mockFlight.airplane;

      const expectedResult = {
        ...mockFlight,
        departure: newDeparture,
        destination: newDestination,
        departureTime: newDepartureTime,
        arrivalTime: newArrivalTime,
      };

      // act
      const reformattingFlightAirport = service.reformattingFlightAirport(
        mockFlightCandidateOrigin,
      );

      // assert
      expect(reformattingFlightAirport).toEqual(expectedResult);
    });

    it('should return reformatted response flight airport in timezone WIT', async () => {
      // arrange
      mockDeparture.timezone = ETimezone.WIT;
      mockDestination.timezone = ETimezone.WIT;
      mockFlightCandidateOrigin.departureTimeInWIB = '09:18:00';
      mockFlightCandidateOrigin.arrivalTimeInWIB = '11:25:00';

      const newDepartureTime = '11:18:00';
      const newDepartureTimezone = EInitialTimezone.WIT;
      const newArrivalTime = '13:25:00';
      const newArrivalTimezone = EInitialTimezone.WIT;

      const newDeparture = { ...mockDeparture, timezone: newDepartureTimezone };
      const newDestination = {
        ...mockDestination,
        timezone: newArrivalTimezone,
      };

      delete mockPrice.id;
      delete mockPrice.flight;
      delete mockFlight.airplane;

      const expectedResult = {
        ...mockFlight,
        departure: newDeparture,
        destination: newDestination,
        departureTime: newDepartureTime,
        arrivalTime: newArrivalTime,
      };

      // act
      const reformattingFlightAirport = service.reformattingFlightAirport(
        mockFlightCandidateOrigin,
      );

      // assert
      expect(reformattingFlightAirport).toEqual(expectedResult);
    });
  });

  describe('reformattingAvailableSeat', () => {
    let mockSeat: ISeat;
    let mockAirplane: IAirplane;
    let mockFlight: IFlight;
    let mockFlightSeatOrigin: IFlightSeatResponse;

    beforeEach(() => {
      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        seats: [mockSeat],
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        airplane: mockAirplane,
      };

      mockFlightSeatOrigin = {
        id: mockFlight.id,
        code: mockFlight.code,
        departure: faker.number.int({ min: 1 }),
        destination: faker.number.int({ min: 1 }),
        airplane: mockFlight.airplane as IAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 1 }),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return reformatted response flight with seats are available', async () => {
      // arrange
      const occupied = [];
      delete mockAirplane.id;

      const expectedResult = {
        id: mockFlightSeatOrigin.id,
        code: mockFlightSeatOrigin.code,
        airplane: mockAirplane,
      };

      // act
      const reformattingAvailableSeat = service.reformattingAvailableSeat(
        mockFlightSeatOrigin,
        occupied,
      );

      // assert
      expect(reformattingAvailableSeat).toEqual(expectedResult);
    });
  });

  describe('reformattingInfoBaggage', () => {
    let mockFlight: IFlight;
    let mockFlightWithBaggageOrigin: IFlightBaggageResponse;
    let mockBaggage: IBaggage;

    beforeEach(() => {
      mockBaggage = {
        id: faker.number.int({ min: 1 }),
        capacity: faker.number.int({ min: 1 }),
        category: faker.helpers.arrayElement([
          'Cabin',
          'Normal',
          'Over',
          'Extra Over',
        ]),
        price: faker.number.int({ min: 0 }),
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: faker.number.int({ min: 1 }),
        destination: faker.number.int({ min: 1 }),
        airplane: faker.number.int({ min: 1 }),
      };

      mockFlightWithBaggageOrigin = {
        id: mockFlight.id,
        code: mockFlight.code,
        departure: mockFlight.departure as number,
        destination: mockFlight.destination as number,
        airplane: mockFlight.airplane as number,
        departureTimeInWIB: mockFlight.departureTime,
        arrivalTimeInWIB: mockFlight.arrivalTime,
        durationInMinutes: mockFlight.durationInMinutes,
        baggages: [mockBaggage],
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return reformatted response flight with baggage', async () => {
      // arrange
      delete mockFlight.departure;
      delete mockFlight.destination;
      delete mockFlight.airplane;

      const newBaggages = mockFlightWithBaggageOrigin.baggages.map(
        ({ id, ...baggage }) => baggage,
      );

      mockFlight.baggages = newBaggages;

      // act
      const reformattingInfoBaggage = service.reformattingInfoBaggage(
        mockFlightWithBaggageOrigin,
      );

      // assert
      expect(reformattingInfoBaggage).toEqual(mockFlight);
    });
  });

  describe('reformattingCurrentTimeFollowTimezone', () => {
    it('should return today in WIB', async () => {
      // arrange
      const timezone = ETimezone.WIB;
      const mockToday = new Date();
      const mockResult = new Date(mockToday.getTime() - -420 * 60 * 1000);
      const mockResultString = mockResult
        .toISOString()
        .slice(0, 19)
        .replace('T', ' ');

      // act
      const reformattingCurrentTimeFollowTimezone =
        service.reformattingCurrentTimeFollowTimezone(timezone);

      // assert
      expect(reformattingCurrentTimeFollowTimezone).toEqual(mockResultString);
    });

    it('should return today in WITA', async () => {
      // arrange
      const timezone = ETimezone.WITA;
      const mockToday = new Date();
      const mockResult = new Date(mockToday.getTime() - -480 * 60 * 1000);
      const mockResultString = mockResult
        .toISOString()
        .slice(0, 19)
        .replace('T', ' ');

      // act
      const reformattingCurrentTimeFollowTimezone =
        service.reformattingCurrentTimeFollowTimezone(timezone);

      // assert
      expect(reformattingCurrentTimeFollowTimezone).toEqual(mockResultString);
    });

    it('should return today in WIT', async () => {
      // arrange
      const timezone = ETimezone.WIT;
      const mockToday = new Date();
      const mockResult = new Date(mockToday.getTime() - -540 * 60 * 1000);
      const mockResultString = mockResult
        .toISOString()
        .slice(0, 19)
        .replace('T', ' ');

      // act
      const reformattingCurrentTimeFollowTimezone =
        service.reformattingCurrentTimeFollowTimezone(timezone);

      // assert
      expect(reformattingCurrentTimeFollowTimezone).toEqual(mockResultString);
    });
  });

  describe('reformattingUTCTimeFollowTimezone', () => {
    const utcString = faker.date.anytime().toISOString();

    it('should return formatted time string for WIB timezone', () => {
      // arrange
      const timezone = ETimezone.WIB;
      const utcDate = new Date(utcString);
      const expectedAdjustedDate = new Date(
        utcDate.getTime() - -420 * 60 * 1000,
      );
      const expectedResult = expectedAdjustedDate
        .toISOString()
        .slice(0, 19)
        .replace('T', ' ');
      // act
      const result = service.reformattingUTCTimeFollowTimezone(
        utcString,
        timezone,
      );

      // assert
      expect(result).toEqual(expectedResult);
    });

    it('should return formatted time string for WITA timezone', () => {
      // arrange
      const timezone = ETimezone.WITA;
      const utcDate = new Date(utcString);
      const expectedAdjustedDate = new Date(
        utcDate.getTime() - -480 * 60 * 1000,
      );
      const expectedResult = expectedAdjustedDate
        .toISOString()
        .slice(0, 19)
        .replace('T', ' ');
      // act
      const result = service.reformattingUTCTimeFollowTimezone(
        utcString,
        timezone,
      );

      // assert
      expect(result).toEqual(expectedResult);
    });

    it('should return formatted time string for WIT timezone', () => {
      // arrange
      const timezone = ETimezone.WIT;
      const utcDate = new Date(utcString);
      const expectedAdjustedDate = new Date(
        utcDate.getTime() - -540 * 60 * 1000,
      );
      const expectedResult = expectedAdjustedDate
        .toISOString()
        .slice(0, 19)
        .replace('T', ' ');
      // act
      const result = service.reformattingUTCTimeFollowTimezone(
        utcString,
        timezone,
      );

      // assert
      expect(result).toEqual(expectedResult);
    });
  });

  describe('generatePaymentTypeForBank', () => {
    it('should return payment type BCA', async () => {
      // arrange
      const bank = EBankChoice.BCA;
      const expectedResult = {
        payment_type: 'bank_transfer',
        bank_transfer: {
          bank: 'bca',
        },
      };

      // act
      const generatePaymentTypeForBank =
        service.generatePaymentTypeForBank(bank);

      // assert
      expect(generatePaymentTypeForBank).toEqual(expectedResult);
    });

    it('should return payment type BNI', async () => {
      // arrange
      const bank = EBankChoice.BNI;
      const expectedResult = {
        payment_type: 'bank_transfer',
        bank_transfer: {
          bank: 'bni',
        },
      };

      // act
      const generatePaymentTypeForBank =
        service.generatePaymentTypeForBank(bank);

      // assert
      expect(generatePaymentTypeForBank).toEqual(expectedResult);
    });

    it('should return payment type BRI', async () => {
      // arrange
      const bank = EBankChoice.BRI;
      const expectedResult = {
        payment_type: 'bank_transfer',
        bank_transfer: {
          bank: 'bri',
        },
      };

      // act
      const generatePaymentTypeForBank =
        service.generatePaymentTypeForBank(bank);

      // assert
      expect(generatePaymentTypeForBank).toEqual(expectedResult);
    });

    it('should return payment type PERMATA', async () => {
      // arrange
      const bank = EBankChoice.PERMATA;
      const expectedResult = {
        payment_type: 'permata',
      };

      // act
      const generatePaymentTypeForBank =
        service.generatePaymentTypeForBank(bank);

      // assert
      expect(generatePaymentTypeForBank).toEqual(expectedResult);
    });

    it('should return payment type MANDIRI', async () => {
      // arrange
      const bank = EBankChoice.MANDIRI;
      const expectedResult = {
        payment_type: 'echannel',
        echannel: {
          bill_info1: 'Payment:',
          bill_info2: 'Online purchase',
        },
      };

      // act
      const generatePaymentTypeForBank =
        service.generatePaymentTypeForBank(bank);

      // assert
      expect(generatePaymentTypeForBank).toEqual(expectedResult);
    });
  });

  describe('generateResponseCharge', () => {
    let mockChargeResponseOrigin: IChargeResponse;

    beforeEach(() => {
      mockChargeResponseOrigin = {
        status_code: faker.helpers.arrayElement(['200', '201']),
        status_message: faker.string.sample(),
        transaction_id: faker.string.uuid(),
        order_id: faker.string.uuid(),
        merchant_id: faker.string.sample(),
        gross_amount: faker.number.int({ min: 1 }).toString(),
        currency: 'IDR',
        payment_type: faker.string.sample(),
        transaction_time: faker.date.past().toISOString(),
        transaction_status: 'pending',
        fraud_status: 'false',
        permata_va_number: faker.string.numeric({ length: 15 }),
        va_numbers: [
          {
            bank: faker.helpers.arrayElement([
              'bni',
              'bca',
              'bri',
              'mandiri',
              'permata',
            ]),
            va_number: faker.string.numeric({ length: 15 }),
          },
        ],
        bill_key: faker.string.numeric({ length: 15 }),
        biller_code: faker.string.numeric({ length: 15 }),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return payment type BCA', async () => {
      // arrange
      const bank = EBankChoice.BCA;
      const expectedResult = {
        bank,
        virtualAccountNumber: mockChargeResponseOrigin.va_numbers[0].va_number,
      };

      // act
      const generateResponseCharge = service.generateResponseCharge(
        bank,
        mockChargeResponseOrigin,
      );

      // assert
      expect(generateResponseCharge).toEqual(expectedResult);
    });

    it('should return payment type BNI', async () => {
      // arrange
      const bank = EBankChoice.BNI;
      const expectedResult = {
        bank,
        virtualAccountNumber: mockChargeResponseOrigin.va_numbers[0].va_number,
      };

      // act
      const generateResponseCharge = service.generateResponseCharge(
        bank,
        mockChargeResponseOrigin,
      );

      // assert
      expect(generateResponseCharge).toEqual(expectedResult);
    });

    it('should return payment type BRI', async () => {
      // arrange
      const bank = EBankChoice.BRI;
      const expectedResult = {
        bank,
        virtualAccountNumber: mockChargeResponseOrigin.va_numbers[0].va_number,
      };

      // act
      const generateResponseCharge = service.generateResponseCharge(
        bank,
        mockChargeResponseOrigin,
      );

      // assert
      expect(generateResponseCharge).toEqual(expectedResult);
    });

    it('should return payment type PERMATA', async () => {
      // arrange
      const bank = EBankChoice.PERMATA;
      const expectedResult = {
        bank,
        virtualAccountNumber: mockChargeResponseOrigin.permata_va_number,
      };

      // act
      const generateResponseCharge = service.generateResponseCharge(
        bank,
        mockChargeResponseOrigin,
      );

      // assert
      expect(generateResponseCharge).toEqual(expectedResult);
    });

    it('should return payment type MANDIRI', async () => {
      // arrange
      const bank = EBankChoice.MANDIRI;
      const expectedResult = {
        bank,
        billerCode: mockChargeResponseOrigin.biller_code,
        billerKey: mockChargeResponseOrigin.bill_key,
      };

      // act
      const generateResponseCharge = service.generateResponseCharge(
        bank,
        mockChargeResponseOrigin,
      );

      // assert
      expect(generateResponseCharge).toEqual(expectedResult);
    });
  });

  describe('calculateDuration', () => {
    it('should return duration in minutes when flight time in WIB', () => {
      // arrange
      const flightTime = '2023-06-07 10:10:10';
      const timezoneCode = ETimezoneCode.WIB;
      const redeemTimeUTC = new Date('2023-06-07 10:00:10');
      const expectedResult = 10;

      // act
      const calculateDuration = service.calculateDuration(
        redeemTimeUTC,
        flightTime,
        timezoneCode,
      );

      // assert
      expect(calculateDuration).toEqual(expectedResult);
    });

    it('should return duration in minutes when flight time in WITA', () => {
      // arrange
      const flightTime = '2023-06-07 09:10:10';
      const timezoneCode = ETimezoneCode.WITA;
      const flightTimeUTC = new Date(flightTime);
      const adjustedWithTimezone = new Date(
        flightTimeUTC.getTime() - 1 * 60 * 60 * 1000,
      );
      const redeemTimeUTC = new Date('2023-06-07 10:00:10');
      const expectedResult = Math.round(
        (adjustedWithTimezone.getTime() - redeemTimeUTC.getTime()) /
          (1000 * 60),
      );

      // act
      const calculateDuration = service.calculateDuration(
        redeemTimeUTC,
        flightTime,
        timezoneCode,
      );

      // assert
      expect(calculateDuration).toEqual(expectedResult);
    });

    it('should return duration in minutes when flight time in WIT', () => {
      // arrange
      const flightTime = '2023-06-07 08:10:10';
      const timezoneCode = ETimezoneCode.WIT;
      const flightTimeUTC = new Date(flightTime);
      const adjustedWithTimezone = new Date(
        flightTimeUTC.getTime() - 2 * 60 * 60 * 1000,
      );
      const redeemTimeUTC = new Date('2023-06-07 10:00:10');
      const expectedResult = Math.round(
        (adjustedWithTimezone.getTime() - redeemTimeUTC.getTime()) /
          (1000 * 60),
      );

      // act
      const calculateDuration = service.calculateDuration(
        redeemTimeUTC,
        flightTime,
        timezoneCode,
      );

      // assert
      expect(calculateDuration).toEqual(expectedResult);
    });
  });
});
