import { Injectable, Logger } from '@nestjs/common';
import { EBankChoice } from 'src/enum/bank-choice.enum';
import { EInitialTimezone } from 'src/enum/initial-timezone.enum';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { ETimezoneCode } from 'src/enum/timezone-code.enum';
import { ETimezone } from 'src/enum/timezone.enum';
import { IStartProcess } from 'src/interface/bpm-service/start-process.interface';
import { IChargeTransaction } from 'src/interface/charge-transaction.interface';
import { IFlight } from 'src/interface/core-service/flight.interface';
import { IPrice } from 'src/interface/core-service/price.interface';
import { IFlightBaggageResponse } from 'src/interface/core-service/response/flight-baggage-response.interface';
import { IFlightCandidateResponse } from 'src/interface/core-service/response/flight-candidate-response.interface';
import { IFlightSeatResponse } from 'src/interface/core-service/response/flight-seat-response.interface';
import { IOccupiedFlightCandidate } from 'src/interface/occupied-flight-candidate.interface';
import { IChargeResponse } from 'src/interface/payment-service/charge-response.interface';

@Injectable()
export class TranslateService {
  private readonly logger = new Logger(TranslateService.name);

  private __addNHours(timeString: string, add: number): string {
    const timeStringArrays = timeString.split(':');
    const hour = Number(timeStringArrays[0]);
    let newHour = hour + add;
    if (newHour >= 24) {
      newHour = newHour - 24;
    }
    const newHourString = String(newHour).padStart(2, '0');
    timeStringArrays[0] = newHourString;
    return timeStringArrays.join(':');
  }

  reformattingFlightCandate(
    flights: IFlightCandidateResponse[],
    occupations: IOccupiedFlightCandidate[],
    flightDate: string,
  ) {
    const newFlights = flights.map(
      ({
        departureTimeInWIB,
        arrivalTimeInWIB,
        airplane,
        prices,
        ...flight
      }) => {
        const departureTimeZone = flight.departure.timezone;
        let newDepartureTime = departureTimeInWIB;
        if (departureTimeZone === ETimezone.WITA) {
          newDepartureTime = this.__addNHours(departureTimeInWIB, 1);
        } else if (departureTimeZone === ETimezone.WIT) {
          newDepartureTime = this.__addNHours(departureTimeInWIB, 2);
        }

        const arrivalTimeZone = flight.destination.timezone;
        let newArrivalTime = arrivalTimeInWIB;
        if (arrivalTimeZone === ETimezone.WITA) {
          newArrivalTime = this.__addNHours(arrivalTimeInWIB, 1);
        } else if (arrivalTimeZone === ETimezone.WIT) {
          newArrivalTime = this.__addNHours(arrivalTimeInWIB, 2);
        }

        const { id, ...newAirplane } = airplane;

        const newPrices = prices.map(({ id, ...price }) => {
          delete price.flight; // tidak bisa didestructure, name flight ambigu

          let remains =
            price.seatClass === ESeatClass.BUSINESS
              ? airplane.maxBusiness
              : airplane.maxEconomy;

          let flightOccupation: IOccupiedFlightCandidate;

          if (price.seatClass === ESeatClass.BUSINESS) {
            flightOccupation = occupations.find(
              (occ) =>
                occ.flight === flight.id &&
                occ.seatClass === ESeatClass.BUSINESS,
            );
          }

          if (price.seatClass === ESeatClass.ECONOMY) {
            flightOccupation = occupations.find(
              (occ) =>
                occ.flight === flight.id &&
                occ.seatClass === ESeatClass.ECONOMY,
            );
          }

          remains = flightOccupation
            ? remains - Number(flightOccupation.occupied)
            : remains;

          return {
            ...price,
            seatLeft: remains,
          } as IPrice;
        });

        this.logger.log('Reformat data flight candidates');
        return {
          ...flight,
          flightDate,
          departureTime: newDepartureTime,
          arrivalTime: newArrivalTime,
          airplane: newAirplane,
          prices: newPrices,
        } as IFlight;
      },
    );
    return newFlights;
  }

  reformattingFlightAirport(flight: IFlightCandidateResponse) {
    const {
      departure,
      destination,
      departureTimeInWIB,
      arrivalTimeInWIB,
      airplane,
      ...resFlight
    } = flight;
    const departureTimeZone = departure.timezone;
    let newDepartureTime = departureTimeInWIB;
    let newDepartureTimezone = EInitialTimezone.WIB;
    if (departureTimeZone === ETimezone.WITA) {
      newDepartureTime = this.__addNHours(departureTimeInWIB, 1);
      newDepartureTimezone = EInitialTimezone.WITA;
    } else if (departureTimeZone === ETimezone.WIT) {
      newDepartureTime = this.__addNHours(departureTimeInWIB, 2);
      newDepartureTimezone = EInitialTimezone.WIT;
    }

    const arrivalTimeZone = destination.timezone;
    let newArrivalTime = arrivalTimeInWIB;
    let newArrivalTimezone = EInitialTimezone.WIB;
    if (arrivalTimeZone === ETimezone.WITA) {
      newArrivalTime = this.__addNHours(arrivalTimeInWIB, 1);
      newArrivalTimezone = EInitialTimezone.WITA;
    } else if (arrivalTimeZone === ETimezone.WIT) {
      newArrivalTime = this.__addNHours(arrivalTimeInWIB, 2);
      newArrivalTimezone = EInitialTimezone.WIT;
    }

    const newDeparture = {
      ...departure,
      timezone: newDepartureTimezone,
    };

    const newDestination = {
      ...destination,
      timezone: newArrivalTimezone,
    };

    return {
      ...resFlight,
      departure: newDeparture,
      destination: newDestination,
      departureTime: newDepartureTime,
      arrivalTime: newArrivalTime,
    };
  }

  reformattingInfoBaggage(flight: IFlightBaggageResponse) {
    const { id, code, baggages } = flight;
    const newBaggages = baggages.map(({ id, ...baggage }) => baggage);
    const newFlight: IFlight = {
      id,
      code,
      baggages: newBaggages,
    };
    return newFlight;
  }

  reformattingAvailableSeat(
    flightSeats: IFlightSeatResponse,
    occupied: number[],
  ) {
    const {
      airplane: { seats },
    } = flightSeats;
    const seatConfigs = seats;
    const seatAvailables = seatConfigs.filter(
      (seat) => !occupied.includes(seat.id),
    );

    const seatWithoutAirPlane = seatAvailables.map(
      ({ airplane, ...seat }) => seat,
    );

    const { id, ...newAirplane } = flightSeats.airplane;
    newAirplane.seats = seatWithoutAirPlane;

    const newFlight: IFlight = {
      id: flightSeats.id,
      code: flightSeats.code,
      airplane: newAirplane,
    };

    return newFlight;
  }

  reformattingCurrentTimeFollowTimezone(timezone: ETimezone): string {
    const today = new Date();

    let result: Date;
    if (timezone === ETimezone.WIB) {
      result = new Date(today.getTime() - -420 * 60 * 1000);
    } else if (timezone === ETimezone.WITA) {
      result = new Date(today.getTime() - -480 * 60 * 1000);
    } else {
      result = new Date(today.getTime() - -540 * 60 * 1000);
    }
    return result.toISOString().slice(0, 19).replace('T', ' ');
  }

  reformattingLast30MinutesInWIB(): string {
    const currentTime = new Date();
    const currentTimeInWIB = new Date(currentTime.getTime() - -420 * 60 * 1000);
    const last30Minutes = new Date(
      currentTimeInWIB.setTime(currentTimeInWIB.getTime() - 30 * 60 * 1000),
    );
    return last30Minutes.toISOString().slice(0, 19).replace('T', ' ');
  }

  reformattingUTCTimeFollowTimezone(
    utcString: string,
    timezone: ETimezone,
  ): string {
    const utcDate = new Date(utcString);
    let result: Date;
    if (timezone === ETimezone.WIB) {
      result = new Date(utcDate.getTime() - -420 * 60 * 1000);
    } else if (timezone === ETimezone.WITA) {
      result = new Date(utcDate.getTime() - -480 * 60 * 1000);
    } else {
      result = new Date(utcDate.getTime() - -540 * 60 * 1000);
    }
    return result.toISOString().slice(0, 19).replace('T', ' ');
  }

  reformattingStartProcessWorkflow(dataWorkflow: IStartProcess) {
    return {
      variables: {
        identityNumber: {
          value: dataWorkflow.identityNumber,
          type: 'String',
        },
        name: {
          value: dataWorkflow.name,
          type: 'String',
        },
        birthDate: {
          value: dataWorkflow.birthDate,
          type: 'String',
        },
        isMember: {
          value: dataWorkflow.isMember,
          type: 'String',
        },
        memberId: {
          value: dataWorkflow.memberId,
          type: 'Integer',
        },
        email: {
          value: dataWorkflow.email,
          type: 'String',
        },
        flightCode: {
          value: dataWorkflow.flightCode,
          type: 'String',
        },
        flightDate: {
          value: dataWorkflow.flightDate,
          type: 'String',
        },
        departureAirport: {
          value: dataWorkflow.departureAirport,
          type: 'String',
        },
        departureCode: {
          value: dataWorkflow.departureCode,
          type: 'String',
        },
        departureTime: {
          value: dataWorkflow.departureTime,
          type: 'String',
        },
        departureTimezone: {
          value: dataWorkflow.departureTimezone,
          type: 'String',
        },
        destination: {
          value: dataWorkflow.destination,
          type: 'String',
        },
        destinationAirport: {
          value: dataWorkflow.destinationAirport,
          type: 'String',
        },
        destinationCode: {
          value: dataWorkflow.destinationCode,
          type: 'String',
        },
        arrivalTime: {
          value: dataWorkflow.arrivalTime,
          type: 'String',
        },
        arrivalTimezone: {
          value: dataWorkflow.arrivalTimezone,
          type: 'String',
        },
        seatNumber: {
          value: dataWorkflow.seatNumber,
          type: 'String',
        },
        reservationTime: {
          value: dataWorkflow.reservationTime,
          type: 'String',
        },
        timezone: {
          value: dataWorkflow.timezone,
          type: 'String',
        },
      },
      businessKey: dataWorkflow.bussinessKey,
    };
  }

  generatePaymentTypeForBank(bank: EBankChoice) {
    if (bank === EBankChoice.BCA) {
      return {
        payment_type: 'bank_transfer',
        bank_transfer: {
          bank: 'bca',
        },
      };
    }

    if (bank === EBankChoice.BNI) {
      return {
        payment_type: 'bank_transfer',
        bank_transfer: {
          bank: 'bni',
        },
      };
    }

    if (bank === EBankChoice.BRI) {
      return {
        payment_type: 'bank_transfer',
        bank_transfer: {
          bank: 'bri',
        },
      };
    }

    if (bank === EBankChoice.MANDIRI) {
      return {
        payment_type: 'echannel',
        echannel: {
          bill_info1: 'Payment:',
          bill_info2: 'Online purchase',
        },
      };
    }

    if (bank === EBankChoice.PERMATA) {
      return {
        payment_type: 'permata',
      };
    }
  }

  generateResponseCharge(
    bank: EBankChoice,
    originResponse: IChargeResponse,
  ): IChargeTransaction {
    if (
      bank === EBankChoice.BCA ||
      bank === EBankChoice.BNI ||
      bank === EBankChoice.BRI
    ) {
      return {
        bank,
        virtualAccountNumber: originResponse.va_numbers[0].va_number,
      };
    }

    if (bank === EBankChoice.PERMATA) {
      return {
        bank,
        virtualAccountNumber: originResponse.permata_va_number,
      };
    }

    if (bank === EBankChoice.MANDIRI) {
      return {
        bank,
        billerCode: originResponse.biller_code,
        billerKey: originResponse.bill_key,
      };
    }
  }

  calculateDuration(
    redeemTimeUTC: Date,
    flightTime: string,
    timezoneCode: ETimezoneCode,
  ) {
    const flightTimeUTC = new Date(flightTime);

    let adjustedWithTimezone: Date;
    if (timezoneCode === ETimezoneCode.WIB) {
      adjustedWithTimezone = new Date(flightTimeUTC);
    } else if (timezoneCode === ETimezoneCode.WITA) {
      adjustedWithTimezone = new Date(
        flightTimeUTC.getTime() - 1 * 60 * 60 * 1000,
      );
    } else {
      adjustedWithTimezone = new Date(
        flightTimeUTC.getTime() - 2 * 60 * 60 * 1000,
      );
    }

    const msInMinutes = 1000 * 60;
    return Math.round(
      (adjustedWithTimezone.getTime() - redeemTimeUTC.getTime()) / msInMinutes,
    );
  }
}
