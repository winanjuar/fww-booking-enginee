import { Test, TestingModule } from '@nestjs/testing';
import { InquiryService } from './inquiry.service';
import { CoreService } from './core.service';
import { TranslateService } from './translate.service';
import { SeatStatusRepository } from 'src/repository/seat-status.repository';
import { FlightCandidateDto } from 'src/dto/flight-candidate.dto';
import { faker, fakerID_ID } from '@faker-js/faker';
import { ETimezone } from 'src/enum/timezone.enum';
import { IFlightCandidateResponse } from 'src/interface/core-service/response/flight-candidate-response.interface';
import { IPrice } from 'src/interface/core-service/price.interface';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { IAirport } from 'src/interface/core-service/airport.interface';
import { IAirplane } from 'src/interface/core-service/airplane.interface';
import { IOccupiedFlightCandidate } from 'src/interface/occupied-flight-candidate.interface';
import { IFlight } from 'src/interface/core-service/flight.interface';
import {
  BadRequestException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { IFlightBaggageResponse } from 'src/interface/core-service/response/flight-baggage-response.interface';
import { IBaggage } from 'src/interface/core-service/baggage.interface';
import { ISeat } from 'src/interface/core-service/seat.interface';
import { IFlightSeatResponse } from 'src/interface/core-service/response/flight-seat-response.interface';
import { ISeatValidationResponse } from 'src/interface/core-service/response/seat-validation-response.interface';
import { ESeatSide } from 'src/enum/seat-side.enum';
import { ESeatPosition } from 'src/enum/seat-position.enum';
import { SeatDto } from 'src/dto/seat.dto';
import { ESeatStatus } from 'src/enum/seat-status.enum';
import { SeatStatus } from 'src/entity/seat-status.entity';
import { async } from 'rxjs';
import { ChoseSeatDto } from 'src/dto/chose-seat.dto';
import { IChoseSeatResult } from 'src/interface/chose-seat-result.interface';

describe('InquiryService', () => {
  let service: InquiryService;

  const coreService = {
    fetchFlightCandidate: jest.fn(),
    fetchFlightBaggage: jest.fn(),
    fetchFlightSeat: jest.fn(),
    validateSeat: jest.fn(),
  };

  const translateService = {
    reformattingCurrentTimeFollowTimezone: jest.fn(),
    reformattingFlightCandate: jest.fn(),
    reformattingInfoBaggage: jest.fn(),
    reformattingAvailableSeat: jest.fn(),
  };

  const seatStatusRepo = {
    getOccupationSummary: jest.fn(),
    getOccupied: jest.fn(),
    checkAnotherLock: jest.fn(),
    requestChooseSeat: jest.fn(),
    requestUnchooseSeat: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        InquiryService,
        { provide: CoreService, useValue: coreService },
        { provide: TranslateService, useValue: translateService },
        { provide: SeatStatusRepository, useValue: seatStatusRepo },
      ],
    }).compile();
    module.useLogger(false);
    service = module.get<InquiryService>(InquiryService);
  });

  afterEach(() => jest.clearAllMocks());

  describe('getFlightCandidate', () => {
    let mockFlightDto: FlightCandidateDto;
    let mockPrice: IPrice;
    let mockDeparture: IAirport;
    let mockDestination: IAirport;
    let mockAirplane: IAirplane;
    let mockFlight: IFlight;
    let mockFlightCandidateResponse: IFlightCandidateResponse;
    let mockOccupiedFlightCandidate: IOccupiedFlightCandidate;

    beforeEach(() => {
      mockFlightDto = {
        departure: faker.number.int({ min: 1 }),
        destination: faker.number.int({ min: 1 }),
        flightDate: faker.date.future().toISOString().split('T')[0],
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockDeparture = {
        id: faker.number.int({ min: 1 }),
        code: fakerID_ID.airline.airport().iataCode,
        name: fakerID_ID.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockDestination = {
        id: faker.number.int({ min: 1 }),
        code: fakerID_ID.airline.airport().iataCode,
        name: fakerID_ID.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
      };

      mockPrice = {
        id: faker.number.int({ min: 1 }),
        seatClass: faker.helpers.enumValue(ESeatClass),
        priceConfig: faker.number.int({ min: 500000 }),
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockDeparture,
        destination: mockDestination,
        airplane: mockAirplane,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
      };

      mockFlightCandidateResponse = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockDeparture,
        destination: mockDestination,
        airplane: mockAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        prices: [mockPrice],
      };

      mockOccupiedFlightCandidate = {
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: mockFlight.id,
        seatClass: mockPrice.seatClass,
        occupied: faker.number.int({ min: 1, max: 80 }),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return list flight candidate', async () => {
      // arrange
      const reformattingCurrentTimeFollowTimezone = jest
        .spyOn(translateService, 'reformattingCurrentTimeFollowTimezone')
        .mockReturnValue(mockFlightDto.flightDate);

      const fetchFlightCandidate = jest
        .spyOn(coreService, 'fetchFlightCandidate')
        .mockResolvedValue(mockFlightCandidateResponse);

      const getOccupationSummary = jest
        .spyOn(seatStatusRepo, 'getOccupationSummary')
        .mockResolvedValue(mockOccupiedFlightCandidate);

      const reformattingFlightCandate = jest
        .spyOn(translateService, 'reformattingFlightCandate')
        .mockReturnValue([mockFlight]);

      // act
      const result = await service.getFlightCandidate(mockFlightDto);

      // assert
      expect(result).toEqual([mockFlight]);
      expect(reformattingCurrentTimeFollowTimezone).toBeCalledTimes(1);
      expect(fetchFlightCandidate).toBeCalledTimes(1);
      expect(getOccupationSummary).toBeCalledTimes(1);
      expect(reformattingFlightCandate).toBeCalledTimes(1);
    });

    it('should throw bad request exception', async () => {
      // arrange
      mockFlightDto.flightDate = '2023-06-05';
      const todayString = '2023-06-06';

      const reformattingCurrentTimeFollowTimezone = jest
        .spyOn(translateService, 'reformattingCurrentTimeFollowTimezone')
        .mockReturnValue(todayString);

      const fetchFlightCandidate = jest
        .spyOn(coreService, 'fetchFlightCandidate')
        .mockResolvedValue(mockFlightCandidateResponse);

      const getOccupationSummary = jest
        .spyOn(seatStatusRepo, 'getOccupationSummary')
        .mockResolvedValue(mockOccupiedFlightCandidate);

      const reformattingFlightCandate = jest
        .spyOn(translateService, 'reformattingFlightCandate')
        .mockReturnValue([mockFlight]);

      // act
      const getFlightCandidate = service.getFlightCandidate(mockFlightDto);

      // assert
      expect(getFlightCandidate).rejects.toEqual(
        new BadRequestException(
          `flightDate should be greather than or equal ${todayString.slice(
            0,
            10,
          )}`,
        ),
      );
      expect(reformattingCurrentTimeFollowTimezone).toBeCalledTimes(1);
      expect(fetchFlightCandidate).toBeCalledTimes(0);
      expect(getOccupationSummary).toBeCalledTimes(0);
      expect(reformattingFlightCandate).toBeCalledTimes(0);
    });
  });

  describe('getInfoBaggage', () => {
    let mockFlight: IFlight;
    let mockFlightWithBaggageOrigin: IFlightBaggageResponse;
    let mockBaggage: IBaggage;

    it('should return a flight with baggage info', async () => {
      // arrange
      mockBaggage = {
        capacity: faker.number.int({ min: 1 }),
        category: faker.helpers.arrayElement([
          'Cabin',
          'Normal',
          'Over',
          'Extra Over',
        ]),
        price: faker.number.int({ min: 0 }),
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: faker.number.int({ min: 1 }),
        destination: faker.number.int({ min: 1 }),
        airplane: faker.number.int({ min: 1 }),
        baggages: [mockBaggage],
      };

      mockFlightWithBaggageOrigin = {
        id: mockFlight.id,
        code: mockFlight.code,
        departure: mockFlight.departure as number,
        destination: mockFlight.destination as number,
        airplane: mockFlight.airplane as number,
        departureTimeInWIB: mockFlight.departureTime,
        arrivalTimeInWIB: mockFlight.arrivalTime,
        durationInMinutes: mockFlight.durationInMinutes,
        baggages: [mockBaggage],
      };

      const fetchFlightBaggage = jest
        .spyOn(coreService, 'fetchFlightBaggage')
        .mockReturnValue(mockFlightWithBaggageOrigin);

      const reformattingInfoBaggage = jest
        .spyOn(translateService, 'reformattingInfoBaggage')
        .mockReturnValue(mockFlight);

      // act
      const result = await service.getInfoBaggage(
        mockFlightWithBaggageOrigin.id,
      );

      // assert
      expect(result).toEqual(mockFlight);
      expect(fetchFlightBaggage).toBeCalledTimes(1);
      expect(reformattingInfoBaggage).toBeCalledTimes(1);
    });
  });

  describe('getAvailableSeat', () => {
    let mockSeat: ISeat;
    let mockAirplane: IAirplane;
    let mockFlight: IFlight;
    let mockSeatStatus: SeatStatus;
    let mockFlightSeatOrigin: IFlightSeatResponse;
    let mockSeatDto: SeatDto;

    beforeEach(() => {
      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        seats: [mockSeat],
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        airplane: mockAirplane,
      };

      mockSeatStatus = {
        id: faker.number.int({ min: 1 }),
        passenger: faker.string.numeric({ length: 16 }),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: faker.number.int({ min: 1 }),
        departure: faker.number.int({ min: 1 }),
        destination: faker.number.int({ min: 1 }),
        airplane: faker.number.int({ min: 1 }),
        seat: faker.number.int({ min: 1 }),
        seatClass: faker.helpers.enumValue(ESeatClass),
        seatNumber: faker.airline.seat(),
        price: faker.number.int(),
        status: faker.helpers.enumValue(ESeatStatus),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockFlightSeatOrigin = {
        id: mockFlight.id,
        code: mockFlight.code,
        departure: faker.number.int({ min: 1 }),
        destination: faker.number.int({ min: 1 }),
        airplane: mockFlight.airplane as IAirplane,
        departureTimeInWIB: faker.date.anytime().toTimeString(),
        arrivalTimeInWIB: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 1 }),
      };

      mockSeatDto = {
        flight: mockFlight.id,
        seatClass: mockSeat.seatClass,
        flightDate: faker.date.future().toISOString().split('T')[0],
        timezone: faker.helpers.enumValue(ETimezone),
      };
    });

    it('should return a flight with available seats', async () => {
      // arrange
      const reformattingCurrentTimeFollowTimezone = jest
        .spyOn(translateService, 'reformattingCurrentTimeFollowTimezone')
        .mockReturnValue(mockSeatDto.flightDate);

      const fetchFlightSeat = jest
        .spyOn(coreService, 'fetchFlightSeat')
        .mockResolvedValue(mockFlightSeatOrigin);

      const getOccupied = jest
        .spyOn(seatStatusRepo, 'getOccupied')
        .mockResolvedValue([mockSeatStatus]);

      const reformattingAvailableSeat = jest
        .spyOn(translateService, 'reformattingAvailableSeat')
        .mockReturnValue(mockFlight);

      // act
      const result = await service.getAvailableSeat(mockSeatDto);

      // assert
      expect(result).toEqual(mockFlight);
      expect(reformattingCurrentTimeFollowTimezone).toBeCalledTimes(1);
      expect(fetchFlightSeat).toBeCalledTimes(1);
      expect(getOccupied).toBeCalledTimes(1);
      expect(reformattingAvailableSeat).toBeCalledTimes(1);
    });

    it('should throw bad request exception', async () => {
      // arrange
      mockSeatDto.flightDate = '2023-06-05';
      const todayString = '2023-06-06';

      const reformattingCurrentTimeFollowTimezone = jest
        .spyOn(translateService, 'reformattingCurrentTimeFollowTimezone')
        .mockReturnValue(todayString);

      const fetchFlightSeat = jest
        .spyOn(coreService, 'fetchFlightSeat')
        .mockResolvedValue(mockFlightSeatOrigin);

      const getOccupied = jest
        .spyOn(seatStatusRepo, 'getOccupied')
        .mockResolvedValue([mockSeatStatus]);

      const reformattingAvailableSeat = jest
        .spyOn(translateService, 'reformattingAvailableSeat')
        .mockReturnValue(mockFlight);

      // act
      const getAvailableSeat = service.getAvailableSeat(mockSeatDto);

      // assert
      expect(getAvailableSeat).rejects.toEqual(
        new BadRequestException(
          `flightDate should be greather than or equal ${todayString.slice(
            0,
            10,
          )}`,
        ),
      );
      expect(reformattingCurrentTimeFollowTimezone).toBeCalledTimes(1);
      expect(fetchFlightSeat).toBeCalledTimes(0);
      expect(getOccupied).toBeCalledTimes(0);
      expect(reformattingAvailableSeat).toBeCalledTimes(0);
    });
  });

  describe('chooseSeat', () => {
    let mockSeatDto: ChoseSeatDto;
    let mockSeatStatus: SeatStatus;
    let mockSeatValidateResponse: ISeatValidationResponse;
    let mockChoseSeatResult: IChoseSeatResult;

    beforeEach(() => {
      mockSeatDto = {
        passenger: faker.string.numeric({ length: 16 }),
        flightDate: faker.date.future().toISOString().split('T')[0],
        timezone: faker.helpers.enumValue(ETimezone),
        flight: faker.number.int({ min: 1 }),
        seat: faker.number.int({ min: 1 }),
      };

      mockSeatValidateResponse = {
        isValid: true,
        flight: mockSeatDto.flight,
        departure: faker.number.int({ min: 1 }),
        destination: faker.number.int({ min: 1 }),
        airplane: faker.number.int({ min: 1 }),
        seat: mockSeatDto.seat,
        seatClass: faker.helpers.enumValue(ESeatClass),
        seatNumber: faker.airline.seat(),
        price: faker.number.int({ min: 500000 }),
      };

      mockSeatStatus = {
        id: faker.number.int({ min: 1 }),
        passenger: mockSeatDto.passenger,
        flightDate: mockSeatDto.flightDate,
        flight: mockSeatValidateResponse.flight,
        departure: mockSeatValidateResponse.departure,
        destination: mockSeatValidateResponse.destination,
        airplane: mockSeatValidateResponse.airplane,
        seat: mockSeatValidateResponse.seat,
        seatClass: mockSeatValidateResponse.seatClass,
        seatNumber: mockSeatValidateResponse.seatNumber,
        price: mockSeatValidateResponse.price,
        status: faker.helpers.enumValue(ESeatStatus),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockChoseSeatResult = {
        passenger: mockSeatDto.passenger,
        flightDate: mockSeatDto.flightDate,
        flight: mockSeatDto.flight,
        seat: mockSeatDto.seat,
        status: ESeatStatus.SELECTED,
      };
    });

    it('should return seat choosen', async () => {
      // arrange
      const reformattingCurrentTimeFollowTimezone = jest
        .spyOn(translateService, 'reformattingCurrentTimeFollowTimezone')
        .mockReturnValue(mockSeatDto.flightDate);

      const checkAnotherLock = jest
        .spyOn(seatStatusRepo, 'checkAnotherLock')
        .mockReturnValue(null);

      mockSeatStatus.status = ESeatStatus.SELECTED;

      const validateSeat = jest
        .spyOn(coreService, 'validateSeat')
        .mockReturnValue(mockSeatValidateResponse);

      const requestChooseSeat = jest
        .spyOn(seatStatusRepo, 'requestChooseSeat')
        .mockReturnValue(mockSeatStatus);

      // act
      const result = await service.chooseSeat(mockSeatDto);

      // assert
      expect(result).toEqual(mockChoseSeatResult);
      expect(reformattingCurrentTimeFollowTimezone).toHaveBeenCalledTimes(1);
      expect(checkAnotherLock).toHaveBeenCalledTimes(1);
      expect(validateSeat).toHaveBeenCalledTimes(1);
      expect(requestChooseSeat).toHaveBeenCalledTimes(1);
    });

    it('should throw bad request exception', async () => {
      mockSeatDto.flightDate = '2023-06-05';
      const todayString = '2023-06-06';

      const reformattingCurrentTimeFollowTimezone = jest
        .spyOn(translateService, 'reformattingCurrentTimeFollowTimezone')
        .mockReturnValue(todayString);

      const checkAnotherLock = jest.spyOn(seatStatusRepo, 'checkAnotherLock');

      const validateSeat = jest.spyOn(coreService, 'validateSeat');

      const requestChooseSeat = jest.spyOn(seatStatusRepo, 'requestChooseSeat');

      // act
      const chooseSeat = service.chooseSeat(mockSeatDto);

      // assert
      await expect(chooseSeat).rejects.toEqual(
        new BadRequestException(
          `flightDate should be greather than or equal ${todayString.slice(
            0,
            10,
          )}`,
        ),
      );
      expect(reformattingCurrentTimeFollowTimezone).toHaveBeenCalledTimes(1);
      expect(checkAnotherLock).toHaveBeenCalledTimes(0);
      expect(validateSeat).toHaveBeenCalledTimes(0);
      expect(requestChooseSeat).toHaveBeenCalledTimes(0);
    });

    it('should throw unprocessable entity exception due to still another selected seat', async () => {
      const reformattingCurrentTimeFollowTimezone = jest
        .spyOn(translateService, 'reformattingCurrentTimeFollowTimezone')
        .mockReturnValue(mockSeatDto.flightDate);

      mockSeatStatus.seat = faker.number.int({ min: 1 });
      const checkAnotherLock = jest
        .spyOn(seatStatusRepo, 'checkAnotherLock')
        .mockReturnValue(mockSeatStatus);

      const validateSeat = jest.spyOn(coreService, 'validateSeat');

      const requestChooseSeat = jest.spyOn(seatStatusRepo, 'requestChooseSeat');

      // act
      const chooseSeat = service.chooseSeat(mockSeatDto);

      // assert
      await expect(chooseSeat).rejects.toEqual(
        new UnprocessableEntityException(
          `You still have another selected seat, need release first: ${mockSeatStatus.flightDate} - ${mockSeatStatus.flight} - ${mockSeatStatus.seat}`,
        ),
      );
      expect(reformattingCurrentTimeFollowTimezone).toHaveBeenCalledTimes(1);
      expect(checkAnotherLock).toHaveBeenCalledTimes(1);
      expect(validateSeat).toHaveBeenCalledTimes(0);
      expect(requestChooseSeat).toHaveBeenCalledTimes(0);
    });

    it('should throw unprocessable entity exception due to data seat request not valid', async () => {
      const reformattingCurrentTimeFollowTimezone = jest
        .spyOn(translateService, 'reformattingCurrentTimeFollowTimezone')
        .mockReturnValue(mockSeatDto.flightDate);

      const checkAnotherLock = jest
        .spyOn(seatStatusRepo, 'checkAnotherLock')
        .mockReturnValue(null);

      mockSeatValidateResponse.isValid = false;
      const validateSeat = jest
        .spyOn(coreService, 'validateSeat')
        .mockResolvedValue(mockSeatValidateResponse);

      const requestChooseSeat = jest.spyOn(seatStatusRepo, 'requestChooseSeat');

      // act
      const chooseSeat = service.chooseSeat(mockSeatDto);

      // assert
      await expect(chooseSeat).rejects.toEqual(
        new UnprocessableEntityException('Invalid input seat info'),
      );
      expect(reformattingCurrentTimeFollowTimezone).toHaveBeenCalledTimes(1);
      expect(checkAnotherLock).toHaveBeenCalledTimes(1);
      expect(validateSeat).toHaveBeenCalledTimes(1);
      expect(requestChooseSeat).toHaveBeenCalledTimes(0);
    });
  });

  describe('unchooseSeat', () => {
    let mockSeatDto: ChoseSeatDto;
    let mockSeatStatus: SeatStatus;
    let mockSeatValidateResponse: ISeatValidationResponse;
    let mockChoseSeatResult: IChoseSeatResult;

    beforeEach(() => {
      mockSeatDto = {
        passenger: faker.string.numeric({ length: 16 }),
        flightDate: faker.date.future().toISOString().split('T')[0],
        timezone: faker.helpers.enumValue(ETimezone),
        flight: faker.number.int({ min: 1 }),
        seat: faker.number.int({ min: 1 }),
      };

      mockSeatValidateResponse = {
        isValid: true,
        flight: mockSeatDto.flight,
        departure: faker.number.int({ min: 1 }),
        destination: faker.number.int({ min: 1 }),
        airplane: faker.number.int({ min: 1 }),
        seat: mockSeatDto.seat,
        seatClass: faker.helpers.enumValue(ESeatClass),
        seatNumber: faker.airline.seat(),
        price: faker.number.int({ min: 500000 }),
      };

      mockSeatStatus = {
        id: faker.number.int({ min: 1 }),
        passenger: mockSeatDto.passenger,
        flightDate: mockSeatDto.flightDate,
        flight: mockSeatValidateResponse.flight,
        departure: mockSeatValidateResponse.departure,
        destination: mockSeatValidateResponse.destination,
        airplane: mockSeatValidateResponse.airplane,
        seat: mockSeatValidateResponse.seat,
        seatClass: mockSeatValidateResponse.seatClass,
        seatNumber: mockSeatValidateResponse.seatNumber,
        price: mockSeatValidateResponse.price,
        status: faker.helpers.enumValue(ESeatStatus),
        createdAt: faker.date.recent().toISOString(),
        updatedAt: faker.date.recent().toISOString(),
        deletedAt: null,
      };

      mockChoseSeatResult = {
        passenger: mockSeatDto.passenger,
        flightDate: mockSeatDto.flightDate,
        flight: mockSeatDto.flight,
        seat: mockSeatDto.seat,
        status: ESeatStatus.RELEASED,
      };
    });

    it('should return seat unchoosen', async () => {
      // arrange
      const reformattingCurrentTimeFollowTimezone = jest
        .spyOn(translateService, 'reformattingCurrentTimeFollowTimezone')
        .mockReturnValue(mockSeatDto.flightDate);

      const validateSeat = jest
        .spyOn(coreService, 'validateSeat')
        .mockReturnValue(mockSeatValidateResponse);

      mockSeatStatus.status = ESeatStatus.RELEASED;
      const requestUnchooseSeat = jest
        .spyOn(seatStatusRepo, 'requestUnchooseSeat')
        .mockReturnValue(mockSeatStatus);

      // act
      const result = await service.unchooseSeat(mockSeatDto);

      // assert
      expect(result).toEqual(mockChoseSeatResult);
      expect(reformattingCurrentTimeFollowTimezone).toHaveBeenCalledTimes(1);
      expect(validateSeat).toHaveBeenCalledTimes(1);
      expect(requestUnchooseSeat).toHaveBeenCalledTimes(1);
    });

    it('should throw bad request exception', async () => {
      mockSeatDto.flightDate = '2023-06-05';
      const todayString = '2023-06-06';

      const reformattingCurrentTimeFollowTimezone = jest
        .spyOn(translateService, 'reformattingCurrentTimeFollowTimezone')
        .mockReturnValue(todayString);

      const validateSeat = jest.spyOn(coreService, 'validateSeat');

      const requestChooseSeat = jest.spyOn(seatStatusRepo, 'requestChooseSeat');

      // act
      const unchooseSeat = service.unchooseSeat(mockSeatDto);

      // assert
      await expect(unchooseSeat).rejects.toEqual(
        new BadRequestException(
          `flightDate should be greather than or equal ${todayString.slice(
            0,
            10,
          )}`,
        ),
      );
      expect(reformattingCurrentTimeFollowTimezone).toHaveBeenCalledTimes(1);
      expect(validateSeat).toHaveBeenCalledTimes(0);
      expect(requestChooseSeat).toHaveBeenCalledTimes(0);
    });

    it('should throw unprocessable entity exception due to data seat request not valid', async () => {
      const reformattingCurrentTimeFollowTimezone = jest
        .spyOn(translateService, 'reformattingCurrentTimeFollowTimezone')
        .mockReturnValue(mockSeatDto.flightDate);

      mockSeatValidateResponse.isValid = false;
      const validateSeat = jest
        .spyOn(coreService, 'validateSeat')
        .mockResolvedValue(mockSeatValidateResponse);

      const requestChooseSeat = jest.spyOn(seatStatusRepo, 'requestChooseSeat');

      // act
      const unchooseSeat = service.unchooseSeat(mockSeatDto);

      // assert
      await expect(unchooseSeat).rejects.toEqual(
        new UnprocessableEntityException('Invalid input seat info'),
      );
      expect(reformattingCurrentTimeFollowTimezone).toHaveBeenCalledTimes(1);
      expect(validateSeat).toHaveBeenCalledTimes(1);
      expect(requestChooseSeat).toHaveBeenCalledTimes(0);
    });
  });
});
