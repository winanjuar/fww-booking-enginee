import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker } from '@faker-js/faker';
import { PartnerRepository } from './partner.repository';
import { Partner } from 'src/entity/partner.entity';
import { IRegisterPartner } from 'src/interface/register-partner.interface';

describe('PartnerRepository', () => {
  let repository: PartnerRepository;
  let mockPartner: Partner;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PartnerRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<PartnerRepository>(PartnerRepository);

    mockPartner = {
      id: faker.number.int({ min: 1 }),
      cognitoId: faker.string.uuid(),
      name: faker.person.fullName(),
      username: faker.internet.userName(),
      picEmail: faker.internet.email(),
      picPhone: faker.phone.number(),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('savePartner', () => {
    it('should return data new passenger', async () => {
      // arrange
      const dataPartner: IRegisterPartner = {
        cognitoId: mockPartner.cognitoId,
        username: mockPartner.username,
        name: mockPartner.name,
        picEmail: mockPartner.picEmail,
        picPhone: mockPartner.picPhone,
      };

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockPartner);

      // act
      const partner = await repository.savePartner(dataPartner);

      // assert
      expect(partner).toEqual(mockPartner);
      expect(saveSpy).toBeCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(dataPartner);
    });
  });
});
