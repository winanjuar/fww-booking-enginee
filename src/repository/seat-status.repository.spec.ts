import { Test, TestingModule } from '@nestjs/testing';
import { DataSource, In, Not } from 'typeorm';
import { faker } from '@faker-js/faker';
import { SeatStatusRepository } from './seat-status.repository';
import { SeatStatus } from 'src/entity/seat-status.entity';
import { ESeatStatus } from 'src/enum/seat-status.enum';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { SeatDto } from 'src/dto/seat.dto';
import { ETimezone } from 'src/enum/timezone.enum';
import { IChoseSeatRequest } from 'src/interface/chose-seat-request.interface';
import {
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { IReservationCheck } from 'src/interface/reservation-check.interface';
import { ISeatStatusUpdate } from 'src/interface/seat-status-update.interface';

describe('SeatStatusRepository', () => {
  let repository: SeatStatusRepository;
  let mockSeatStatus: SeatStatus;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SeatStatusRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<SeatStatusRepository>(SeatStatusRepository);

    mockSeatStatus = {
      id: faker.number.int({ min: 1 }),
      passenger: faker.string.numeric({ length: 16 }),
      flightDate: faker.date.future().toISOString().split('T')[0],
      flight: faker.number.int({ min: 1 }),
      departure: faker.number.int({ min: 1 }),
      destination: faker.number.int({ min: 1 }),
      airplane: faker.number.int({ min: 1 }),
      seat: faker.number.int({ min: 1 }),
      seatClass: faker.helpers.enumValue(ESeatClass),
      seatNumber: faker.airline.seat(),
      price: faker.number.int(),
      status: faker.helpers.enumValue(ESeatStatus),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('getOccupied', () => {
    it('should return data flight with seat already occupied', async () => {
      // arrange
      const seatDto: SeatDto = {
        flight: mockSeatStatus.flight,
        seatClass: mockSeatStatus.seatClass,
        flightDate: mockSeatStatus.flightDate,
        timezone: ETimezone.WIB,
      };

      const { timezone, ...criteria } = seatDto;

      mockSeatStatus.status = ESeatStatus.SELECTED;

      const findSpy = jest
        .spyOn(repository, 'find')
        .mockResolvedValue([mockSeatStatus]);

      // act
      const occupied = await repository.getOccupied(seatDto);

      // assert
      expect(occupied).toEqual([mockSeatStatus]);
      expect(findSpy).toBeCalledTimes(1);
      expect(findSpy).toHaveBeenCalledWith({
        where: {
          ...criteria,
          status: In([
            ESeatStatus.SELECTED,
            ESeatStatus.BOOKED,
            ESeatStatus.PAID,
          ]),
        },
      });
    });
  });

  describe('checkAnotherLock', () => {
    it('should return data another seat still lock', async () => {
      // arrange
      const passenger = mockSeatStatus.passenger;
      const flightDate = mockSeatStatus.flightDate;
      const seat = faker.number.int({ min: 1 });
      mockSeatStatus.status = ESeatStatus.SELECTED;

      const findOne = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockSeatStatus);

      // act
      const seatStillLocked = await repository.checkAnotherLock(
        passenger,
        flightDate,
        seat,
      );

      // assert
      expect(seatStillLocked).toEqual(mockSeatStatus);
      expect(findOne).toHaveBeenCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        where: {
          passenger,
          flightDate,
          seat: Not(seat),
          status: ESeatStatus.SELECTED,
        },
      });
    });
  });

  describe('requestChooseSeat', () => {
    it('should return data seat choosen', async () => {
      // arrange
      const seatRequest: IChoseSeatRequest = {
        passenger: mockSeatStatus.passenger,
        flightDate: mockSeatStatus.flightDate,
        flight: mockSeatStatus.flight,
        departure: mockSeatStatus.departure,
        destination: mockSeatStatus.destination,
        airplane: mockSeatStatus.airplane,
        seat: mockSeatStatus.seat,
        seatClass: mockSeatStatus.seatClass,
        seatNumber: mockSeatStatus.seatNumber,
        price: mockSeatStatus.price,
      };

      const newChoseSeatRequest: IChoseSeatRequest = {
        ...seatRequest,
        status: ESeatStatus.SELECTED,
      };

      mockSeatStatus.status = ESeatStatus.SELECTED;

      const seatExist = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(null);

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockSeatStatus);

      // act
      const seatChoosen = await repository.requestChooseSeat(seatRequest);

      // assert
      expect(seatChoosen).toEqual(mockSeatStatus);
      expect(seatExist).toHaveBeenCalledTimes(1);
      expect(seatExist).toHaveBeenCalledWith({
        where: {
          flightDate: seatRequest.flightDate,
          flight: seatRequest.flight,
          seat: seatRequest.seat,
        },
      });
      expect(saveSpy).toHaveBeenCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(newChoseSeatRequest);
    });

    it('should return data seat choosen even seat exist but status is released', async () => {
      // arrange
      mockSeatStatus.status = ESeatStatus.RELEASED;

      const seatRequest: IChoseSeatRequest = {
        passenger: mockSeatStatus.passenger,
        flightDate: mockSeatStatus.flightDate,
        flight: mockSeatStatus.flight,
        departure: mockSeatStatus.departure,
        destination: mockSeatStatus.destination,
        airplane: mockSeatStatus.airplane,
        seat: mockSeatStatus.seat,
        seatClass: mockSeatStatus.seatClass,
        seatNumber: mockSeatStatus.seatNumber,
        price: mockSeatStatus.price,
      };

      const seatExist = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockSeatStatus);

      const seatStatusBeforeSave = {
        ...mockSeatStatus,
      };
      seatStatusBeforeSave.status = ESeatStatus.SELECTED;

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(seatStatusBeforeSave);

      // act
      const seatChoosen = await repository.requestChooseSeat(seatRequest);

      // assert
      expect(seatChoosen).toEqual(mockSeatStatus);
      expect(seatExist).toHaveBeenCalledTimes(1);
      expect(seatExist).toHaveBeenCalledWith({
        where: {
          flightDate: seatRequest.flightDate,
          flight: seatRequest.flight,
          seat: seatRequest.seat,
        },
      });
      expect(saveSpy).toHaveBeenCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(seatStatusBeforeSave);
    });

    it('should return data seat choosen even seat exist but status is unlocked', async () => {
      // arrange
      mockSeatStatus.status = ESeatStatus.UNLOCKED;

      const seatRequest: IChoseSeatRequest = {
        passenger: mockSeatStatus.passenger,
        flightDate: mockSeatStatus.flightDate,
        flight: mockSeatStatus.flight,
        departure: mockSeatStatus.departure,
        destination: mockSeatStatus.destination,
        airplane: mockSeatStatus.airplane,
        seat: mockSeatStatus.seat,
        seatClass: mockSeatStatus.seatClass,
        seatNumber: mockSeatStatus.seatNumber,
        price: mockSeatStatus.price,
      };

      const seatExist = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockSeatStatus);

      const seatStatusBeforeSave = {
        ...mockSeatStatus,
      };
      seatStatusBeforeSave.status = ESeatStatus.SELECTED;

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(seatStatusBeforeSave);

      // act
      const seatChoosen = await repository.requestChooseSeat(seatRequest);

      // assert
      expect(seatChoosen).toEqual(mockSeatStatus);
      expect(seatExist).toHaveBeenCalledTimes(1);
      expect(seatExist).toHaveBeenCalledWith({
        where: {
          flightDate: seatRequest.flightDate,
          flight: seatRequest.flight,
          seat: seatRequest.seat,
        },
      });
      expect(saveSpy).toHaveBeenCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(seatStatusBeforeSave);
    });

    it('should return data seat choosen even seat exist but status is reset', async () => {
      // arrange
      mockSeatStatus.status = ESeatStatus.RESET;

      const seatRequest: IChoseSeatRequest = {
        passenger: mockSeatStatus.passenger,
        flightDate: mockSeatStatus.flightDate,
        flight: mockSeatStatus.flight,
        departure: mockSeatStatus.departure,
        destination: mockSeatStatus.destination,
        airplane: mockSeatStatus.airplane,
        seat: mockSeatStatus.seat,
        seatClass: mockSeatStatus.seatClass,
        seatNumber: mockSeatStatus.seatNumber,
        price: mockSeatStatus.price,
      };

      const seatExist = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockSeatStatus);

      const seatStatusBeforeSave = {
        ...mockSeatStatus,
      };
      seatStatusBeforeSave.status = ESeatStatus.SELECTED;

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(seatStatusBeforeSave);

      // act
      const seatChoosen = await repository.requestChooseSeat(seatRequest);

      // assert
      expect(seatChoosen).toEqual(mockSeatStatus);
      expect(seatExist).toHaveBeenCalledTimes(1);
      expect(seatExist).toHaveBeenCalledWith({
        where: {
          flightDate: seatRequest.flightDate,
          flight: seatRequest.flight,
          seat: seatRequest.seat,
        },
      });
      expect(saveSpy).toHaveBeenCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(seatStatusBeforeSave);
    });

    it('should return data seat choosen even seat exist but status is unpaid', async () => {
      // arrange
      mockSeatStatus.status = ESeatStatus.UNPAID;

      const seatRequest: IChoseSeatRequest = {
        passenger: mockSeatStatus.passenger,
        flightDate: mockSeatStatus.flightDate,
        flight: mockSeatStatus.flight,
        departure: mockSeatStatus.departure,
        destination: mockSeatStatus.destination,
        airplane: mockSeatStatus.airplane,
        seat: mockSeatStatus.seat,
        seatClass: mockSeatStatus.seatClass,
        seatNumber: mockSeatStatus.seatNumber,
        price: mockSeatStatus.price,
      };

      const seatExist = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockSeatStatus);

      const seatStatusBeforeSave = {
        ...mockSeatStatus,
      };
      seatStatusBeforeSave.status = ESeatStatus.SELECTED;

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(seatStatusBeforeSave);

      // act
      const seatChoosen = await repository.requestChooseSeat(seatRequest);

      // assert
      expect(seatChoosen).toEqual(mockSeatStatus);
      expect(seatExist).toHaveBeenCalledTimes(1);
      expect(seatExist).toHaveBeenCalledWith({
        where: {
          flightDate: seatRequest.flightDate,
          flight: seatRequest.flight,
          seat: seatRequest.seat,
        },
      });
      expect(saveSpy).toHaveBeenCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(seatStatusBeforeSave);
    });

    it('should throw unprocessable entity', async () => {
      // arrange
      mockSeatStatus.status = ESeatStatus.BOOKED;

      const seatRequest: IChoseSeatRequest = {
        passenger: mockSeatStatus.passenger,
        flightDate: mockSeatStatus.flightDate,
        flight: mockSeatStatus.flight,
        departure: mockSeatStatus.departure,
        destination: mockSeatStatus.destination,
        airplane: mockSeatStatus.airplane,
        seat: mockSeatStatus.seat,
        seatClass: mockSeatStatus.seatClass,
        seatNumber: mockSeatStatus.seatNumber,
        price: mockSeatStatus.price,
      };

      const seatExist = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockSeatStatus);

      const seatStatusBeforeSave = {
        ...mockSeatStatus,
      };
      seatStatusBeforeSave.status = ESeatStatus.SELECTED;

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(seatStatusBeforeSave);

      // act
      const requestChooseSeat = repository.requestChooseSeat(seatRequest);

      // assert
      await expect(requestChooseSeat).rejects.toEqual(
        new UnprocessableEntityException(
          `Failed to choose seat : ${seatRequest.flightDate} - ${seatRequest.flight} - ${seatRequest.seat}`,
        ),
      );
      expect(seatExist).toHaveBeenCalledTimes(1);
      expect(seatExist).toHaveBeenCalledWith({
        where: {
          flightDate: seatRequest.flightDate,
          flight: seatRequest.flight,
          seat: seatRequest.seat,
        },
      });
      expect(saveSpy).toHaveBeenCalledTimes(0);
    });
  });

  describe('requestUnchooseSeat', () => {
    it('should return data unchoose seat', async () => {
      // arrange
      const seatRequest: IChoseSeatRequest = {
        passenger: mockSeatStatus.passenger,
        flightDate: mockSeatStatus.flightDate,
        flight: mockSeatStatus.flight,
        departure: mockSeatStatus.departure,
        destination: mockSeatStatus.destination,
        airplane: mockSeatStatus.airplane,
        seat: mockSeatStatus.seat,
        seatClass: mockSeatStatus.seatClass,
        seatNumber: mockSeatStatus.seatNumber,
        price: mockSeatStatus.price,
      };

      mockSeatStatus.status = ESeatStatus.SELECTED;

      const seatExist = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockSeatStatus);

      const mockSeatStatusNew = {
        ...mockSeatStatus,
        status: ESeatStatus.RELEASED,
      };

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockSeatStatusNew);

      // act
      const seatUnchoose = await repository.requestUnchooseSeat(seatRequest);

      // assert
      expect(seatUnchoose).toEqual(mockSeatStatusNew);
      expect(seatExist).toHaveBeenCalledTimes(1);
      expect(seatExist).toHaveBeenCalledWith({
        where: seatRequest,
      });
      expect(saveSpy).toHaveBeenCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(mockSeatStatusNew);
    });

    it('should throw not found exception', async () => {
      // arrange
      const seatRequest: IChoseSeatRequest = {
        passenger: faker.string.numeric({ length: 16 }),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: mockSeatStatus.flight,
        departure: mockSeatStatus.departure,
        destination: mockSeatStatus.destination,
        airplane: mockSeatStatus.airplane,
        seat: mockSeatStatus.seat,
        seatClass: mockSeatStatus.seatClass,
        seatNumber: mockSeatStatus.seatNumber,
        price: mockSeatStatus.price,
      };

      mockSeatStatus.status = ESeatStatus.SELECTED;

      const seatExist = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(null);

      const saveSpy = jest.spyOn(repository, 'save');

      // act
      const requestUnchooseSeat = repository.requestUnchooseSeat(seatRequest);

      // assert
      await expect(requestUnchooseSeat).rejects.toEqual(
        new NotFoundException(
          `Failed to unchoose seat : ${seatRequest.flightDate} - ${seatRequest.flight} - ${seatRequest.seat}`,
        ),
      );
      expect(seatExist).toHaveBeenCalledTimes(1);
      expect(seatExist).toHaveBeenCalledWith({
        where: seatRequest,
      });
      expect(saveSpy).toHaveBeenCalledTimes(0);
    });

    it('should throw unprocessable entity', async () => {
      // arrange
      mockSeatStatus.status = ESeatStatus.BOOKED;

      const seatRequest: IChoseSeatRequest = {
        passenger: mockSeatStatus.passenger,
        flightDate: mockSeatStatus.flightDate,
        flight: mockSeatStatus.flight,
        departure: mockSeatStatus.departure,
        destination: mockSeatStatus.destination,
        airplane: mockSeatStatus.airplane,
        seat: mockSeatStatus.seat,
        seatClass: mockSeatStatus.seatClass,
        seatNumber: mockSeatStatus.seatNumber,
        price: mockSeatStatus.price,
      };

      const seatExist = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockSeatStatus);

      const saveSpy = jest.spyOn(repository, 'save');

      // act
      const requestUnchooseSeat = repository.requestUnchooseSeat(seatRequest);

      // assert
      await expect(requestUnchooseSeat).rejects.toEqual(
        new UnprocessableEntityException(
          `Failed to unchoose seat : ${seatRequest.flightDate} - ${seatRequest.flight} - ${seatRequest.seat}`,
        ),
      );
      expect(seatExist).toHaveBeenCalledTimes(1);
      expect(seatExist).toHaveBeenCalledWith({
        where: seatRequest,
      });
      expect(saveSpy).toHaveBeenCalledTimes(0);
    });
  });

  describe('findOneByPassengerAndFlightDate', () => {
    it('should return data seat status by passenger and flight date', async () => {
      // arrange
      const reservationCheck: IReservationCheck = {
        passenger: mockSeatStatus.passenger,
        flightDate: mockSeatStatus.flightDate,
      };

      const findOne = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockSeatStatus);

      // act
      const seatLocked = await repository.findOneByPassengerAndFlightDate(
        reservationCheck,
      );

      // assert
      expect(seatLocked).toEqual(mockSeatStatus);
      expect(findOne).toHaveBeenCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        where: reservationCheck,
      });
    });
  });

  describe('updateStatus', () => {
    it('should return data seat status by passenger and flight date', async () => {
      // arrange
      const seatStatusData: ISeatStatusUpdate = {
        flightDate: mockSeatStatus.flightDate,
        flight: mockSeatStatus.flight,
        seat: mockSeatStatus.seat,
        seatStatus: ESeatStatus.PAID,
      };

      const { seatStatus, ...search } = seatStatusData;

      const findOneSpy = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockSeatStatus);

      mockSeatStatus.status = seatStatus;

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockSeatStatus);

      // act
      const saveResult = await repository.updateStatus(seatStatusData);

      // assert
      expect(saveResult).toEqual(mockSeatStatus);
      expect(findOneSpy).toHaveBeenCalledTimes(1);
      expect(findOneSpy).toHaveBeenCalledWith({
        where: search,
      });
      expect(saveSpy).toHaveBeenCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(mockSeatStatus);
    });
  });
});
