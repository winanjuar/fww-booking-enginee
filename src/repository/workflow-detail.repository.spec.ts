import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker } from '@faker-js/faker';
import { WorkflowDetail } from 'src/entity/workflow-detail.entity';
import { ETaskName } from 'src/enum/task-name.enum';
import {
  IWorkflowDetail,
  IWorkflowMaster,
} from 'src/interface/workflow.interface';
import { WorkflowDetailRepository } from './workflow-detail.repository';
import { Workflow } from 'src/entity/workflow.entity';

describe('WorkflowDetailRepository', () => {
  let repository: WorkflowDetailRepository;
  let mockWorkflowDetail: WorkflowDetail;
  let mockWorkflow: Workflow;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        WorkflowDetailRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<WorkflowDetailRepository>(WorkflowDetailRepository);

    mockWorkflow = {
      processInstanceId: faker.string.uuid(),
      reservationId: faker.number.int({ min: 1 }),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
      details: [mockWorkflowDetail],
    };

    mockWorkflowDetail = {
      workflow: mockWorkflow,
      taskId: faker.string.uuid(),
      activityName: faker.helpers.enumValue(ETaskName),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('saveWorkflowDetail', () => {
    it('should return data new workflow', async () => {
      // arrange
      const dataWorkflowMaster: IWorkflowMaster = {
        processInstanceId: mockWorkflow.processInstanceId,
      };

      const dataWorkflowDetail: IWorkflowDetail = {
        workflow: { processInstanceId: dataWorkflowMaster.processInstanceId },
        taskId: mockWorkflowDetail.taskId,
        activityName: mockWorkflowDetail.activityName,
      };

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockWorkflowDetail);

      // act
      const newWorkflow = await repository.saveWorkflowDetail(
        dataWorkflowDetail,
      );

      // assert
      expect(newWorkflow).toEqual(mockWorkflowDetail);
      expect(saveSpy).toBeCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(dataWorkflowDetail);
    });
  });
});
