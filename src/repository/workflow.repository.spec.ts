import { Test, TestingModule } from '@nestjs/testing';
import { DataSource } from 'typeorm';
import { faker } from '@faker-js/faker';
import { WorkflowRepository } from './workflow.repository';
import { Workflow } from 'src/entity/workflow.entity';
import { WorkflowDetail } from 'src/entity/workflow-detail.entity';
import { ETaskName } from 'src/enum/task-name.enum';
import {
  IWorkflowDetail,
  IWorkflowMaster,
} from 'src/interface/workflow.interface';

describe('WorkflowRepository', () => {
  let repository: WorkflowRepository;
  let mockWorkflow: Workflow;
  let mockWorkflowDetail: WorkflowDetail;

  const dataSource = {
    createEntityManager: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        WorkflowRepository,
        { provide: DataSource, useValue: dataSource },
      ],
    }).compile();
    module.useLogger(false);
    repository = module.get<WorkflowRepository>(WorkflowRepository);

    mockWorkflow = {
      processInstanceId: faker.string.uuid(),
      reservationId: faker.number.int({ min: 1 }),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
      details: [mockWorkflowDetail],
    };

    mockWorkflowDetail = {
      workflow: mockWorkflow,
      taskId: faker.string.uuid(),
      activityName: faker.helpers.enumValue(ETaskName),
      createdAt: faker.date.recent().toISOString(),
      updatedAt: faker.date.recent().toISOString(),
      deletedAt: null,
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('saveWorkflow', () => {
    it('should return data new workflow', async () => {
      // arrange
      const dataWorkflowDetail: IWorkflowDetail = {
        taskId: mockWorkflowDetail.taskId,
        activityName: mockWorkflowDetail.activityName,
      };

      const dataWorkflowMaster: IWorkflowMaster = {
        processInstanceId: mockWorkflow.processInstanceId,
        reservationId: mockWorkflow.reservationId,
        details: [dataWorkflowDetail],
      };

      const saveSpy = jest
        .spyOn(repository, 'save')
        .mockResolvedValue(mockWorkflow);

      // act
      const newWorkflow = await repository.saveWorkflow(dataWorkflowMaster);

      // assert
      expect(newWorkflow).toEqual(mockWorkflow);
      expect(saveSpy).toBeCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(dataWorkflowMaster);
    });
  });

  describe('getWorkflowByReservationId', () => {
    it('should return data workflow', async () => {
      // arrange
      const reservationId = mockWorkflow.reservationId;
      delete mockWorkflow.details;

      const findOne = jest
        .spyOn(repository, 'findOne')
        .mockResolvedValue(mockWorkflow);

      // act
      const workflow = await repository.getWorkflowByReservationId(
        reservationId,
      );

      // assert
      expect(workflow).toEqual(mockWorkflow);
      expect(findOne).toBeCalledTimes(1);
      expect(findOne).toHaveBeenCalledWith({
        where: { reservationId },
      });
    });
  });
});
