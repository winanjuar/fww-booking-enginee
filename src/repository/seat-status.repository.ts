import {
  Injectable,
  Logger,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { FlightCandidateDto } from 'src/dto/flight-candidate.dto';
import { SeatStatus } from 'src/entity/seat-status.entity';
import { ESeatStatus } from 'src/enum/seat-status.enum';
import { DataSource, In, LessThanOrEqual, Not, Repository } from 'typeorm';
import { SeatDto } from 'src/dto/seat.dto';
import { IReservationCheck } from 'src/interface/reservation-check.interface';
import { ISeatStatusUpdate } from 'src/interface/seat-status-update.interface';
import { IChoseSeatRequest } from 'src/interface/chose-seat-request.interface';
import { IOccupiedFlightCandidate } from 'src/interface/occupied-flight-candidate.interface';

@Injectable()
export class SeatStatusRepository extends Repository<SeatStatus> {
  private readonly logger = new Logger(SeatStatusRepository.name);
  constructor(dataSource: DataSource) {
    super(SeatStatus, dataSource.createEntityManager());
  }

  async getOccupationSummary(
    flightDto: FlightCandidateDto,
  ): Promise<IOccupiedFlightCandidate[]> {
    const occupiedQB = this.createQueryBuilder('seat_status')
      .select('seat_status.flightDate', 'flighDate')
      .addSelect('seat_status.flight', 'flight')
      .addSelect('seat_status.seatClass', 'seatClass')
      .addSelect('COUNT(*)', 'occupied')
      .where('seat_status.flightDate = :flightDate', {
        flightDate: flightDto.flightDate,
      })
      .andWhere('seat_status.departure = :departure', {
        departure: flightDto.departure,
      })
      .andWhere('seat_status.destination = :destination', {
        destination: flightDto.destination,
      })
      .andWhere('seat_status.status <> :seatStatus', {
        seatStatus: ESeatStatus.RELEASED,
      })
      .groupBy('seat_status.flightDate')
      .addGroupBy('seat_status.flight')
      .addGroupBy('seat_status.seatClass');

    const occupiedFlightCandidates = await occupiedQB.getRawMany();

    this.logger.log('Query data occupied flight candidates with criteria');
    return occupiedFlightCandidates as IOccupiedFlightCandidate[];
  }

  async getOccupied(seatDto: SeatDto) {
    const { timezone, ...criteria } = seatDto;
    const occupied = await this.find({
      where: {
        ...criteria,
        status: In([
          ESeatStatus.SELECTED,
          ESeatStatus.BOOKED,
          ESeatStatus.PAID,
        ]),
      },
    });
    this.logger.log('Query data flight with seat already occupied');
    return occupied;
  }

  async checkAnotherLock(
    passenger: string,
    flightDate: string,
    seat: number,
  ): Promise<SeatStatus> {
    const seatStillLocked = await this.findOne({
      where: {
        passenger,
        flightDate,
        seat: Not(seat),
        status: ESeatStatus.SELECTED,
      },
    });

    this.logger.log('Check another seat still lock');
    return seatStillLocked;
  }

  async requestChooseSeat(
    chooseSeatRequest: IChoseSeatRequest,
  ): Promise<SeatStatus> {
    const seatExist = await this.findOne({
      where: {
        flightDate: chooseSeatRequest.flightDate,
        flight: chooseSeatRequest.flight,
        seat: chooseSeatRequest.seat,
      },
    });

    const newChoseSeatRequest: IChoseSeatRequest = {
      ...chooseSeatRequest,
      status: ESeatStatus.SELECTED,
    };

    if (!seatExist) {
      const seatChoosen = await this.save(newChoseSeatRequest);
      this.logger.log(
        `Seat choosen: ${chooseSeatRequest.flightDate} - ${chooseSeatRequest.flight} - ${chooseSeatRequest.seat}`,
      );
      return seatChoosen;
    } else {
      if (
        seatExist.status === ESeatStatus.RELEASED ||
        seatExist.status === ESeatStatus.UNLOCKED ||
        seatExist.status === ESeatStatus.RESET ||
        seatExist.status === ESeatStatus.UNPAID
      ) {
        seatExist.passenger = chooseSeatRequest.passenger;
        seatExist.status = ESeatStatus.SELECTED;
        const seatChoosen = await this.save(seatExist);
        this.logger.log(
          `Seat choosen: ${chooseSeatRequest.flightDate} - ${chooseSeatRequest.flight} - ${chooseSeatRequest.seat}`,
        );
        return seatChoosen;
      } else {
        throw new UnprocessableEntityException(
          `Failed to choose seat : ${chooseSeatRequest.flightDate} - ${chooseSeatRequest.flight} - ${chooseSeatRequest.seat}`,
        );
      }
    }
  }

  async requestUnchooseSeat(
    chooseSeat: IChoseSeatRequest,
  ): Promise<SeatStatus> {
    const seatExist = await this.findOne({
      where: chooseSeat,
    });

    if (!seatExist) {
      throw new NotFoundException(
        `Failed to unchoose seat : ${chooseSeat.flightDate} - ${chooseSeat.flight} - ${chooseSeat.seat}`,
      );
    }

    if (seatExist.status === ESeatStatus.SELECTED) {
      seatExist.status = ESeatStatus.RELEASED;
      const seatUnchoose = await this.save(seatExist);
      this.logger.log(
        `Set seat unchoose: ${chooseSeat.flightDate} - ${chooseSeat.flight} - ${chooseSeat.seat}`,
      );
      return seatUnchoose;
    } else {
      throw new UnprocessableEntityException(
        `Failed to unchoose seat : ${chooseSeat.flightDate} - ${chooseSeat.flight} - ${chooseSeat.seat}`,
      );
    }
  }

  async findOneByPassengerAndFlightDate(
    reservationCheck: IReservationCheck,
  ): Promise<SeatStatus> {
    const seatLocked = await this.findOne({
      where: reservationCheck,
    });

    this.logger.log('Query data seat status by passenger and flight date');
    return seatLocked;
  }

  async updateStatus(seatStatusData: ISeatStatusUpdate): Promise<SeatStatus> {
    const { seatStatus, ...search } = seatStatusData;
    const seatExist = await this.findOne({
      where: search,
    });

    seatExist.status = seatStatus;
    await this.save(seatExist);
    this.logger.log(
      `Set seat status: ${seatExist.flightDate} - ${seatExist.flight} - ${seatExist.seat} - ${seatExist.status}`,
    );
    return seatExist;
  }

  async resetSeatStatus(updatedAtSting: string): Promise<void> {
    this.logger.log(
      `Reset seat status still selected with no activity for more than 30 minutes`,
    );
    await this.update(
      {
        status: ESeatStatus.SELECTED,
        updatedAt: LessThanOrEqual(updatedAtSting),
      },
      { status: ESeatStatus.RESET },
    );
  }
}
