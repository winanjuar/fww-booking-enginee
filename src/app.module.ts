import { Module } from '@nestjs/common';
import { InquiryController } from './controller/inquiry.controller';
import { InquiryService } from './service/inquiry.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule } from 'nestjs-pino';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';
import { SeatStatusRepository } from './repository/seat-status.repository';
import { SeatStatus } from './entity/seat-status.entity';
import { CoreService } from './service/core.service';
import { Partner } from './entity/partner.entity';
import { BullModule } from '@nestjs/bull';
import { BPMService } from './service/bpm.service';
import { MailService } from './service/mail.service';
import { NormalMailProcessor } from './tasks/normal-mail.processor';
import { DeclineMailProcessor } from './tasks/decline-mail.processor';
import { MailController } from './controller/mail.controller';
import { WorkflowRepository } from './repository/workflow.repository';
import { Workflow } from './entity/workflow.entity';
import { WorkflowDetail } from './entity/workflow-detail.entity';
import { WorkflowDetailRepository } from './repository/workflow-detail.repository';
import { WorkflowController } from './controller/workflow.controller';
import { WorkflowService } from './service/workflow.service';
import { PaymentService } from './service/payment.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AuthModule } from './auth/auth.module';
import { TranslateService } from './service/translate.service';
import { ReservationController } from './controller/reservation.controller';
import { ReservationService } from './service/reservation.service';
import { PartnerController } from './controller/partner.controller';
import { PartnerService } from './service/partner.service';
import { PartnerRepository } from './repository/partner.repository';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    ConfigModule.forRoot(),
    LoggerModule.forRoot({
      pinoHttp: {
        formatters: {
          level: (label: string) => {
            return { level: label.toUpperCase() };
          },
        },
        customLevels: {
          emergerncy: 80,
          alert: 70,
          critical: 60,
          error: 50,
          warn: 40,
          notice: 30,
          info: 20,
          debug: 10,
        },
        useOnlyCustomLevels: true,
        transport: {
          target: 'pino-pretty',
          options: {
            singleLine: true,
            colorize: true,
            levelFirst: true,
            translateTime: 'SYS:standard',
            ignore: 'hostname,pid',
          },
        },
      },
    }),
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 5000,
        maxRedirects: 5,
      }),
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: 'mariadb',
        host: configService.get<string>('DB_HOST'),
        port: configService.get<number>('DB_PORT'),
        username: configService.get<string>('DB_USER'),
        password: configService.get<string>('DB_PASS'),
        database: configService.get<string>('DB_NAME'),
        entities: [SeatStatus, Partner, Workflow, WorkflowDetail],
        synchronize: configService.get<boolean>('DB_SYNC') || false,
        dateStrings: true,
      }),
    }),
    BullModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        redis: {
          host: configService.get<string>('REDIS_HOST'),
          port: configService.get<number>('REDIS_PORT'),
        },
      }),
    }),
    BullModule.registerQueue(
      {
        name: 'NormalMailQueue',
      },
      {
        name: 'DeclineMailQueue',
      },
    ),
    ClientsModule.registerAsync([
      {
        name: 'CoreEngine',
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: async (configService: ConfigService) => ({
          transport: Transport.RMQ,
          options: {
            urls: [configService.get<string>('RMQ_URL')],
            queue: configService.get<string>('RMQ_CORE_QUEUE'),
            queueOptions: { durable: false },
            prefetchCount: 1,
          },
        }),
      },
    ]),
    ScheduleModule.forRoot(),
    AuthModule,
  ],
  controllers: [
    InquiryController,
    ReservationController,
    MailController,
    WorkflowController,
    PartnerController,
  ],
  providers: [
    InquiryService,
    ReservationService,
    TranslateService,
    MailService,
    CoreService,
    BPMService,
    PaymentService,
    WorkflowService,
    PartnerService,
    NormalMailProcessor,
    DeclineMailProcessor,
    SeatStatusRepository,
    WorkflowRepository,
    WorkflowDetailRepository,
    PartnerRepository,
  ],
})
export class AppModule {}
