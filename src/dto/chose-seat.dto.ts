import { IsNotEmpty, IsNumber, IsString, Length } from 'class-validator';
import { SeatDto } from './seat.dto';
import { OmitType } from '@nestjs/swagger';

export class ChoseSeatDto extends OmitType(SeatDto, ['seatClass']) {
  @IsString()
  @Length(16)
  @IsNotEmpty()
  passenger: string;

  @IsNumber()
  @IsNotEmpty()
  seat: number;
}
