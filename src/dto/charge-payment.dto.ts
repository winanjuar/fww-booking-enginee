import { IsEnum, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { EBankChoice } from 'src/enum/bank-choice.enum';

export class ChargePaymentDto {
  @IsEnum(EBankChoice)
  @IsNotEmpty()
  bank: EBankChoice;

  @IsNumber()
  @IsNotEmpty()
  reservationId: number;

  @IsString()
  @IsNotEmpty()
  bookingCode: string;
}
