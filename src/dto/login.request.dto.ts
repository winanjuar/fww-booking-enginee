import { PickType } from '@nestjs/swagger';
import { PartnerDto } from './partner.dto';

export class LoginRequestDto extends PickType(PartnerDto, [
  'username',
  'password',
]) {}
