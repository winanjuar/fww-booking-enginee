import {
  IsDateString,
  IsEmail,
  IsEnum,
  IsISO8601,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsString,
  Length,
} from 'class-validator';
import { ETimezone } from 'src/enum/timezone.enum';

export class ReservationDto {
  @IsString()
  @Length(16)
  @IsNotEmpty()
  identityNumber: string;

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsISO8601({ strict: true })
  @IsDateString()
  @IsNotEmpty()
  birthDate: string;

  @IsPhoneNumber()
  @IsNotEmpty()
  phone: string;

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsOptional()
  @IsNumber()
  member: number;

  @IsISO8601({ strict: true })
  @IsDateString()
  @IsNotEmpty()
  flightDate: string;

  @IsNumber()
  @IsNotEmpty()
  flight: number;

  @IsNumber()
  @IsNotEmpty()
  seat: number;

  @IsEnum(ETimezone)
  @IsNotEmpty()
  timezone: ETimezone;
}
