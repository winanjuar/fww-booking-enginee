import {
  IsEnum,
  IsISO8601,
  IsNotEmpty,
  IsNumber,
  IsOptional,
} from 'class-validator';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { ETimezone } from 'src/enum/timezone.enum';

export class SeatDto {
  @IsNumber()
  @IsNotEmpty()
  flight: number;

  @IsEnum(ESeatClass)
  @IsOptional()
  seatClass: ESeatClass;

  @IsISO8601({ strict: true })
  @IsNotEmpty()
  flightDate: string;

  @IsEnum(ETimezone)
  @IsNotEmpty()
  timezone: ETimezone;
}
