import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString } from 'class-validator';

export class NormalMailRequestDto {
  @ApiProperty()
  @IsEmail()
  mailTo: string;

  @IsString()
  flightCode: string;

  @IsString()
  flightDate: string;

  @IsString()
  departureAirport: string;

  @IsString()
  departureCode: string;

  @IsString()
  departureTime: string;

  @IsString()
  departureTimezone: string;

  @IsString()
  destinationAirport: string;

  @IsString()
  destinationCode: string;

  @IsString()
  arrivalTime: string;

  @IsString()
  arrivalTimezone: string;

  @IsString()
  seatNumber: string;

  @IsString()
  typeCode: string;

  @IsString()
  realCode: string;

  @IsString()
  nextProcess: string;

  @IsString()
  deadline: string;

  @IsString()
  consequence: string;

  @IsString()
  specialNote: string;
}
