import {
  IsDateString,
  IsEnum,
  IsISO8601,
  IsNotEmpty,
  IsNumber,
} from 'class-validator';
import { ETimezone } from 'src/enum/timezone.enum';

export class FlightCandidateDto {
  @IsNumber()
  @IsNotEmpty()
  departure: number;

  @IsNumber()
  @IsNotEmpty()
  destination: number;

  @IsDateString()
  @IsNotEmpty()
  @IsISO8601({ strict: true })
  flightDate: string;

  @IsEnum(ETimezone)
  @IsNotEmpty()
  timezone: ETimezone;
}
