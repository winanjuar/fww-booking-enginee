import { IsEmail, IsString } from 'class-validator';

export class DeclineMailRequestDto {
  @IsEmail()
  mailTo: string;

  @IsString()
  flightCode: string;

  @IsString()
  flightDate: string;

  @IsString()
  departureAirport: string;

  @IsString()
  departureCode: string;

  @IsString()
  departureTime: string;

  @IsString()
  departureTimezone: string;

  @IsString()
  destinationAirport: string;

  @IsString()
  destinationCode: string;

  @IsString()
  arrivalTime: string;

  @IsString()
  arrivalTimezone: string;

  @IsString()
  seatNumber: string;

  @IsString()
  reservationStatus: string;

  @IsString()
  reason: string;
}
