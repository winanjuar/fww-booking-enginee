import { PickType } from '@nestjs/swagger';
import { PartnerDto } from './partner.dto';

export class CognitoUserDto extends PickType(PartnerDto, [
  'cognitoId',
  'username',
]) {}
