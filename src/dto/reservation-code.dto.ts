import { IsNotEmpty, IsString, Length } from 'class-validator';

export class ReservationCodeDto {
  @IsString()
  @IsNotEmpty()
  @Length(16)
  identityNumber: string;

  @IsString()
  @IsNotEmpty()
  reservationCode: string;
}
