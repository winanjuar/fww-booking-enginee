import { EBankChoice } from 'src/enum/bank-choice.enum';
import { IGenericId } from './generic-id.interface';

export interface IPaymentMaster {
  id: string;
  reservation?: IGenericId;
  paymentMethod: EBankChoice;
  paymentFinal: number;
  paymentStatus: string;
  chargeTime: string;
  paymentTime?: string;
  details?: IPaymentDetail[];
}

export interface IPaymentDetail {
  id: string;
  payment?: IPaymentMaster;
  name: string;
  quantity: number;
  price: number;
}

export interface IPaymentUpdate {
  id: string;
  paymentStatus: string;
  checkTime?: string;
  paymentTime?: string;
}
