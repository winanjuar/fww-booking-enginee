export interface ICheckPaymentResponse {
  paymentId: string;
  paymentStatus: string;
  checkTime?: string;
  paymentTime?: string;
}
