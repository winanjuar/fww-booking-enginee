export interface IAWSError {
  name: string;
  response: IBasicError;
}

interface IBasicError {
  statusCode: number;
  message: string;
  error: string;
}
