export interface IReservationNew {
  partner: string;
  identityNumber: string;
  name: string;
  birthDate: string;
  phone: string;
  email: string;
  member?: number;
  flightDate: string;
  flight: number;
  seat: number;
  reservationTime: string;
  priceActual: number;
}
