import { ESeatStatus } from 'src/enum/seat-status.enum';

export interface ISeatStatusUpdate {
  flightDate: string;
  flight: number;
  seat: number;
  seatStatus: ESeatStatus;
}
