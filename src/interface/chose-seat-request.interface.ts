import { ESeatClass } from 'src/enum/seat-class.enum';
import { ESeatStatus } from 'src/enum/seat-status.enum';

export interface IChoseSeatRequest {
  passenger: string;
  flightDate: string;
  flight: number;
  departure: number;
  destination: number;
  airplane: number;
  seat: number;
  seatClass: ESeatClass;
  seatNumber: string;
  price: number;
  status?: ESeatStatus;
}
