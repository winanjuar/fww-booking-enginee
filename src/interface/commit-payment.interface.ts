export interface ICommitPayment {
  reservationId: number;
  commitPaymentTime: string;
}
