import { ESeatClass } from 'src/enum/seat-class.enum';
import { ESeatStatus } from 'src/enum/seat-status.enum';

export interface IChoseSeatResult {
  passenger: string;
  flightDate: string;
  flight: number;
  seat: number;
  status: ESeatStatus;
}
