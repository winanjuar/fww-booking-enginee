export interface IChargeResponse {
  status_code: string;
  status_message: string;
  transaction_id: string;
  order_id: string;
  merchant_id?: string;
  gross_amount: string;
  currency: string;
  payment_type: string;
  transaction_time: string;
  transaction_status: string;
  fraud_status?: string;
  permata_va_number?: string;
  va_numbers?: IVANumber[];
  bill_key?: string;
  biller_code?: string;
}

export interface IVANumber {
  bank: string;
  va_number: string;
}
