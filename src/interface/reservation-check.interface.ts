export interface IReservationCheck {
  passenger: string;
  flightDate: string;
}
