import { EReservationStatus } from 'src/enum/reservation-status.enum';
import { IPassenger } from './core-service/passenger.interface';
import { IFlight } from './core-service/flight.interface';

export interface IReservationResult {
  id: number;
  partner: number;
  passenger: IPassenger;
  member: number;
  email: string;
  phone: string;
  flightDate: string;
  flight: IFlight;
  seat: number;
  priceActual: number;
  bookingCode: string;
  bookingTime: string;
  paymentId: string;
  paymentStatus: string;
  paymentCode: number;
  discountDestination: number;
  discountFastPayment: number;
  paymentFinal: number;
  paymentTime: string;
  reservationCode: string;
  ticketNumber: string;
  promotion: string;
  status: EReservationStatus;
}
