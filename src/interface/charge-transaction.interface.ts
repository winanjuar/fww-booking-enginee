import { EBankChoice } from 'src/enum/bank-choice.enum';

export interface IChargeTransaction {
  bank: EBankChoice;
  virtualAccountNumber?: string;
  billerCode?: string;
  billerKey?: string;
}
