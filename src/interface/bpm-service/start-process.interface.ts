export interface IStartProcess {
  identityNumber: string; //untuk pengecekan regulasi (disdukcapil, imigrasi, dan pedulilindungi)
  name: string; //untuk pengecekan regulasi disdukcapil
  birthDate: string; //untuk pengecekan regulasi disdukcapil
  isMember: string; //untuk pengecekan status member
  memberId: number; //untuk pengecekan status member
  email: string; //alamat kirim email
  flight: number; //untuk reset kursi
  flightCode: string; //untuk content email
  flightDate: string; //untuk content email
  departureAirport: string; //untuk content email
  departureCode: string; //untuk content email
  departureTime: string; //untuk content email
  departureTimezone: string; //untuk content email
  destination: string; //untuk cek diskon destination
  destinationAirport: string; //untuk content email
  destinationCode: string; //untuk content email
  arrivalTime: string; //untuk content email
  arrivalTimezone: string; //untuk content email
  seat: number; //untuk reset kursi
  seatNumber: string; //untuk content email
  reservationTime: string; //untuk cek diskon fast payment
  timezone: string; //supaya lebih presisi ngitung deadline
  bussinessKey: string;
}
