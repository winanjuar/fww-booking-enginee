export interface ITaskResultWithInstanceID {
  workflowInstanceId: string;
  currentTask: ITaskResult;
}

export interface ITaskResult {
  id: string;
  name: string;
  assignee: string;
  created: string;
  due: string;
  followUp: string;
  lastUpdated: string;
  delegationState: string;
  description: string;
  executionId: string;
  owner: string;
  parentTaskId: string;
  priority: number;
  processDefinitionId: string;
  processInstanceId: string;
  taskDefinitionKey: string;
  caseExecutionId: string;
  caseInstanceId: string;
  caseDefinitionId: string;
  suspended: boolean;
  formKey: string;
  camundaFormRef: string;
  tenantId: string;
}

export interface ITaskVariableResult {
  workflowInstanceId: string;
  currentTaskId: string;
  currentTaskName: string;
  variables: any;
}
