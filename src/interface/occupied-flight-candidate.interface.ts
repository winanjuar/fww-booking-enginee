import { ESeatClass } from 'src/enum/seat-class.enum';

export interface IOccupiedFlightCandidate {
  flightDate: string;
  flight: number;
  seatClass: ESeatClass;
  occupied: number;
}
