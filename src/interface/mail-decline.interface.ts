export interface IMailDecline {
  mailTo: string;
  flightCode: string;
  flightDate: string;
  departureAirport: string;
  departureCode: string;
  departureTime: string;
  departureTimezone: string;
  destinationAirport: string;
  destinationCode: string;
  arrivalTime: string;
  arrivalTimezone: string;
  seatNumber: string;
  reservationStatus: string;
  reason: string;
}
