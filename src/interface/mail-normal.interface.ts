export interface IMailNormal {
  mailTo: string;
  flightCode: string;
  flightDate: string;
  departureAirport: string;
  departureCode: string;
  departureTime: string;
  departureTimezone: string;
  destinationAirport: string;
  destinationCode: string;
  arrivalTime: string;
  arrivalTimezone: string;
  seatNumber: string;
  typeCode: string;
  realCode: string;
  nextProcess: string;
  deadline: string;
  consequence: string;
  specialNote: string;
}
