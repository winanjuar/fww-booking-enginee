export interface IRegisterPartner {
  cognitoId: string;
  username: string;
  name: string;
  picEmail: string;
  picPhone: string;
}
