import { ISeat } from './seat.interface';

export interface IAirplane {
  id?: number;
  name: string;
  registrationNumber: string;
  maxPassenger: number;
  maxBusiness: number;
  maxEconomy: number;
  chairConfig: string;
  seats?: ISeat[];
}
