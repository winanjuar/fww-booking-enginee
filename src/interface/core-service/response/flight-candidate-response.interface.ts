import { IAirport } from '../airport.interface';
import { IAirplane } from '../airplane.interface';
import { IPrice } from '../price.interface';

export interface IFlightCandidateResponse {
  id: number;
  code: string;
  departure: IAirport;
  destination: IAirport;
  airplane: IAirplane;
  departureTimeInWIB: string; //response masih WIB
  arrivalTimeInWIB: string; //response masih WIB
  durationInMinutes: number;
  prices: IPrice[];
}
