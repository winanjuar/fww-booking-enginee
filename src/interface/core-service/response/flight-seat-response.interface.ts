import { IAirplane } from '../airplane.interface';

export interface IFlightSeatResponse {
  id: number;
  code: string;
  departure: number;
  destination: number;
  airplane: IAirplane;
  departureTimeInWIB: string;
  arrivalTimeInWIB: string;
  durationInMinutes?: number;
}
