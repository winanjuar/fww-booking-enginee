import { IBaggage } from '../baggage.interface';

export interface IFlightBaggageResponse {
  id: number;
  code: string;
  departure: number;
  destination: number;
  airplane: number;
  departureTimeInWIB: string;
  arrivalTimeInWIB: string;
  durationInMinutes: number;
  baggages?: IBaggage[];
}
