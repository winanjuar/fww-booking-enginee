export interface IBaggage {
  id?: number;
  capacity: number;
  category: string;
  price: number;
}
