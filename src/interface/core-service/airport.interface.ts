import { ETimezone } from 'src/enum/timezone.enum';

export interface IAirport {
  id: number;
  code: string;
  name: string;
  city: string;
  timezone: ETimezone;
}
