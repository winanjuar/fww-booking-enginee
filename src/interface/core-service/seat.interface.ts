import { ESeatPosition } from 'src/enum/seat-position.enum';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { ESeatSide } from 'src/enum/seat-side.enum';

export interface ISeat {
  id: number;
  code: string;
  seatClass: ESeatClass;
  side: ESeatSide;
  position: ESeatPosition;
  airplane?: number;
}
