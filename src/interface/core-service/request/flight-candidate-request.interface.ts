export interface IFlightCandidateRequest {
  departureId: number;
  destinationId: number;
}
