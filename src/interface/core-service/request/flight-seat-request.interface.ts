export interface IFlightSeatRequest {
  flight: number;
  seat: number;
}
