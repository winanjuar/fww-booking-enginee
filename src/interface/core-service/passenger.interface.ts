export interface IPassenger {
  identityNumber: string;
  name?: string;
  birthDate?: string;
}
