import { ESeatClass } from 'src/enum/seat-class.enum';

export interface IPrice {
  id?: number;
  flight?: number;
  seatClass: ESeatClass;
  priceConfig: number;
  seatLeft?: number;
}
