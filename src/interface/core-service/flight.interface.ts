import { IAirplane } from './airplane.interface';
import { IAirport } from './airport.interface';
import { IBaggage } from './baggage.interface';
import { IPrice } from './price.interface';

export interface IFlight {
  id: number;
  code: string;
  departure?: IAirport | number;
  destination?: IAirport | number;
  airplane?: IAirplane | number;
  flightDate?: string;
  departureTime?: string;
  arrivalTime?: string;
  durationInMinutes?: number;
  prices?: IPrice[];
  baggages?: IBaggage[];
}
