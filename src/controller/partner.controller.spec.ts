import { Test, TestingModule } from '@nestjs/testing';
import { PartnerController } from './partner.controller';
import { PartnerService } from 'src/service/partner.service';
import { ITokenLogin } from 'src/interface/token-login.interface';
import { RegisterRequestDto } from 'src/dto/register-partner.dto';
import { faker } from '@faker-js/faker';
import { IRegisterPartner } from 'src/interface/register-partner.interface';
import {
  InternalServerErrorException,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { LoginRequestDto } from 'src/dto/login.request.dto';
import { CognitoUserDto } from 'src/dto/cognito-user.dto';

describe('PartnerController', () => {
  let controller: PartnerController;
  let mockPartner: IRegisterPartner;
  let mockToken: ITokenLogin;

  const partnerService = {
    register: jest.fn(),
    login: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PartnerController],
      providers: [{ provide: PartnerService, useValue: partnerService }],
    }).compile();
    module.useLogger(false);
    controller = module.get<PartnerController>(PartnerController);

    mockPartner = {
      cognitoId: faker.string.uuid(),
      username: faker.internet.userName(),
      name: faker.string.sample(),
      picEmail: faker.internet.email(),
      picPhone: faker.phone.number(),
    };

    mockToken = {
      accessToken:
        'eyJraWQiOiJ3VnU5cEtzcW9OQTV3b1BnTG5tbURrZGcwVFdUeDUxSmpsV0ZTR3VHUWE4PSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI4MmNlZjIzYi01NWYwLTQ2ZTctODYyYS1iZmU0ZTU0OTA2NDgiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5hcC1zb3V0aGVhc3QtMS5hbWF6b25hd3MuY29tXC9hcC1zb3V0aGVhc3QtMV9sdTNWOFBlYjAiLCJwaG9uZV9udW1iZXJfdmVyaWZpZWQiOmZhbHNlLCJjb2duaXRvOnVzZXJuYW1lIjoic3lhdXFpYTEiLCJvcmlnaW5fanRpIjoiMWZkNDVkMDctN2JjYi00MDE3LWExZmEtZWNlOGNiMTZhYzBkIiwiYXVkIjoiMmd1M2ZmcGViYWJpaWo2bmVqY3RydW92MDUiLCJldmVudF9pZCI6IjRmOTJmZWQyLWE2ZTgtNGVhMi1hZjBmLTQ4MmQzNWNlNDVmNiIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjcyNjYyMDA1LCJwaG9uZV9udW1iZXIiOiIrNjI4MTEzMTAwOTQzIiwiZXhwIjoxNjcyNjY1NjA1LCJpYXQiOjE2NzI2NjIwMDUsImp0aSI6IjExODg1YmI1LTIwNGMtNDYzZC05N2RiLThiOTk0MTU3OWNjMSIsImVtYWlsIjoic3VnZW5nLndpbmFuanVhckBnbWFpbC5jb20ifQ.EjtAjIPsYCxSmLoBdzijuXt7zYBK6xYpd801GTLo_WNqS6KeGgH8RPwysdRxOISpGfYJgk2NtdWnis-5RPPV257-mF4osdEhO5zMWMQbUEt-YS8qINJ_Az9q0PRMlcXgcq70wAVzfOKEEf_QewhbD_J_CFWb5EvTpeX81XrNv9L-XZUGwCgg0kfLLxO6eahDClFKaMmZT1kcxVZ1F_3gWQuQs4Bd8MqIdzdNQQYgljBAPTJgZjFnmR6Ad_9MIMGVtAsiWz0eJf5c2KjeG_wdCtpw0Ez4fL_WoigTYcaKcm2goJWix7P9zAcwZ__UFynnZqoyoQcYAV33qVIrCAQGFw',
      refreshToken:
        'eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiUlNBLU9BRVAifQ.V6XG0Gi_Y5k2rxTK3KsWxyT7R9XXOJhi0FvLktLFu_MEuEXhjvWnVKSTHuk591UBLVBDmjkaSlekXubNzAjQg6LhWLnU7m7YAfWEa7VTpTIKgTxJcdkdmB9FcEdjXTP9ck2zXaZU3kT534ZGrk_HjzVEsimwh3Sp__3ixG-xOcAFq4j_UPZfixBH6C_xbzvMCpzYz1booxOeMIUNmRqAXU0wTvHNC7d2gU-rGJijOpZr7vtsuDoPtMVwCyhXTNsnnc9yWm6losizxxOcoHG_5hchgta8TQDD8bUJnzKYJ9qomntFC4MZNjY4tSvtPH59IuwZMuMNsh-YsC2EaABv2A.-AllfM9207sbxHPw.pbQqwCCeDZOZuHh2auLf3nfngQi1XIDgpjpBSfnWbD4Iv1G77O43C1uN9acpT5WgS_jtbz-bYQrSccR0PhavF6XH5DXnvZtNjJRnonIF82twSJqACWu6wRqoIa1ZmOMaXMi8dMfbG98Op5fhH8Tl0kMruqwd6yf-Y_e9RIDLPSbeKzAeLjeL0I6lsT7mHzQrMWonJTlxSOsG8dAza5Id5Dx6BXbquGsfQjSZOL3z60dRS7WYW5FF_RBPA7qKy_amRDHojh4LaZ26y7KMlfniEvf46x7KnYAwZNI9GxL8K4-9em3oeZfsq0EHLtARwfjusGbIDWENEXPUAGkoihZhmyRaNScn4-xFRCO-Z0Ue77tuC2Vk09aothmg_I7Cop04ToqNIM9XtARSpAQfyIPcypmAuArTma4PyMSwyKFyVPJz2iVMF9W5rzTvN-M-QgU9HaQQxjFtFDJfBwbWGrGqEYMQQ4ddsNKwZetPPGZEkdrt85MVsEQ7KjN27PmILI1SCP28rXQluruga2-GgUqRVPcLRAyRFCPw5Kbiq648jqKUahdQAYl9AjJKhE7UcpVAqw04kXCeaQYtDoVw9DSr2gI0aFNdQTGj1wyefghMz-DOrlftzLenPNoLWIPbBWMcDRkstPxsI463Pe4ZVAQ3wzadB-rigagwPHjBrz4K-qZMpPmvJMb1f9NI2EOsKKvA04xpznYQLXR-Jp1gf7ea5UJWkB689OOjDJjptxiCoFa79VKY0IvWBmlvIeq2Xe4rLQOmxxttoBqwbOGR2Z-8o-H2PvkNzIBG63I4KtG2WkuKVJpIk5bCfsAnc35lnluYDb-7bK7LlWv2OMNazEz1ssGodod7WJKHv8qcrm79OG_giJKpEN_85zYs3TYh-2R1JUih20dXjo4SZmjEAGyDlpmnnoJghagSYVg8hhpL7evwTmB_FOF-dPyM0AoBhHwowh4eHG2goi9T9t9Oat4_vKQ86qzHyC01G-VNxfiZJfgl4_XBro2j3ddkrrRPr19xuGQhHSJyqj2DSf-IL_fhgE3_tVwklT7XVH-wxi8huvmgBrRI6QL-kg75W9ZL_tpEBXLEAbD-EQO9Sqan5KnIaz8ympeS45Lvnjuk3SUmdqthgbM7ATh9jMZ_bHuxH1hAZZhoughtmAZgweKphT_m_Elqa2Baqp9viWEM2BZ0VjabPX6Y4NXxaNsOu5zQt1IuxX_uSpr6RgyEN2na3VV1_7aQS1SP-deHzm9DN48RyPNwVNED4326mGXlO3QXU4W72xMGw91jnRtjS6e3joIoscKLImU.a3avvFwXtrweJSvjUESfMw',
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('register', () => {
    it('should return new partner', async () => {
      // arrange
      const registerDto: RegisterRequestDto = {
        name: mockPartner.name,
        username: mockPartner.username,
        password: faker.string.sample(),
        picEmail: mockPartner.picEmail,
        picPhone: mockPartner.picPhone,
      };

      const registerSpy = jest
        .spyOn(partnerService, 'register')
        .mockResolvedValue(mockPartner);

      // act
      const newPartner = await controller.register(registerDto);

      // assert
      expect(newPartner).toEqual(mockPartner);
      expect(registerSpy).toBeCalledTimes(1);
      expect(registerSpy).toBeCalledWith(registerDto);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      const registerDto: RegisterRequestDto = {
        name: mockPartner.name,
        username: mockPartner.username,
        password: faker.string.sample(),
        picEmail: mockPartner.picEmail,
        picPhone: mockPartner.picPhone,
      };

      const registerSpy = jest
        .spyOn(partnerService, 'register')
        .mockRejectedValue(
          new UnprocessableEntityException('Unprocessable Entity'),
        );

      // act
      const register = controller.register(registerDto);

      // assert
      await expect(register).rejects.toEqual(
        new UnprocessableEntityException('Unprocessable Entity'),
      );
      expect(registerSpy).toBeCalledTimes(1);
      expect(registerSpy).toBeCalledWith(registerDto);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const registerDto: RegisterRequestDto = {
        name: mockPartner.name,
        username: mockPartner.username,
        password: faker.string.sample(),
        picEmail: mockPartner.picEmail,
        picPhone: mockPartner.picPhone,
      };

      const registerSpy = jest
        .spyOn(partnerService, 'register')
        .mockRejectedValue(
          new InternalServerErrorException('Internal Server Error'),
        );

      // act
      const register = controller.register(registerDto);

      // assert
      await expect(register).rejects.toEqual(
        new InternalServerErrorException('Internal Server Error'),
      );
      expect(registerSpy).toBeCalledTimes(1);
      expect(registerSpy).toBeCalledWith(registerDto);
    });
  });

  describe('login', () => {
    it('should return new token login', async () => {
      // arrange
      const loginDto: LoginRequestDto = {
        username: mockPartner.username,
        password: faker.string.sample(),
      };

      const loginSpy = jest
        .spyOn(partnerService, 'login')
        .mockResolvedValue(mockToken);

      // act
      const newToken = await controller.login(loginDto);

      // assert
      expect(newToken).toEqual(mockToken);
      expect(loginSpy).toBeCalledTimes(1);
      expect(loginSpy).toBeCalledWith(loginDto);
    });

    it('should throw unauthorized exception', async () => {
      // arrange
      const loginDto: LoginRequestDto = {
        username: mockPartner.username,
        password: faker.string.sample(),
      };

      const loginSpy = jest
        .spyOn(partnerService, 'login')
        .mockRejectedValue(new UnauthorizedException('Unauthorize'));

      // act
      const login = controller.login(loginDto);

      // assert
      await expect(login).rejects.toEqual(
        new UnauthorizedException('Unauthorize'),
      );
      expect(loginSpy).toBeCalledTimes(1);
      expect(loginSpy).toBeCalledWith(loginDto);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const loginDto: LoginRequestDto = {
        username: mockPartner.username,
        password: faker.string.sample(),
      };

      const loginSpy = jest
        .spyOn(partnerService, 'login')
        .mockRejectedValue(
          new InternalServerErrorException('Internal Server Error'),
        );

      // act
      const login = controller.login(loginDto);

      // assert
      await expect(login).rejects.toEqual(
        new InternalServerErrorException('Internal Server Error'),
      );
      expect(loginSpy).toBeCalledTimes(1);
      expect(loginSpy).toBeCalledWith(loginDto);
    });
  });

  describe('getMe', () => {
    it('should return authenticated cognitoId and username', async () => {
      // arrange
      const user: CognitoUserDto = {
        cognitoId: faker.string.uuid(),
        username: faker.internet.userName(),
      };

      // act
      const response = controller.getMe(user);

      // assert
      expect(response).toEqual(user);
    });
  });
});
