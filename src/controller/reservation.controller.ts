import {
  BadRequestException,
  Body,
  Controller,
  HttpCode,
  InternalServerErrorException,
  Logger,
  NotFoundException,
  Post,
  UnprocessableEntityException,
  UseGuards,
} from '@nestjs/common';
import JwtGuard from 'src/auth/jwt.guard';
import { GetUser } from 'src/core/get-user.decorator';
import { CognitoUserDto } from 'src/dto/cognito-user.dto';
import { ReservationDto } from 'src/dto/reservation.dto';
import { ReservationService } from 'src/service/reservation.service';

@Controller({ version: '1' })
export class ReservationController {
  private readonly logger = new Logger(ReservationController.name);
  constructor(private readonly reservationService: ReservationService) {}

  @UseGuards(JwtGuard)
  @HttpCode(200)
  @Post('reservation')
  async newReservation(
    @GetUser() user: CognitoUserDto,
    @Body() reservationDto: ReservationDto,
  ) {
    this.logger.log(`[POST] /api/v1/reservation`);
    try {
      const reservation = await this.reservationService.makeReservation(
        user,
        reservationDto,
      );
      this.logger.log('Return data reservation');
      return reservation;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 400) {
        throw new BadRequestException(error.response.message);
      } else if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else if (error.response.statusCode === 422) {
        throw new UnprocessableEntityException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }
}
