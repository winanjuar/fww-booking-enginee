import { Test, TestingModule } from '@nestjs/testing';
import { InquiryController } from './inquiry.controller';
import { InquiryService } from 'src/service/inquiry.service';
import { CoreService } from 'src/service/core.service';
import { IAirport } from 'src/interface/core-service/airport.interface';
import { faker, fakerID_ID } from '@faker-js/faker';
import { ETimezone } from 'src/enum/timezone.enum';
import { FilterAirportDto } from 'src/dto/filter-airport.dto';
import {
  BadRequestException,
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { IAirplane } from 'src/interface/core-service/airplane.interface';
import { IPrice } from 'src/interface/core-service/price.interface';
import { IFlight } from 'src/interface/core-service/flight.interface';
import { FlightCandidateDto } from 'src/dto/flight-candidate.dto';
import { ESeatClass } from 'src/enum/seat-class.enum';
import { IBaggage } from 'src/interface/core-service/baggage.interface';
import { ISeat } from 'src/interface/core-service/seat.interface';
import { SeatDto } from 'src/dto/seat.dto';
import { ESeatSide } from 'src/enum/seat-side.enum';
import { ESeatPosition } from 'src/enum/seat-position.enum';
import { ChoseSeatDto } from 'src/dto/chose-seat.dto';
import { IChoseSeatResult } from 'src/interface/chose-seat-result.interface';
import { ESeatStatus } from 'src/enum/seat-status.enum';

describe('InquiryController', () => {
  let controller: InquiryController;

  const inquiryService = {
    getFlightCandidate: jest.fn(),
    getInfoBaggage: jest.fn(),
    getAvailableSeat: jest.fn(),
    chooseSeat: jest.fn(),
    unchooseSeat: jest.fn(),
  };

  const coreService = {
    fetchAirport: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [InquiryController],
      providers: [
        { provide: InquiryService, useValue: inquiryService },
        { provide: CoreService, useValue: coreService },
      ],
    }).compile();
    module.useLogger(false);
    controller = module.get<InquiryController>(InquiryController);
  });

  afterEach(() => jest.clearAllMocks());

  describe('searchAirport', () => {
    let mockAirport: IAirport;
    let mockQueryName: string;
    let mockQueryCity: string;
    let mockQueryCode: string;

    beforeEach(() => {
      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockQueryName =
        mockAirport.name.length <= 5
          ? mockAirport.name
          : mockAirport.name.substring(
              2,
              Math.floor(Math.random() * (mockAirport.name.length - 3) + 3),
            );

      mockQueryCity =
        mockAirport.city.length <= 5
          ? mockAirport.city
          : mockAirport.city.substring(
              2,
              Math.floor(Math.random() * (mockAirport.city.length - 3) + 3),
            );

      mockQueryCode = mockAirport.code.substring(1);
    });

    afterEach(() => jest.clearAllMocks());

    it('should response list airport without criteria', async () => {
      // arrange
      const query: FilterAirportDto = null;
      const fetchAirport = jest
        .spyOn(coreService, 'fetchAirport')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await controller.searchAirport(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(fetchAirport).toHaveBeenCalledTimes(1);
      expect(fetchAirport).toHaveBeenCalledWith(query);
    });

    it('should response list airport with name, city and code contains criteria value', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: mockQueryName,
        code: mockQueryCode,
        city: mockQueryCity,
      };

      const fetchAirport = jest
        .spyOn(coreService, 'fetchAirport')
        .mockResolvedValue([mockAirport]);

      // act
      const airports = await controller.searchAirport(query);

      // assert
      expect(airports).toEqual([mockAirport]);
      expect(fetchAirport).toHaveBeenCalledTimes(1);
      expect(fetchAirport).toHaveBeenCalledWith(query);
      airports.forEach((airport) => expect(airport.name).toContain(query.name));
      airports.forEach((airport) => expect(airport.city).toContain(query.city));
      airports.forEach((airport) => expect(airport.code).toContain(query.code));
    });

    it('should throw unauthorized exception', async () => {
      // arrange
      const differentName = faker.airline.airport().name;
      const differentCity = fakerID_ID.location.city();

      const name =
        differentName.length <= 5
          ? differentName
          : differentName.substring(
              2,
              Math.floor(Math.random() * (differentName.length - 3) + 3),
            );

      const city =
        differentCity.length <= 5
          ? differentCity
          : differentCity.substring(
              2,
              Math.floor(Math.random() * (differentCity.length - 3) + 3),
            );

      const query: FilterAirportDto = {
        name,
        code: null,
        city,
      };

      const fetchAirport = jest
        .spyOn(coreService, 'fetchAirport')
        .mockRejectedValue(new UnauthorizedException('Unauthorized'));

      // act
      const searchAirport = controller.searchAirport(query);

      // assert
      await expect(searchAirport).rejects.toEqual(
        new UnauthorizedException('Unauthorized'),
      );
      expect(fetchAirport).toHaveBeenCalledTimes(1);
      expect(fetchAirport).toHaveBeenCalledWith(query);
    });

    it('should throw not found exception', async () => {
      // arrange
      const differentName = faker.airline.airport().name;
      const differentCity = fakerID_ID.location.city();

      const name =
        differentName.length <= 5
          ? differentName
          : differentName.substring(
              2,
              Math.floor(Math.random() * (differentName.length - 3) + 3),
            );

      const city =
        differentCity.length <= 5
          ? differentCity
          : differentCity.substring(
              2,
              Math.floor(Math.random() * (differentCity.length - 3) + 3),
            );

      const query: FilterAirportDto = {
        name,
        code: null,
        city,
      };

      const fetchAirport = jest
        .spyOn(coreService, 'fetchAirport')
        .mockRejectedValue(new NotFoundException('Airport not found'));

      // act
      const searchAirport = controller.searchAirport(query);

      // assert
      await expect(searchAirport).rejects.toEqual(
        new NotFoundException('Airport not found'),
      );
      expect(fetchAirport).toHaveBeenCalledTimes(1);
      expect(fetchAirport).toHaveBeenCalledWith(query);
    });

    it('should throw internal server exception', async () => {
      // arrange
      const query: FilterAirportDto = {
        name: null,
        code: null,
        city: null,
      };

      const fetchAirport = jest
        .spyOn(coreService, 'fetchAirport')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const searchAirport = controller.searchAirport(query);

      // assert
      await expect(searchAirport).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(fetchAirport).toHaveBeenCalledTimes(1);
      expect(fetchAirport).toHaveBeenCalledWith(query);
    });
  });

  describe('searchFlightCandidate', () => {
    let mockDeparture: IAirport;
    let mockDestination: IAirport;
    let mockAirplane: IAirplane;
    let mockPrice: IPrice;
    let mockFlight: IFlight;

    let mockFlightDto: FlightCandidateDto;

    beforeEach(async () => {
      mockDeparture = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockDestination = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
      };

      mockPrice = {
        id: faker.number.int({ min: 1 }),
        seatClass: faker.helpers.enumValue(ESeatClass),
        priceConfig: faker.number.int({ min: 500000 }),
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockDeparture,
        destination: mockDestination,
        airplane: mockAirplane,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        prices: [mockPrice],
      };

      mockFlightDto = {
        timezone: faker.helpers.enumValue(ETimezone),
        flightDate: faker.date.future().toISOString().split('T')[0],
        departure: mockDeparture.id,
        destination: mockDestination.id,
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response list flight candidate', async () => {
      // arrange
      const getFlightCandidate = jest
        .spyOn(inquiryService, 'getFlightCandidate')
        .mockResolvedValue([mockFlight]);

      // act
      const flightCandidates = await controller.searchFlightCandidate(
        mockFlightDto,
      );

      // assert
      expect(flightCandidates).toEqual([mockFlight]);
      expect(getFlightCandidate).toHaveBeenCalledTimes(1);
      expect(getFlightCandidate).toHaveBeenCalledWith(mockFlightDto);
    });

    it('should throw bad request exception', async () => {
      // arrange
      mockFlightDto = {
        timezone: faker.helpers.enumValue(ETimezone),
        flightDate: faker.date.future().toISOString().split('T')[0],
        departure: faker.number.int({ min: 1 }),
        destination: faker.number.int({ min: 1 }),
      };

      const getFlightCandidate = jest
        .spyOn(inquiryService, 'getFlightCandidate')
        .mockRejectedValue(new BadRequestException('Bad request'));

      // act
      const searchFlightCandidate =
        controller.searchFlightCandidate(mockFlightDto);

      // assert
      await expect(searchFlightCandidate).rejects.toEqual(
        new BadRequestException('Bad request'),
      );
      expect(getFlightCandidate).toHaveBeenCalledTimes(1);
      expect(getFlightCandidate).toHaveBeenCalledWith(mockFlightDto);
    });

    it('should throw unauthorized exception', async () => {
      // arrange
      const getFlightCandidate = jest
        .spyOn(inquiryService, 'getFlightCandidate')
        .mockRejectedValue(new UnauthorizedException('Unauthorized'));

      // act
      const searchFlightCandidate =
        controller.searchFlightCandidate(mockFlightDto);

      // assert
      await expect(searchFlightCandidate).rejects.toEqual(
        new UnauthorizedException('Unauthorized'),
      );
      expect(getFlightCandidate).toHaveBeenCalledTimes(1);
      expect(getFlightCandidate).toHaveBeenCalledWith(mockFlightDto);
    });

    it('should throw not found exception', async () => {
      // arrange
      mockFlightDto = {
        timezone: faker.helpers.enumValue(ETimezone),
        flightDate: faker.date.future().toISOString().split('T')[0],
        departure: faker.number.int({ min: 1 }),
        destination: faker.number.int({ min: 1 }),
      };

      const getFlightCandidate = jest
        .spyOn(inquiryService, 'getFlightCandidate')
        .mockRejectedValue(new NotFoundException('Flight not found'));

      // act
      const searchFlightCandidate =
        controller.searchFlightCandidate(mockFlightDto);

      // assert
      await expect(searchFlightCandidate).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(getFlightCandidate).toHaveBeenCalledTimes(1);
      expect(getFlightCandidate).toHaveBeenCalledWith(mockFlightDto);
    });

    it('should throw internal server exception', async () => {
      // arrange
      const getFlightCandidate = jest
        .spyOn(inquiryService, 'getFlightCandidate')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const searchFlightCandidate =
        controller.searchFlightCandidate(mockFlightDto);

      // assert
      await expect(searchFlightCandidate).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(getFlightCandidate).toHaveBeenCalledTimes(1);
      expect(getFlightCandidate).toHaveBeenCalledWith(mockFlightDto);
    });
  });

  describe('infoFlighBaggage', () => {
    let mockFlight: IFlight;
    let mockAirport: IAirport;
    let mockAirplane: IAirplane;
    let mockBaggage: IBaggage;

    beforeEach(async () => {
      mockBaggage = {
        id: faker.number.int({ min: 1 }),
        capacity: faker.number.int({ min: 1 }),
        category: faker.helpers.arrayElement([
          'Cabin',
          'Normal',
          'Over',
          'Extra Over',
        ]),
        price: faker.number.int({ min: 0 }),
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
        baggages: [mockBaggage],
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response a flight with baggage info', async () => {
      // arrange
      const id = mockFlight.id;
      const getInfoBaggage = jest
        .spyOn(inquiryService, 'getInfoBaggage')
        .mockResolvedValue(mockFlight);

      // act
      const flight = await controller.infoFlighBaggage(id);

      // assert
      expect(flight).toEqual(mockFlight);
      expect(getInfoBaggage).toHaveBeenCalledTimes(1);
      expect(getInfoBaggage).toHaveBeenCalledWith(id);
    });

    it('should throw unauthorized exception', async () => {
      // arrange
      const id = mockFlight.id;
      const getInfoBaggage = jest
        .spyOn(inquiryService, 'getInfoBaggage')
        .mockRejectedValue(new UnauthorizedException('Unauthorized'));

      // act
      const infoFlighBaggage = controller.infoFlighBaggage(id);

      // assert
      await expect(infoFlighBaggage).rejects.toEqual(
        new UnauthorizedException('Unauthorized'),
      );
      expect(getInfoBaggage).toHaveBeenCalledTimes(1);
      expect(getInfoBaggage).toHaveBeenCalledWith(id);
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });
      const getInfoBaggage = jest
        .spyOn(inquiryService, 'getInfoBaggage')
        .mockRejectedValue(new NotFoundException('Flight not found'));

      // act
      const infoFlighBaggage = controller.infoFlighBaggage(id);

      // assert
      await expect(infoFlighBaggage).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(getInfoBaggage).toHaveBeenCalledTimes(1);
      expect(getInfoBaggage).toHaveBeenCalledWith(id);
    });

    it('should throw internal server exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });
      const getInfoBaggage = jest
        .spyOn(inquiryService, 'getInfoBaggage')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const infoFlighBaggage = controller.infoFlighBaggage(id);

      // assert
      await expect(infoFlighBaggage).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(getInfoBaggage).toHaveBeenCalledTimes(1);
      expect(getInfoBaggage).toHaveBeenCalledWith(id);
    });
  });

  describe('infoFlighAvailableSeat', () => {
    let mockFlight: IFlight;
    let mockAirport: IAirport;
    let mockAirplane: IAirplane;
    let mockSeat: ISeat;
    let mockSeatDto: SeatDto;

    beforeEach(async () => {
      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        seats: [mockSeat],
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
      };

      mockSeatDto = {
        flight: mockFlight.id,
        flightDate: faker.date.future().toISOString().split('T')[0],
        timezone: faker.helpers.enumValue(ETimezone),
        seatClass: faker.helpers.enumValue(ESeatClass),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response a flight with available seat', async () => {
      // arrange
      const getAvailableSeat = jest
        .spyOn(inquiryService, 'getAvailableSeat')
        .mockResolvedValue(mockFlight);

      // act
      const flight = await controller.infoFlighAvailableSeat(mockSeatDto);

      // assert
      expect(flight).toEqual(mockFlight);
      expect(getAvailableSeat).toHaveBeenCalledTimes(1);
      expect(getAvailableSeat).toHaveBeenCalledWith(mockSeatDto);
    });

    it('should throw bad request exception', async () => {
      // arrange
      const getAvailableSeat = jest
        .spyOn(inquiryService, 'getAvailableSeat')
        .mockRejectedValue(new BadRequestException('Bad request'));

      // act
      const infoFlighAvailableSeat =
        controller.infoFlighAvailableSeat(mockSeatDto);

      // assert
      await expect(infoFlighAvailableSeat).rejects.toEqual(
        new BadRequestException('Bad request'),
      );
      expect(getAvailableSeat).toHaveBeenCalledTimes(1);
      expect(getAvailableSeat).toHaveBeenCalledWith(mockSeatDto);
    });

    it('should throw unauthorized exception', async () => {
      // arrange
      const getAvailableSeat = jest
        .spyOn(inquiryService, 'getAvailableSeat')
        .mockRejectedValue(new UnauthorizedException('Unauthorized'));

      // act
      const infoFlighAvailableSeat =
        controller.infoFlighAvailableSeat(mockSeatDto);

      // assert
      await expect(infoFlighAvailableSeat).rejects.toEqual(
        new UnauthorizedException('Unauthorized'),
      );
      expect(getAvailableSeat).toHaveBeenCalledTimes(1);
      expect(getAvailableSeat).toHaveBeenCalledWith(mockSeatDto);
    });

    it('should throw not found exception', async () => {
      // arrange
      mockSeatDto = {
        flight: faker.number.int({ min: 1 }),
        flightDate: faker.date.future().toISOString().split('T')[0],
        timezone: faker.helpers.enumValue(ETimezone),
        seatClass: faker.helpers.enumValue(ESeatClass),
      };

      const getAvailableSeat = jest
        .spyOn(inquiryService, 'getAvailableSeat')
        .mockRejectedValue(new NotFoundException('Flight not found'));

      // act
      const infoFlighAvailableSeat =
        controller.infoFlighAvailableSeat(mockSeatDto);

      // assert
      await expect(infoFlighAvailableSeat).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(getAvailableSeat).toHaveBeenCalledTimes(1);
      expect(getAvailableSeat).toHaveBeenCalledWith(mockSeatDto);
    });

    it('should throw internal server exception', async () => {
      // arrange
      const getAvailableSeat = jest
        .spyOn(inquiryService, 'getAvailableSeat')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const infoFlighAvailableSeat =
        controller.infoFlighAvailableSeat(mockSeatDto);

      // assert
      await expect(infoFlighAvailableSeat).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(getAvailableSeat).toHaveBeenCalledTimes(1);
      expect(getAvailableSeat).toHaveBeenCalledWith(mockSeatDto);
    });
  });

  describe('chooseSeat', () => {
    let mockFlight: IFlight;
    let mockAirport: IAirport;
    let mockAirplane: IAirplane;
    let mockSeat: ISeat;
    let mockSeatDto: ChoseSeatDto;
    let mockChoseSeatResult: IChoseSeatResult;

    beforeEach(async () => {
      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        seats: [mockSeat],
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
      };

      mockSeatDto = {
        passenger: faker.string.numeric({ length: 16 }),
        flight: mockFlight.id,
        flightDate: faker.date.future().toISOString().split('T')[0],
        timezone: faker.helpers.enumValue(ETimezone),
        seat: mockSeat.id,
      };

      mockChoseSeatResult = {
        passenger: mockSeatDto.passenger,
        flightDate: mockSeatDto.flightDate,
        flight: mockSeatDto.flight,
        seat: mockSeat.id,
        status: faker.helpers.enumValue(ESeatStatus),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response choosen seat info', async () => {
      // arrange
      const chooseSeat = jest
        .spyOn(inquiryService, 'chooseSeat')
        .mockResolvedValue(mockChoseSeatResult);

      // act
      const seat = await controller.chooseSeat(mockSeatDto);

      // assert
      expect(seat).toEqual(mockChoseSeatResult);
      expect(chooseSeat).toHaveBeenCalledTimes(1);
      expect(chooseSeat).toHaveBeenCalledWith(mockSeatDto);
    });

    it('should throw bad request exception', async () => {
      // arrange
      const chooseSeatSpy = jest
        .spyOn(inquiryService, 'chooseSeat')
        .mockRejectedValue(new BadRequestException('Bad request'));

      // act
      const chooseSeat = controller.chooseSeat(mockSeatDto);

      // assert
      await expect(chooseSeat).rejects.toEqual(
        new BadRequestException('Bad request'),
      );
      expect(chooseSeatSpy).toHaveBeenCalledTimes(1);
      expect(chooseSeatSpy).toHaveBeenCalledWith(mockSeatDto);
    });

    it('should throw unauthorized exception', async () => {
      // arrange
      const chooseSeatSpy = jest
        .spyOn(inquiryService, 'chooseSeat')
        .mockRejectedValue(new UnauthorizedException('Unauthorized'));

      // act
      const chooseSeat = controller.chooseSeat(mockSeatDto);

      // assert
      await expect(chooseSeat).rejects.toEqual(
        new UnauthorizedException('Unauthorized'),
      );
      expect(chooseSeatSpy).toHaveBeenCalledTimes(1);
      expect(chooseSeatSpy).toHaveBeenCalledWith(mockSeatDto);
    });

    it('should throw not found exception', async () => {
      // arrange
      mockSeatDto = {
        passenger: faker.string.numeric({ length: 16 }),
        flight: faker.number.int({ min: 1 }),
        flightDate: faker.date.future().toISOString().split('T')[0],
        timezone: faker.helpers.enumValue(ETimezone),
        seat: faker.number.int({ min: 1 }),
      };

      const chooseSeatSpy = jest
        .spyOn(inquiryService, 'chooseSeat')
        .mockRejectedValue(new NotFoundException('Flight not found'));

      // act
      const chooseSeat = controller.chooseSeat(mockSeatDto);

      // assert
      await expect(chooseSeat).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(chooseSeatSpy).toHaveBeenCalledTimes(1);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      const chooseSeatSpy = jest
        .spyOn(inquiryService, 'chooseSeat')
        .mockRejectedValue(
          new UnprocessableEntityException('Unprocessable entity'),
        );

      // act
      const chooseSeat = controller.chooseSeat(mockSeatDto);

      // assert
      await expect(chooseSeat).rejects.toEqual(
        new UnprocessableEntityException('Unprocessable entity'),
      );
      expect(chooseSeatSpy).toHaveBeenCalledTimes(1);
    });

    it('should throw internal server exception', async () => {
      // arrange
      const chooseSeatSpy = jest
        .spyOn(inquiryService, 'chooseSeat')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const chooseSeat = controller.chooseSeat(mockSeatDto);

      // assert
      await expect(chooseSeat).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(chooseSeatSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('unchooseSeat', () => {
    let mockFlight: IFlight;
    let mockAirport: IAirport;
    let mockAirplane: IAirplane;
    let mockSeat: ISeat;
    let mockSeatDto: ChoseSeatDto;
    let mockChoseSeatResult: IChoseSeatResult;

    beforeEach(async () => {
      mockSeat = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.seat(),
        seatClass: faker.helpers.enumValue(ESeatClass),
        side: faker.helpers.enumValue(ESeatSide),
        position: faker.helpers.enumValue(ESeatPosition),
      };

      mockAirplane = {
        id: faker.number.int({ min: 1 }),
        name: faker.airline.airplane().name,
        registrationNumber: faker.airline.airplane().iataTypeCode,
        maxPassenger: faker.number.int({ min: 60, max: 172 }),
        maxBusiness: faker.number.int({ min: 0 }),
        maxEconomy: faker.number.int({ min: 50 }),
        chairConfig: faker.string.sample(),
        seats: [mockSeat],
      };

      mockAirport = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.airport().iataCode,
        name: faker.airline.airport().name,
        city: fakerID_ID.location.city(),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      mockFlight = {
        id: faker.number.int({ min: 1 }),
        code: faker.airline.flightNumber(),
        departure: mockAirport,
        destination: mockAirport,
        airplane: mockAirplane,
        departureTime: faker.date.anytime().toTimeString(),
        arrivalTime: faker.date.anytime().toTimeString(),
        durationInMinutes: faker.number.int({ min: 60 }),
      };

      mockSeatDto = {
        passenger: faker.string.numeric({ length: 16 }),
        flight: mockFlight.id,
        flightDate: faker.date.future().toISOString().split('T')[0],
        timezone: faker.helpers.enumValue(ETimezone),
        seat: mockSeat.id,
      };

      mockChoseSeatResult = {
        passenger: mockSeatDto.passenger,
        flightDate: mockSeatDto.flightDate,
        flight: mockSeatDto.flight,
        seat: mockSeat.id,
        status: faker.helpers.enumValue(ESeatStatus),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should response unchoosen seat info', async () => {
      // arrange
      const unchooseSeatSpy = jest
        .spyOn(inquiryService, 'unchooseSeat')
        .mockResolvedValue(mockChoseSeatResult);

      // act
      const seat = await controller.unchooseSeat(mockSeatDto);

      // assert
      expect(seat).toEqual(mockChoseSeatResult);
      expect(unchooseSeatSpy).toHaveBeenCalledTimes(1);
      expect(unchooseSeatSpy).toHaveBeenCalledWith(mockSeatDto);
    });

    it('should throw bad request exception', async () => {
      // arrange
      const unchooseSeatSpy = jest
        .spyOn(inquiryService, 'unchooseSeat')
        .mockRejectedValue(new BadRequestException('Bad request'));

      // act
      const unchooseSeat = controller.unchooseSeat(mockSeatDto);

      // assert
      await expect(unchooseSeat).rejects.toEqual(
        new BadRequestException('Bad request'),
      );
      expect(unchooseSeatSpy).toHaveBeenCalledTimes(1);
      expect(unchooseSeatSpy).toHaveBeenCalledWith(mockSeatDto);
    });

    it('should throw unauthorized exception', async () => {
      // arrange
      const unchooseSeatSpy = jest
        .spyOn(inquiryService, 'unchooseSeat')
        .mockRejectedValue(new UnauthorizedException('Unauthorized'));

      // act
      const unchooseSeat = controller.unchooseSeat(mockSeatDto);

      // assert
      await expect(unchooseSeat).rejects.toEqual(
        new UnauthorizedException('Unauthorized'),
      );
      expect(unchooseSeatSpy).toHaveBeenCalledTimes(1);
      expect(unchooseSeatSpy).toHaveBeenCalledWith(mockSeatDto);
    });

    it('should throw not found exception', async () => {
      // arrange
      mockSeatDto = {
        passenger: faker.string.numeric({ length: 16 }),
        flight: faker.number.int({ min: 1 }),
        flightDate: faker.date.future().toISOString().split('T')[0],
        timezone: faker.helpers.enumValue(ETimezone),
        seat: faker.number.int({ min: 1 }),
      };

      const unchooseSeatSpy = jest
        .spyOn(inquiryService, 'unchooseSeat')
        .mockRejectedValue(new NotFoundException('Flight not found'));

      // act
      const unchooseSeat = controller.unchooseSeat(mockSeatDto);

      // assert
      await expect(unchooseSeat).rejects.toEqual(
        new NotFoundException('Flight not found'),
      );
      expect(unchooseSeatSpy).toHaveBeenCalledTimes(1);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      const unchooseSeatSpy = jest
        .spyOn(inquiryService, 'unchooseSeat')
        .mockRejectedValue(
          new UnprocessableEntityException('Unprocessable entity'),
        );

      // act
      const unchooseSeat = controller.unchooseSeat(mockSeatDto);

      // assert
      await expect(unchooseSeat).rejects.toEqual(
        new UnprocessableEntityException('Unprocessable entity'),
      );
      expect(unchooseSeatSpy).toHaveBeenCalledTimes(1);
    });

    it('should throw internal server exception', async () => {
      // arrange
      const unchooseSeatSpy = jest
        .spyOn(inquiryService, 'unchooseSeat')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const unchooseSeat = controller.unchooseSeat(mockSeatDto);

      // assert
      await expect(unchooseSeat).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(unchooseSeatSpy).toHaveBeenCalledTimes(1);
    });
  });
});
