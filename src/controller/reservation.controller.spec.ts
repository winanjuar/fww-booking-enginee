import { Test, TestingModule } from '@nestjs/testing';
import { ReservationController } from './reservation.controller';
import { CognitoUserDto } from 'src/dto/cognito-user.dto';
import { faker } from '@faker-js/faker';
import { ReservationDto } from 'src/dto/reservation.dto';
import { ETimezone } from 'src/enum/timezone.enum';
import { EReservationStatus } from 'src/enum/reservation-status.enum';
import { ReservationService } from 'src/service/reservation.service';
import {
  BadRequestException,
  InternalServerErrorException,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';

describe('ReservationController', () => {
  let controller: ReservationController;

  const reservationService = {
    makeReservation: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReservationController],
      providers: [
        { provide: ReservationService, useValue: reservationService },
      ],
    }).compile();
    module.useLogger(false);
    controller = module.get<ReservationController>(ReservationController);
  });

  afterEach(() => jest.clearAllMocks());

  describe('newReservation', () => {
    it('should return new reservation just made', async () => {
      // arrange
      const user: CognitoUserDto = {
        cognitoId: faker.string.uuid(),
        username: faker.internet.userName(),
      };

      const reservationDto: ReservationDto = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date.birthdate().toISOString(),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        member: faker.number.int({ min: 1 }),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: faker.number.int({ min: 1 }),
        seat: faker.number.int({ min: 1 }),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      const mockReservationResult = {
        messageResponse:
          'Success make a reservation, we will deliver booking code via email as soon as possible',
        data: {
          id: faker.number.int({ min: 1 }),
          reservationTime: faker.date.recent().toISOString(),
          status: faker.helpers.enumValue(EReservationStatus),
        },
      };

      const makeReservation = jest
        .spyOn(reservationService, 'makeReservation')
        .mockResolvedValue(mockReservationResult);

      // act
      const newReservation = await controller.newReservation(
        user,
        reservationDto,
      );

      // assert
      expect(newReservation).toEqual(mockReservationResult);
      expect(makeReservation).toBeCalledTimes(1);
      expect(makeReservation).toBeCalledWith(user, reservationDto);
    });

    it('should throw bad request exception', async () => {
      // arrange
      const user: CognitoUserDto = {
        cognitoId: faker.string.uuid(),
        username: faker.internet.userName(),
      };

      const reservationDto: ReservationDto = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date.birthdate().toISOString(),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        member: faker.number.int({ min: 1 }),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: faker.number.int({ min: 1 }),
        seat: faker.number.int({ min: 1 }),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      const makeReservationSpy = jest
        .spyOn(reservationService, 'makeReservation')
        .mockRejectedValue(new BadRequestException('Bad request'));

      // act
      const newReservation = controller.newReservation(user, reservationDto);

      // assert
      await expect(newReservation).rejects.toEqual(
        new BadRequestException('Bad request'),
      );
      expect(makeReservationSpy).toBeCalledTimes(1);
      expect(makeReservationSpy).toBeCalledWith(user, reservationDto);
    });

    it('should throw not found exception', async () => {
      // arrange
      const user: CognitoUserDto = {
        cognitoId: faker.string.uuid(),
        username: faker.internet.userName(),
      };

      const reservationDto: ReservationDto = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date.birthdate().toISOString(),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        member: faker.number.int({ min: 1 }),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: faker.number.int({ min: 1 }),
        seat: faker.number.int({ min: 1 }),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      const makeReservationSpy = jest
        .spyOn(reservationService, 'makeReservation')
        .mockRejectedValue(new NotFoundException('Not found'));

      // act
      const newReservation = controller.newReservation(user, reservationDto);

      // assert
      await expect(newReservation).rejects.toEqual(
        new NotFoundException('Not found'),
      );
      expect(makeReservationSpy).toBeCalledTimes(1);
      expect(makeReservationSpy).toBeCalledWith(user, reservationDto);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      const user: CognitoUserDto = {
        cognitoId: faker.string.uuid(),
        username: faker.internet.userName(),
      };

      const reservationDto: ReservationDto = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date.birthdate().toISOString(),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        member: faker.number.int({ min: 1 }),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: faker.number.int({ min: 1 }),
        seat: faker.number.int({ min: 1 }),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      const makeReservationSpy = jest
        .spyOn(reservationService, 'makeReservation')
        .mockRejectedValue(
          new UnprocessableEntityException('Unprocessable entity'),
        );

      // act
      const newReservation = controller.newReservation(user, reservationDto);

      // assert
      await expect(newReservation).rejects.toEqual(
        new UnprocessableEntityException('Unprocessable entity'),
      );
      expect(makeReservationSpy).toBeCalledTimes(1);
      expect(makeReservationSpy).toBeCalledWith(user, reservationDto);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const user: CognitoUserDto = {
        cognitoId: faker.string.uuid(),
        username: faker.internet.userName(),
      };

      const reservationDto: ReservationDto = {
        identityNumber: faker.string.numeric({ length: 16 }),
        name: faker.person.fullName(),
        birthDate: faker.date.birthdate().toISOString(),
        phone: faker.phone.number(),
        email: faker.internet.email(),
        member: faker.number.int({ min: 1 }),
        flightDate: faker.date.future().toISOString().split('T')[0],
        flight: faker.number.int({ min: 1 }),
        seat: faker.number.int({ min: 1 }),
        timezone: faker.helpers.enumValue(ETimezone),
      };

      const makeReservationSpy = jest
        .spyOn(reservationService, 'makeReservation')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const newReservation = controller.newReservation(user, reservationDto);

      // assert
      await expect(newReservation).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(makeReservationSpy).toBeCalledTimes(1);
      expect(makeReservationSpy).toBeCalledWith(user, reservationDto);
    });
  });
});
