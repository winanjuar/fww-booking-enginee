import { Test, TestingModule } from '@nestjs/testing';
import { MailController } from './mail.controller';
import { MailService } from 'src/service/mail.service';
import { NormalMailRequestDto } from 'src/dto/normal-mail-request.dto';
import { faker } from '@faker-js/faker';
import { ETimezoneCode } from 'src/enum/timezone-code.enum';
import { IMailNormal } from 'src/interface/mail-normal.interface';
import { DeclineMailRequestDto } from 'src/dto/decline-mail-request.dto';
import { IMailDecline } from 'src/interface/mail-decline.interface';
import { InternalServerErrorException } from '@nestjs/common';

describe('MailController', () => {
  let controller: MailController;
  let mockMailNormalDto: NormalMailRequestDto;
  let mockMailDeclineDto: DeclineMailRequestDto;

  const mailService = {
    sendNormalMail: jest.fn(),
    sendDeclineMail: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MailController],
      providers: [{ provide: MailService, useValue: mailService }],
    }).compile();
    module.useLogger(false);
    controller = module.get<MailController>(MailController);

    mockMailNormalDto = {
      mailTo: faker.internet.email(),
      flightCode: faker.airline.flightNumber(),
      flightDate: faker.date.future().toISOString().split('T')[0],
      departureAirport: faker.airline.airport().name,
      departureCode: faker.airline.airport().iataCode,
      departureTime: faker.date.anytime().toTimeString(),
      departureTimezone: faker.helpers.enumValue(ETimezoneCode),
      destinationAirport: faker.airline.airport().name,
      destinationCode: faker.airline.airport().iataCode,
      arrivalTime: faker.date.anytime().toTimeString(),
      arrivalTimezone: faker.helpers.enumValue(ETimezoneCode),
      seatNumber: faker.airline.seat(),
      typeCode: faker.helpers.arrayElement([
        'Booking Code',
        'Reservation Code',
      ]),
      realCode: faker.string.alphanumeric({ casing: 'upper', length: 8 }),
      nextProcess: faker.helpers.arrayElement(['pembayaran', 'redeem']),
      deadline: faker.date.future().toISOString(),
      consequence: faker.string.sample(),
      specialNote: faker.string.sample(),
    };

    mockMailDeclineDto = {
      mailTo: faker.internet.email(),
      flightCode: faker.airline.flightNumber(),
      flightDate: faker.date.future().toISOString().split('T')[0],
      departureAirport: faker.airline.airport().name,
      departureCode: faker.airline.airport().iataCode,
      departureTime: faker.date.anytime().toTimeString(),
      departureTimezone: faker.helpers.enumValue(ETimezoneCode),
      destinationAirport: faker.airline.airport().name,
      destinationCode: faker.airline.airport().iataCode,
      arrivalTime: faker.date.anytime().toTimeString(),
      arrivalTimezone: faker.helpers.enumValue(ETimezoneCode),
      seatNumber: faker.airline.seat(),
      reservationStatus: 'DIBATALKAN',
      reason: faker.string.sample(),
    };
  });

  afterEach(() => jest.clearAllMocks());

  describe('sendNormalMail', () => {
    it('should send normal mail', async () => {
      // arrange
      const sendNormalMailSpy = jest.spyOn(mailService, 'sendNormalMail');

      // act
      const sendNormalMail = controller.sendNormalMail(mockMailNormalDto);

      // assert
      await expect(sendNormalMail).resolves.not.toThrow();
      expect(sendNormalMailSpy).toBeCalledTimes(1);
      expect(sendNormalMailSpy).toBeCalledWith(mockMailNormalDto);
    });
  });

  describe('handleNormalMail', () => {
    it('should send normal mail', async () => {
      // arrange
      const dataNormalMail: IMailNormal = mockMailNormalDto;
      const sendNormalMailSpy = jest.spyOn(mailService, 'sendNormalMail');

      // act
      const handleNormalMail = controller.handleNormalMail(dataNormalMail);

      // assert
      await expect(handleNormalMail).resolves.not.toThrow();
      expect(sendNormalMailSpy).toBeCalledTimes(1);
      expect(sendNormalMailSpy).toBeCalledWith(dataNormalMail);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const dataNormalMail: IMailNormal = mockMailNormalDto;
      const sendNormalMailSpy = jest
        .spyOn(mailService, 'sendNormalMail')
        .mockRejectedValue(
          new InternalServerErrorException('Error in handleNormalMail'),
        );

      // act
      const handleNormalMail = controller.handleNormalMail(dataNormalMail);

      // assert
      await expect(handleNormalMail).rejects.toEqual(
        new InternalServerErrorException('Error in handleNormalMail'),
      );
      expect(sendNormalMailSpy).toBeCalledTimes(1);
      expect(sendNormalMailSpy).toBeCalledWith(dataNormalMail);
    });
  });

  describe('sendDeclineMail', () => {
    it('should send normal mail', async () => {
      // arrange
      const sendDeclineMailSpy = jest.spyOn(mailService, 'sendDeclineMail');

      // act
      const sendDeclineMail = controller.sendDeclineMail(mockMailDeclineDto);

      // assert
      await expect(sendDeclineMail).resolves.not.toThrow();
      expect(sendDeclineMailSpy).toBeCalledTimes(1);
      expect(sendDeclineMailSpy).toBeCalledWith(mockMailDeclineDto);
    });
  });

  describe('handleDeclineMail', () => {
    it('should send normal mail', async () => {
      // arrange
      const dataDeclineMail: IMailDecline = mockMailDeclineDto;
      const sendDeclineMailSpy = jest.spyOn(mailService, 'sendDeclineMail');

      // act
      const handleDeclineMail = controller.handleDeclineMail(dataDeclineMail);

      // assert
      await expect(handleDeclineMail).resolves.not.toThrow();
      expect(sendDeclineMailSpy).toBeCalledTimes(1);
      expect(sendDeclineMailSpy).toBeCalledWith(dataDeclineMail);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const dataDeclineMail: IMailDecline = mockMailDeclineDto;
      const sendDeclineMailSpy = jest
        .spyOn(mailService, 'sendDeclineMail')
        .mockRejectedValue(
          new InternalServerErrorException('Error in handleDeclineMail'),
        );

      // act
      const handleDeclineMail = controller.handleDeclineMail(dataDeclineMail);

      // assert
      await expect(handleDeclineMail).rejects.toEqual(
        new InternalServerErrorException('Error in handleDeclineMail'),
      );
      expect(sendDeclineMailSpy).toBeCalledTimes(1);
      expect(sendDeclineMailSpy).toBeCalledWith(dataDeclineMail);
    });
  });
});
