import {
  Body,
  Controller,
  InternalServerErrorException,
  Logger,
  Post,
} from '@nestjs/common';
import { MailService } from 'src/service/mail.service';
import { NormalMailRequestDto } from 'src/dto/normal-mail-request.dto';
import { DeclineMailRequestDto } from 'src/dto/decline-mail-request.dto';
import { EventPattern, Payload } from '@nestjs/microservices';
import { EPatternMessage } from 'src/core/pattern-message.enum';
import { IMailNormal } from 'src/interface/mail-normal.interface';
import { IMailDecline } from 'src/interface/mail-decline.interface';

@Controller({ version: '1' })
export class MailController {
  constructor(private readonly mailService: MailService) {}
  private readonly logger = new Logger(MailController.name);

  // buat ngetes email manual
  @Post('send-normal-mail')
  async sendNormalMail(@Body() mailDto: NormalMailRequestDto) {
    await this.mailService.sendNormalMail(mailDto);
    this.logger.log('Send normal mail done');
    return mailDto;
  }

  // buat ngetes email manual
  @Post('send-decline-mail')
  async sendDeclineMail(@Body() mailDto: DeclineMailRequestDto) {
    await this.mailService.sendDeclineMail(mailDto);
    this.logger.log('Send decline mail done');
    return mailDto;
  }

  @EventPattern(EPatternMessage.SEND_NORMAL_MAIL)
  async handleNormalMail(@Payload() data: any) {
    try {
      const dataEmail = data as IMailNormal;
      await this.mailService.sendNormalMail(dataEmail);
      this.logger.log('Send normal mail done');
    } catch (error) {
      throw new InternalServerErrorException('Error in handleNormalMail');
    }
  }

  @EventPattern(EPatternMessage.SEND_DECLINE_MAIL)
  async handleDeclineMail(@Payload() data: any) {
    try {
      const dataEmail = data as IMailDecline;
      await this.mailService.sendDeclineMail(dataEmail);
      this.logger.log('Send decline mail done');
    } catch (error) {
      throw new InternalServerErrorException('Error in handleDeclineMail');
    }
  }
}
