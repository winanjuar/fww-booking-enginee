import {
  Body,
  Controller,
  Get,
  HttpCode,
  InternalServerErrorException,
  Logger,
  Post,
  UnauthorizedException,
  UnprocessableEntityException,
  UseGuards,
} from '@nestjs/common';
import JwtGuard from 'src/auth/jwt.guard';
import { GetUser } from 'src/core/get-user.decorator';
import { CognitoUserDto } from 'src/dto/cognito-user.dto';
import { LoginRequestDto } from 'src/dto/login.request.dto';
import { RegisterRequestDto } from 'src/dto/register-partner.dto';

import { PartnerService } from 'src/service/partner.service';

@Controller({ version: '1' })
export class PartnerController {
  private readonly logger = new Logger(PartnerController.name);
  constructor(private readonly partnerService: PartnerService) {}

  @Post('register')
  async register(@Body() registerDto: RegisterRequestDto) {
    this.logger.log('[POST] /api/v1/register');
    try {
      const newPartner = await this.partnerService.register(registerDto);
      this.logger.log('Return new partner');
      return newPartner;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 422) {
        throw new UnprocessableEntityException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @HttpCode(200)
  @Post('login')
  async login(@Body() loginRequest: LoginRequestDto) {
    this.logger.log('[POST] /api/v1/login');
    try {
      const token = await this.partnerService.login(loginRequest);
      this.logger.log('Return token');
      return token;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 401) {
        throw new UnauthorizedException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(JwtGuard)
  @Get('whoami')
  getMe(@GetUser() user: CognitoUserDto) {
    this.logger.log('[GET] /api/v1/whoami');
    this.logger.log('Return user logged in');
    return user;
  }
}
