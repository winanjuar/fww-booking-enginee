import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  InternalServerErrorException,
  Logger,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Post,
  UnprocessableEntityException,
  UseGuards,
} from '@nestjs/common';
import { IWorkflowDetail } from 'src/interface/workflow.interface';
import { EventPattern, Payload } from '@nestjs/microservices';
import { EPatternMessage } from 'src/core/pattern-message.enum';
import { WorkflowService } from 'src/service/workflow.service';
import { ChargePaymentDto } from 'src/dto/charge-payment.dto';
import { AuthGuard } from '@nestjs/passport';
import { ReservationCodeDto } from 'src/dto/reservation-code.dto';
import { ISeatStatusUpdate } from 'src/interface/seat-status-update.interface';
import JwtGuard from 'src/auth/jwt.guard';

@Controller({ version: '1' })
export class WorkflowController {
  private readonly logger = new Logger(WorkflowController.name);
  constructor(private readonly workflowService: WorkflowService) {}

  @UseGuards(AuthGuard('basic'))
  @Get('check-payment/:paymentId')
  async checkPayment(@Param('paymentId', ParseUUIDPipe) paymentId: string) {
    try {
      this.logger.log(`[GET] /api/v1/check-payment/${paymentId}`);
      const result = await this.workflowService.checkPayment(paymentId);
      this.logger.log('Return status midtrans');
      return result;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 400) {
        throw new BadRequestException(error.response.message);
      } else if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else if (error.response.statusCode === 422) {
        throw new UnprocessableEntityException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(JwtGuard)
  @Post('commit-payment')
  async commitPayment(@Body('reservationId') reservationId: number) {
    try {
      this.logger.log(`[POST] /api/v1/commit-payment`);
      const result = await this.workflowService.commitPayment(reservationId);
      this.logger.log('Process workflow commit payment done');
      return result;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 400) {
        throw new BadRequestException(error.response.message);
      } else if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else if (error.response.statusCode === 422) {
        throw new UnprocessableEntityException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(JwtGuard)
  @HttpCode(200)
  @Post('charge-transaction')
  async chargeTransaction(@Body() chargeDto: ChargePaymentDto) {
    try {
      this.logger.log(`[POST] /api/v1/charge-transaction`);
      const result = await this.workflowService.chargeTransaction(chargeDto);
      this.logger.log('Process workflow commit payment done');
      return result;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 400) {
        throw new BadRequestException(error.response.message);
      } else if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else if (error.response.statusCode === 422) {
        throw new UnprocessableEntityException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(JwtGuard)
  @HttpCode(200)
  @Post('confirm-payment')
  async confirmPayment(@Body('reservationId') reservationId: number) {
    try {
      this.logger.log(`[POST] /api/v1/confirm-payment`);
      const result = await this.workflowService.confirmPayment(reservationId);
      this.logger.log('Process workflow confirm payment done');
      return result;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 400) {
        throw new BadRequestException(error.response.message);
      } else if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else if (error.response.statusCode === 422) {
        throw new UnprocessableEntityException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Post('redeem-ticket')
  async redeemTicket(@Body() reservationDto: ReservationCodeDto) {
    try {
      this.logger.log(`[POST] /api/v1/redeem-ticket`);
      const result = await this.workflowService.redeemTicket(reservationDto);
      this.logger.log('Process workflow redeem ticket done');
      return result;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 400) {
        throw new BadRequestException(error.response.message);
      } else if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else if (error.response.statusCode === 422) {
        throw new UnprocessableEntityException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Post('onboard')
  async onboard(@Body('ticketNumber') ticketNumber: string) {
    try {
      this.logger.log(`[POST] /api/v1/onboard`);
      const result = await this.workflowService.onboard(ticketNumber);
      this.logger.log('Process workflow onboard done');
      return result;
    } catch (error) {
      this.logger.error(error.response.message);
      if (error.response.statusCode === 400) {
        throw new BadRequestException(error.response.message);
      } else if (error.response.statusCode === 404) {
        throw new NotFoundException(error.response.message);
      } else if (error.response.statusCode === 422) {
        throw new UnprocessableEntityException(error.response.message);
      } else {
        throw new InternalServerErrorException(error.response.message);
      }
    }
  }

  @EventPattern(EPatternMessage.REPORT_TASK_WORKFLOW)
  async handleReportTaskWorkflow(@Payload() data: any) {
    try {
      const dataWorkflow = data as IWorkflowDetail;
      await this.workflowService.insertWorkflowDetail(dataWorkflow);
      this.logger.log('Process data workflow detail done');
    } catch (error) {
      throw new InternalServerErrorException(
        'Error in handleReportTaskWorkflow',
      );
    }
  }

  @EventPattern(EPatternMessage.UPDATE_SEAT_STATUS)
  async handleUpdateSeatStatus(@Payload() data: any) {
    try {
      const seatStatusUpdate = data as ISeatStatusUpdate;
      await this.workflowService.updateSeatStatus(seatStatusUpdate);
      this.logger.log('Process reset seat status done');
    } catch (error) {
      throw new InternalServerErrorException('Error in handleUpdateSeatStatus');
    }
  }
}
