import { Test, TestingModule } from '@nestjs/testing';
import { WorkflowController } from './workflow.controller';
import { WorkflowService } from 'src/service/workflow.service';
import { IPaymentUpdate } from 'src/interface/payment.interface';
import { faker } from '@faker-js/faker';
import {
  BadRequestException,
  InternalServerErrorException,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { ICommitPayment } from 'src/interface/commit-payment.interface';
import { IChargeTransaction } from 'src/interface/charge-transaction.interface';
import { EBankChoice } from 'src/enum/bank-choice.enum';
import { ChargePaymentDto } from 'src/dto/charge-payment.dto';
import { ReservationCodeDto } from 'src/dto/reservation-code.dto';
import {
  IWorkflowDetail,
  IWorkflowMaster,
} from 'src/interface/workflow.interface';
import { ETaskName } from 'src/enum/task-name.enum';
import { ISeatStatusUpdate } from 'src/interface/seat-status-update.interface';
import { ESeatStatus } from 'src/enum/seat-status.enum';

describe('WorkflowController', () => {
  let controller: WorkflowController;

  const workflowService = {
    checkPayment: jest.fn(),
    commitPayment: jest.fn(),
    chargeTransaction: jest.fn(),
    confirmPayment: jest.fn(),
    redeemTicket: jest.fn(),
    onboard: jest.fn(),
    insertWorkflowDetail: jest.fn(),
    updateSeatStatus: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WorkflowController],
      providers: [{ provide: WorkflowService, useValue: workflowService }],
    }).compile();
    module.useLogger(false);
    controller = module.get<WorkflowController>(WorkflowController);
  });

  afterEach(() => jest.clearAllMocks());

  describe('checkPayment', () => {
    let mockPaymentUpdate: IPaymentUpdate;

    beforeEach(() => {
      mockPaymentUpdate = {
        id: faker.string.uuid(),
        paymentStatus: faker.helpers.arrayElement(['pending', 'settlement']),
        checkTime: faker.date.recent().toISOString(),
        paymentTime: faker.date.past().toISOString(),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return status payment', async () => {
      // arrange
      const id = mockPaymentUpdate.id;

      const checkPaymentSpy = jest
        .spyOn(workflowService, 'checkPayment')
        .mockResolvedValue(mockPaymentUpdate);

      // act
      const result = await controller.checkPayment(id);

      // assert
      expect(result).toEqual(mockPaymentUpdate);
      expect(checkPaymentSpy).toBeCalledTimes(1);
      expect(checkPaymentSpy).toBeCalledWith(id);
    });

    it('should throw bad request exception', async () => {
      // arrange
      const id = faker.string.sample();

      const checkPaymentSpy = jest
        .spyOn(workflowService, 'checkPayment')
        .mockRejectedValue(new BadRequestException('Bad request'));

      // act
      const checkPayment = controller.checkPayment(id);

      // assert
      await expect(checkPayment).rejects.toEqual(
        new BadRequestException('Bad request'),
      );
      expect(checkPaymentSpy).toBeCalledTimes(1);
      expect(checkPaymentSpy).toBeCalledWith(id);
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.string.uuid();

      const checkPaymentSpy = jest
        .spyOn(workflowService, 'checkPayment')
        .mockRejectedValue(new NotFoundException('Not found'));

      // act
      const checkPayment = controller.checkPayment(id);

      // assert
      await expect(checkPayment).rejects.toEqual(
        new NotFoundException('Not found'),
      );
      expect(checkPaymentSpy).toBeCalledTimes(1);
      expect(checkPaymentSpy).toBeCalledWith(id);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      const id = faker.string.uuid();

      const checkPaymentSpy = jest
        .spyOn(workflowService, 'checkPayment')
        .mockRejectedValue(
          new UnprocessableEntityException('Unprocessable entity'),
        );

      // act
      const checkPayment = controller.checkPayment(id);

      // assert
      await expect(checkPayment).rejects.toEqual(
        new UnprocessableEntityException('Unprocessable entity'),
      );
      expect(checkPaymentSpy).toBeCalledTimes(1);
      expect(checkPaymentSpy).toBeCalledWith(id);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const id = faker.string.uuid();

      const checkPaymentSpy = jest
        .spyOn(workflowService, 'checkPayment')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const checkPayment = controller.checkPayment(id);

      // assert
      await expect(checkPayment).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(checkPaymentSpy).toBeCalledTimes(1);
      expect(checkPaymentSpy).toBeCalledWith(id);
    });
  });

  describe('commitPayment', () => {
    let mockCommitPayment: ICommitPayment;

    beforeEach(() => {
      mockCommitPayment = {
        reservationId: faker.number.int({ min: 1 }),
        commitPaymentTime: faker.date.recent().toISOString(),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return commit payment time', async () => {
      // arrange
      const id = mockCommitPayment.reservationId;

      const commitPaymentSpy = jest
        .spyOn(workflowService, 'commitPayment')
        .mockResolvedValue(mockCommitPayment);

      // act
      const result = await controller.commitPayment(id);

      // assert
      expect(result).toEqual(mockCommitPayment);
      expect(commitPaymentSpy).toBeCalledTimes(1);
      expect(commitPaymentSpy).toBeCalledWith(id);
    });

    it('should throw bad request exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const commitPaymentSpy = jest
        .spyOn(workflowService, 'commitPayment')
        .mockRejectedValue(new BadRequestException('Bad request'));

      // act
      const commitPayment = controller.commitPayment(id);

      // assert
      await expect(commitPayment).rejects.toEqual(
        new BadRequestException('Bad request'),
      );
      expect(commitPaymentSpy).toBeCalledTimes(1);
      expect(commitPaymentSpy).toBeCalledWith(id);
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const commitPaymentSpy = jest
        .spyOn(workflowService, 'commitPayment')
        .mockRejectedValue(new NotFoundException('Not found'));

      // act
      const commitPayment = controller.commitPayment(id);

      // assert
      await expect(commitPayment).rejects.toEqual(
        new NotFoundException('Not found'),
      );
      expect(commitPaymentSpy).toBeCalledTimes(1);
      expect(commitPaymentSpy).toBeCalledWith(id);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const commitPaymentSpy = jest
        .spyOn(workflowService, 'commitPayment')
        .mockRejectedValue(
          new UnprocessableEntityException('Unprocessable entity'),
        );

      // act
      const checkPayment = controller.commitPayment(id);

      // assert
      await expect(checkPayment).rejects.toEqual(
        new UnprocessableEntityException('Unprocessable entity'),
      );
      expect(commitPaymentSpy).toBeCalledTimes(1);
      expect(commitPaymentSpy).toBeCalledWith(id);
    });

    it('should throw not found exception', async () => {
      // arrange
      const id = faker.number.int({ min: 1 });

      const commitPaymentSpy = jest
        .spyOn(workflowService, 'commitPayment')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const checkPayment = controller.commitPayment(id);

      // assert
      await expect(checkPayment).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(commitPaymentSpy).toBeCalledTimes(1);
      expect(commitPaymentSpy).toBeCalledWith(id);
    });
  });

  describe('chargeTransaction', () => {
    let mockChargeDto: ChargePaymentDto;
    let mockChargeTransaction: IChargeTransaction;

    beforeEach(() => {
      mockChargeTransaction = {
        bank: faker.helpers.enumValue(EBankChoice),
        virtualAccountNumber: faker.string.numeric({ length: 10 }),
        billerCode: faker.string.numeric({ length: 10 }),
        billerKey: faker.string.numeric({ length: 10 }),
      };

      mockChargeDto = {
        bank: faker.helpers.enumValue(EBankChoice),
        reservationId: faker.number.int({ min: 1 }),
        bookingCode: faker.string.alphanumeric({ length: 8, casing: 'upper' }),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should return result charge transaction', async () => {
      // arrange
      const chargeTransactionSpy = jest
        .spyOn(workflowService, 'chargeTransaction')
        .mockResolvedValue(mockChargeTransaction);

      // act
      const result = await controller.chargeTransaction(mockChargeDto);

      // assert
      expect(result).toEqual(mockChargeTransaction);
      expect(chargeTransactionSpy).toBeCalledTimes(1);
      expect(chargeTransactionSpy).toBeCalledWith(mockChargeDto);
    });

    it('should throw bad request exception', async () => {
      // arrange
      const chargeTransactionSpy = jest
        .spyOn(workflowService, 'chargeTransaction')
        .mockRejectedValue(new BadRequestException('Bad request'));

      // act
      const chargeTransaction = controller.chargeTransaction(mockChargeDto);

      // assert
      await expect(chargeTransaction).rejects.toEqual(
        new BadRequestException('Bad request'),
      );
      expect(chargeTransactionSpy).toBeCalledTimes(1);
      expect(chargeTransactionSpy).toBeCalledWith(mockChargeDto);
    });

    it('should throw not found exception', async () => {
      // arrange
      const chargeTransactionSpy = jest
        .spyOn(workflowService, 'chargeTransaction')
        .mockRejectedValue(new NotFoundException('Not found'));

      // act
      const chargeTransaction = controller.chargeTransaction(mockChargeDto);

      // assert
      await expect(chargeTransaction).rejects.toEqual(
        new NotFoundException('Not found'),
      );
      expect(chargeTransactionSpy).toBeCalledTimes(1);
      expect(chargeTransactionSpy).toBeCalledWith(mockChargeDto);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      const chargeTransactionSpy = jest
        .spyOn(workflowService, 'chargeTransaction')
        .mockRejectedValue(
          new UnprocessableEntityException('Unprocessable entity'),
        );

      // act
      const chargeTransaction = controller.chargeTransaction(mockChargeDto);

      // assert
      await expect(chargeTransaction).rejects.toEqual(
        new UnprocessableEntityException('Unprocessable entity'),
      );
      expect(chargeTransactionSpy).toBeCalledTimes(1);
      expect(chargeTransactionSpy).toBeCalledWith(mockChargeDto);
    });

    it('should throw not found exception', async () => {
      // arrange
      const chargeTransactionSpy = jest
        .spyOn(workflowService, 'chargeTransaction')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const chargeTransaction = controller.chargeTransaction(mockChargeDto);

      // assert
      await expect(chargeTransaction).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(chargeTransactionSpy).toBeCalledTimes(1);
      expect(chargeTransactionSpy).toBeCalledWith(mockChargeDto);
    });
  });

  describe('confirmPayment', () => {
    it('should update workflow task confirm payment', async () => {
      // arrange
      const reservationId = faker.number.int({ min: 1 });
      const mockResponse = {
        messageResponse: faker.string.sample(),
        reservationId,
      };

      const confirmPaymentSpy = jest
        .spyOn(workflowService, 'confirmPayment')
        .mockResolvedValue(mockResponse);

      // act
      const result = await controller.confirmPayment(reservationId);

      // assert
      expect(result).toEqual(mockResponse);
      expect(confirmPaymentSpy).toBeCalledTimes(1);
      expect(confirmPaymentSpy).toBeCalledWith(reservationId);
    });

    it('should throw bad request exception', async () => {
      // arrange
      const reservationId = faker.number.int({ min: 1 });

      const confirmPaymentSpy = jest
        .spyOn(workflowService, 'confirmPayment')
        .mockRejectedValue(new BadRequestException('Bad request'));

      // act
      const confirmPayment = controller.confirmPayment(reservationId);

      // assert
      await expect(confirmPayment).rejects.toEqual(
        new BadRequestException('Bad request'),
      );
      expect(confirmPaymentSpy).toBeCalledTimes(1);
      expect(confirmPaymentSpy).toBeCalledWith(reservationId);
    });

    it('should throw not found exception', async () => {
      // arrange
      const reservationId = faker.number.int({ min: 1 });

      const confirmPaymentSpy = jest
        .spyOn(workflowService, 'confirmPayment')
        .mockRejectedValue(new NotFoundException('Not found'));

      // act
      const confirmPayment = controller.confirmPayment(reservationId);

      // assert
      await expect(confirmPayment).rejects.toEqual(
        new NotFoundException('Not found'),
      );
      expect(confirmPaymentSpy).toBeCalledTimes(1);
      expect(confirmPaymentSpy).toBeCalledWith(reservationId);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      const reservationId = faker.number.int({ min: 1 });

      const confirmPaymentSpy = jest
        .spyOn(workflowService, 'confirmPayment')
        .mockRejectedValue(
          new UnprocessableEntityException('Unprocessable entity'),
        );

      // act
      const confirmPayment = controller.confirmPayment(reservationId);

      // assert
      await expect(confirmPayment).rejects.toEqual(
        new UnprocessableEntityException('Unprocessable entity'),
      );
      expect(confirmPaymentSpy).toBeCalledTimes(1);
      expect(confirmPaymentSpy).toBeCalledWith(reservationId);
    });

    it('should throw not found exception', async () => {
      // arrange
      const reservationId = faker.number.int({ min: 1 });

      const confirmPaymentSpy = jest
        .spyOn(workflowService, 'confirmPayment')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const confirmPayment = controller.confirmPayment(reservationId);

      // assert
      await expect(confirmPayment).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(confirmPaymentSpy).toBeCalledTimes(1);
      expect(confirmPaymentSpy).toBeCalledWith(reservationId);
    });
  });

  describe('redeemTicket', () => {
    let mockReservationDto: ReservationCodeDto;

    beforeEach(() => {
      mockReservationDto = {
        identityNumber: faker.string.numeric({ length: 16 }),
        reservationCode: faker.string.alphanumeric({
          length: 8,
          casing: 'upper',
        }),
      };
    });

    afterEach(() => jest.clearAllMocks());
    it('should update workflow task redeem ticket', async () => {
      // arrange
      const mockResponse = {
        messageResponse: faker.string.sample(),
        redeemTime: faker.date.recent().toISOString(),
      };

      const redeemTicketSpy = jest
        .spyOn(workflowService, 'redeemTicket')
        .mockResolvedValue(mockResponse);

      // act
      const result = await controller.redeemTicket(mockReservationDto);

      // assert
      expect(result).toEqual(mockResponse);
      expect(redeemTicketSpy).toBeCalledTimes(1);
      expect(redeemTicketSpy).toBeCalledWith(mockReservationDto);
    });

    it('should throw bad request exception', async () => {
      // arrange
      const redeemTicketSpy = jest
        .spyOn(workflowService, 'redeemTicket')
        .mockRejectedValue(new BadRequestException('Bad request'));

      // act
      const redeemTicket = controller.redeemTicket(mockReservationDto);

      // assert
      await expect(redeemTicket).rejects.toEqual(
        new BadRequestException('Bad request'),
      );
      expect(redeemTicketSpy).toBeCalledTimes(1);
      expect(redeemTicketSpy).toBeCalledWith(mockReservationDto);
    });

    it('should throw not found exception', async () => {
      // arrange
      const redeemTicketSpy = jest
        .spyOn(workflowService, 'redeemTicket')
        .mockRejectedValue(new NotFoundException('Not found'));

      // act
      const redeemTicket = controller.redeemTicket(mockReservationDto);

      // assert
      await expect(redeemTicket).rejects.toEqual(
        new NotFoundException('Not found'),
      );
      expect(redeemTicketSpy).toBeCalledTimes(1);
      expect(redeemTicketSpy).toBeCalledWith(mockReservationDto);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      const redeemTicketSpy = jest
        .spyOn(workflowService, 'redeemTicket')
        .mockRejectedValue(
          new UnprocessableEntityException('Unprocessable entity'),
        );

      // act
      const redeemTicket = controller.redeemTicket(mockReservationDto);

      // assert
      await expect(redeemTicket).rejects.toEqual(
        new UnprocessableEntityException('Unprocessable entity'),
      );
      expect(redeemTicketSpy).toBeCalledTimes(1);
      expect(redeemTicketSpy).toBeCalledWith(mockReservationDto);
    });

    it('should throw not found exception', async () => {
      // arrange
      const redeemTicketSpy = jest
        .spyOn(workflowService, 'redeemTicket')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const redeemTicket = controller.redeemTicket(mockReservationDto);

      // assert
      await expect(redeemTicket).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(redeemTicketSpy).toBeCalledTimes(1);
      expect(redeemTicketSpy).toBeCalledWith(mockReservationDto);
    });
  });

  describe('onboard', () => {
    const ticketNumber = faker.string.numeric({ length: 13 });

    it('should update workflow task onboarding', async () => {
      // arrange
      const mockResponse = {
        messageResponse: faker.string.sample(),
        boardingTime: faker.date.recent().toISOString(),
      };

      const onboardSpy = jest
        .spyOn(workflowService, 'onboard')
        .mockResolvedValue(mockResponse);

      // act
      const result = await controller.onboard(ticketNumber);

      // assert
      expect(result).toEqual(mockResponse);
      expect(onboardSpy).toBeCalledTimes(1);
      expect(onboardSpy).toBeCalledWith(ticketNumber);
    });

    it('should throw bad request exception', async () => {
      // arrange
      const onboardSpy = jest
        .spyOn(workflowService, 'onboard')
        .mockRejectedValue(new BadRequestException('Bad request'));

      // act
      const onboard = controller.onboard(ticketNumber);

      // assert
      await expect(onboard).rejects.toEqual(
        new BadRequestException('Bad request'),
      );
      expect(onboardSpy).toBeCalledTimes(1);
      expect(onboardSpy).toBeCalledWith(ticketNumber);
    });

    it('should throw not found exception', async () => {
      // arrange
      const onboardSpy = jest
        .spyOn(workflowService, 'onboard')
        .mockRejectedValue(new NotFoundException('Not found'));

      // act
      const onboard = controller.onboard(ticketNumber);

      // assert
      await expect(onboard).rejects.toEqual(new NotFoundException('Not found'));
      expect(onboardSpy).toBeCalledTimes(1);
      expect(onboardSpy).toBeCalledWith(ticketNumber);
    });

    it('should throw unprocessable entity exception', async () => {
      // arrange
      const onboardSpy = jest
        .spyOn(workflowService, 'onboard')
        .mockRejectedValue(
          new UnprocessableEntityException('Unprocessable entity'),
        );

      // act
      const onboard = controller.onboard(ticketNumber);

      // assert
      await expect(onboard).rejects.toEqual(
        new UnprocessableEntityException('Unprocessable entity'),
      );
      expect(onboardSpy).toBeCalledTimes(1);
      expect(onboardSpy).toBeCalledWith(ticketNumber);
    });

    it('should throw not found exception', async () => {
      // arrange
      const onboardSpy = jest
        .spyOn(workflowService, 'onboard')
        .mockRejectedValue(
          new InternalServerErrorException('Unexpected error'),
        );

      // act
      const onboard = controller.onboard(ticketNumber);

      // assert
      await expect(onboard).rejects.toEqual(
        new InternalServerErrorException('Unexpected error'),
      );
      expect(onboardSpy).toBeCalledTimes(1);
      expect(onboardSpy).toBeCalledWith(ticketNumber);
    });
  });

  describe('handleReportTaskWorkflow', () => {
    let dataWorkflowDetail: IWorkflowDetail;
    let dataWorkflow: IWorkflowMaster;

    beforeEach(() => {
      dataWorkflow = {
        processInstanceId: faker.string.uuid(),
        reservationId: faker.number.int({ min: 1 }),
      };

      dataWorkflowDetail = {
        workflow: dataWorkflow,
        taskId: faker.string.uuid(),
        activityName: faker.helpers.enumValue(ETaskName),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should process data workflow without rejection', async () => {
      // arrange

      const insertWorkflowDetail = jest.spyOn(
        workflowService,
        'insertWorkflowDetail',
      );

      // act
      const handleReportTaskWorkflow =
        controller.handleReportTaskWorkflow(dataWorkflowDetail);

      // assert
      await expect(handleReportTaskWorkflow).resolves.not.toThrow();
      expect(insertWorkflowDetail).toBeCalledTimes(1);
      expect(insertWorkflowDetail).toBeCalledWith(dataWorkflowDetail);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const insertWorkflowDetail = jest
        .spyOn(workflowService, 'insertWorkflowDetail')
        .mockRejectedValue(
          new InternalServerErrorException('Error in handleReportTaskWorkflow'),
        );

      // act
      const handleReportTaskWorkflow =
        controller.handleReportTaskWorkflow(dataWorkflowDetail);

      // assert
      await expect(handleReportTaskWorkflow).rejects.toEqual(
        new InternalServerErrorException('Error in handleReportTaskWorkflow'),
      );
      expect(insertWorkflowDetail).toBeCalledTimes(1);
      expect(insertWorkflowDetail).toBeCalledWith(dataWorkflowDetail);
    });
  });

  describe('handleUpdateSeatStatus', () => {
    let seatStatusUpdate: ISeatStatusUpdate;

    beforeEach(() => {
      seatStatusUpdate = {
        flightDate: faker.date.future().toISOString(),
        flight: faker.number.int({ min: 1 }),
        seat: faker.number.int({ min: 1 }),
        seatStatus: faker.helpers.enumValue(ESeatStatus),
      };
    });

    afterEach(() => jest.clearAllMocks());

    it('should process data workflow without rejection', async () => {
      // arrange

      const updateSeatStatus = jest.spyOn(workflowService, 'updateSeatStatus');

      // act
      const handleUpdateSeatStatus =
        controller.handleUpdateSeatStatus(seatStatusUpdate);

      // assert
      await expect(handleUpdateSeatStatus).resolves.not.toThrow();
      expect(updateSeatStatus).toBeCalledTimes(1);
      expect(updateSeatStatus).toBeCalledWith(seatStatusUpdate);
    });

    it('should throw internal server error exception', async () => {
      // arrange
      const updateSeatStatus = jest
        .spyOn(workflowService, 'updateSeatStatus')
        .mockRejectedValue(
          new InternalServerErrorException('Error in handleUpdateSeatStatus'),
        );

      // act
      const handleUpdateSeatStatus =
        controller.handleUpdateSeatStatus(seatStatusUpdate);

      // assert
      await expect(handleUpdateSeatStatus).rejects.toEqual(
        new InternalServerErrorException('Error in handleUpdateSeatStatus'),
      );
      expect(updateSeatStatus).toBeCalledTimes(1);
      expect(updateSeatStatus).toBeCalledWith(seatStatusUpdate);
    });
  });
});
