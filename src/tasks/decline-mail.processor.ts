import { Process, Processor } from '@nestjs/bull';
import { ConfigService } from '@nestjs/config';
import { join } from 'path';
import { readFile } from 'fs';
import * as nodemailer from 'nodemailer';
import { Logger } from '@nestjs/common';
import { templateSettings, template } from 'lodash';
import { MailOptions } from 'nodemailer/lib/json-transport';

@Processor('DeclineMailQueue')
export class DeclineMailProcessor {
  private readonly logger = new Logger(DeclineMailProcessor.name);
  private readonly mailerService = nodemailer;

  constructor(private configService: ConfigService) {}

  @Process()
  async senderHandler(job: any) {
    try {
      const { mail_to, ...data } = job.data;

      const templatePath = join(
        __dirname,
        '../../template/template-decline.html',
      );
      templateSettings.interpolate = /{{([\s\S]+?)}}/g;
      let _content = await this.__readFilePromise(templatePath);
      const compiled = template(_content);
      _content = compiled(data);

      const transporter = this.mailerService.createTransport({
        host: this.configService.get<string>('SMTP_HOST'),
        port: this.configService.get<number>('SMTP_PORT'),
        // logger: true,
        // debug: true,
        secure: false,
        auth: {
          user: this.configService.get<string>('SMTP_USER'),
          pass: this.configService.get<string>('SMTP_PASS'),
        },
      });

      const mailOptions: MailOptions = {
        from: this.configService.get<string>('MAIL_SENDER'),
        to: this.configService.get<string>('MAIL_RECIPIENT'),
        subject: 'FWW Airlines',
        html: _content,
      };

      await transporter.sendMail(mailOptions);
    } catch (error) {
      this.logger.error(error);
    }
  }

  async __readFilePromise(filePath: string): Promise<string> {
    return new Promise((resolve, reject) => {
      readFile(filePath, 'utf8', (err, html) => {
        if (!err) {
          resolve(html);
        } else {
          reject(err);
        }
      });
    });
  }
}
