import { Module } from '@nestjs/common';
import { AuthBasicStrategy } from './auth-basic.strategy';
import { ConfigModule } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { AuthConfig } from './auth.config';
import { CognitoStrategy } from './cognito.strategy';

@Module({
  imports: [PassportModule, ConfigModule],
  providers: [AuthBasicStrategy, AuthConfig, CognitoStrategy],
  exports: [AuthConfig],
})
export class AuthModule {}
