import { BasicStrategy as Strategy } from 'passport-http';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthBasicStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly configService: ConfigService) {
    super({
      passReqToCallback: true,
    });
  }

  public validate = async (
    _: Request,
    username: string,
    password: string,
  ): Promise<boolean> => {
    if (
      this.configService.get<string>('BASIC_USER') === username &&
      this.configService.get<string>('BASIC_PASS') === password
    ) {
      return true;
    }
    throw new UnauthorizedException();
  };
}
